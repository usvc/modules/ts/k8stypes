# k8stypes

This repository contains TypeScript type definitions for Kubernetes resources.

# Usage

## Installation

```sh
npm install -D k8stypes;
```

To install the types for a specific version of Kubernetes, use the `@` operator followed by the Kubernetes version. For example if you'd like to install the types for 1.15.2:

```sh
npm install -D k8stypes@1.15.2;
```

## Usage

### TypeScript

```typescript
import { Deployment } from 'k8stypes/apps/v1/Deployment';

const deployment : Deployment = {/* ... */};
export default deployment;
```

### JavaScript

```js
/**
 * @typedef {import('k8stypes/apps/v1/Deployment').Deployment} Deployment
 */

/** @type {Deployment} */
const deployment = {/* ... */};
module.exports = deployment;
```



# Development Runbook

**All development should be done within the `/src` directory**.

## Retrieving the latest Kubernetes OpenAPI specification

```sh
npm run update;
```

> This has only been tested to work on Ubuntu.

## Generating the TypeScript definitions

```sh
npm run generate;
```

> This will result in a `./src` directory containing the TypeScript definitions.

## Building the JavaScript version

```sh
npm run build;
```

> This will result in the NPM publishable package at `./lib`. Run `npm publish` from `./lib` of the repository to publish it:

## Publishing the package

1. Bump the appropriate version in `package.json`
2. Commit the change
3. **Navigate to `./lib`** and run:

```sh
npm publish;
```

# License

This code is licensed under [the MIT license](./LICENSE).
