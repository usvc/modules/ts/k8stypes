import { ExternalMetricStatus } from '../../autoscaling/v2beta1/ExternalMetricStatus';
import { ObjectMetricStatus } from '../../autoscaling/v2beta1/ObjectMetricStatus';
import { PodsMetricStatus } from '../../autoscaling/v2beta1/PodsMetricStatus';
import { ResourceMetricStatus } from '../../autoscaling/v2beta1/ResourceMetricStatus';

/**
 * MetricStatus describes the last-read state of a single metric.
 * @interface MetricStatus
 * @see io.k8s.api.autoscaling.v2beta1.MetricStatus
 */
export interface MetricStatus {
  /**
   * external refers to a global metric that is not associated with any Kubernetes object. It allows autoscaling based on information coming from components running outside of cluster (for example length of queue in cloud messaging service, or QPS from loadbalancer running outside of cluster).
   * @type ExternalMetricStatus
   */
  external ?: ExternalMetricStatus

  /**
   * object refers to a metric describing a single kubernetes object (for example, hits-per-second on an Ingress object).
   * @type ObjectMetricStatus
   */
  object ?: ObjectMetricStatus

  /**
   * pods refers to a metric describing each pod in the current scale target (for example, transactions-processed-per-second).  The values will be averaged together before being compared to the target value.
   * @type PodsMetricStatus
   */
  pods ?: PodsMetricStatus

  /**
   * resource refers to a resource metric (such as those specified in requests and limits) known to Kubernetes describing each pod in the current scale target (e.g. CPU or memory). Such metrics are built in to Kubernetes, and have special scaling options on top of those available to normal per-pod metrics using the "pods" source.
   * @type ResourceMetricStatus
   */
  resource ?: ResourceMetricStatus

  /**
   * type is the type of metric source.  It will be one of "Object", "Pods" or "Resource", each corresponds to a matching field in the object.
   * @type string
   */
  type : string

}
