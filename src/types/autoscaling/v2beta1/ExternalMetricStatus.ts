import { LabelSelector } from '../../meta/v1/LabelSelector';
import { Quantity } from '../../api/resource/Quantity';

/**
 * ExternalMetricStatus indicates the current value of a global metric not associated with any Kubernetes object.
 * @interface ExternalMetricStatus
 * @see io.k8s.api.autoscaling.v2beta1.ExternalMetricStatus
 */
export interface ExternalMetricStatus {
  /**
   * currentAverageValue is the current value of metric averaged over autoscaled pods.
   * @type Quantity
   */
  currentAverageValue ?: Quantity

  /**
   * currentValue is the current value of the metric (as a quantity)
   * @type Quantity
   */
  currentValue : Quantity

  /**
   * metricName is the name of a metric used for autoscaling in metric system.
   * @type string
   */
  metricName : string

  /**
   * metricSelector is used to identify a specific time series within a given metric.
   * @type LabelSelector
   */
  metricSelector ?: LabelSelector

}
