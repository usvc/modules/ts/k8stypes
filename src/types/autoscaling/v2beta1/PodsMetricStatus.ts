import { LabelSelector } from '../../meta/v1/LabelSelector';
import { Quantity } from '../../api/resource/Quantity';

/**
 * PodsMetricStatus indicates the current value of a metric describing each pod in the current scale target (for example, transactions-processed-per-second).
 * @interface PodsMetricStatus
 * @see io.k8s.api.autoscaling.v2beta1.PodsMetricStatus
 */
export interface PodsMetricStatus {
  /**
   * currentAverageValue is the current value of the average of the metric across all relevant pods (as a quantity)
   * @type Quantity
   */
  currentAverageValue : Quantity

  /**
   * metricName is the name of the metric in question
   * @type string
   */
  metricName : string

  /**
   * selector is the string-encoded form of a standard kubernetes label selector for the given metric When set in the PodsMetricSource, it is passed as an additional parameter to the metrics server for more specific metrics scoping. When unset, just the metricName will be used to gather metrics.
   * @type LabelSelector
   */
  selector ?: LabelSelector

}
