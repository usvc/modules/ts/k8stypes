import { Time } from '../../meta/v1/Time';

/**
 * HorizontalPodAutoscalerCondition describes the state of a HorizontalPodAutoscaler at a certain point.
 * @interface HorizontalPodAutoscalerCondition
 * @see io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscalerCondition
 */
export interface HorizontalPodAutoscalerCondition {
  /**
   * lastTransitionTime is the last time the condition transitioned from one status to another
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * message is a human-readable explanation containing details about the transition
   * @type string
   */
  message ?: string

  /**
   * reason is the reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * status is the status of the condition (True, False, Unknown)
   * @type string
   */
  status : string

  /**
   * type describes the current condition
   * @type string
   */
  type : string

}
