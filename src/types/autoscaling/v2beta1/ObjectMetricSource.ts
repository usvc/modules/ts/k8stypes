import { CrossVersionObjectReference } from '../../autoscaling/v2beta1/CrossVersionObjectReference';
import { LabelSelector } from '../../meta/v1/LabelSelector';
import { Quantity } from '../../api/resource/Quantity';

/**
 * ObjectMetricSource indicates how to scale on a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).
 * @interface ObjectMetricSource
 * @see io.k8s.api.autoscaling.v2beta1.ObjectMetricSource
 */
export interface ObjectMetricSource {
  /**
   * averageValue is the target value of the average of the metric across all relevant pods (as a quantity)
   * @type Quantity
   */
  averageValue ?: Quantity

  /**
   * metricName is the name of the metric in question.
   * @type string
   */
  metricName : string

  /**
   * selector is the string-encoded form of a standard kubernetes label selector for the given metric When set, it is passed as an additional parameter to the metrics server for more specific metrics scoping When unset, just the metricName will be used to gather metrics.
   * @type LabelSelector
   */
  selector ?: LabelSelector

  /**
   * target is the described Kubernetes object.
   * @type CrossVersionObjectReference
   */
  target : CrossVersionObjectReference

  /**
   * targetValue is the target value of the metric (as a quantity).
   * @type Quantity
   */
  targetValue : Quantity

}
