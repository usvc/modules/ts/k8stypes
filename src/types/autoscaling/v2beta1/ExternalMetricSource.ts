import { LabelSelector } from '../../meta/v1/LabelSelector';
import { Quantity } from '../../api/resource/Quantity';

/**
 * ExternalMetricSource indicates how to scale on a metric not associated with any Kubernetes object (for example length of queue in cloud messaging service, or QPS from loadbalancer running outside of cluster). Exactly one "target" type should be set.
 * @interface ExternalMetricSource
 * @see io.k8s.api.autoscaling.v2beta1.ExternalMetricSource
 */
export interface ExternalMetricSource {
  /**
   * metricName is the name of the metric in question.
   * @type string
   */
  metricName : string

  /**
   * metricSelector is used to identify a specific time series within a given metric.
   * @type LabelSelector
   */
  metricSelector ?: LabelSelector

  /**
   * targetAverageValue is the target per-pod value of global metric (as a quantity). Mutually exclusive with TargetValue.
   * @type Quantity
   */
  targetAverageValue ?: Quantity

  /**
   * targetValue is the target value of the metric (as a quantity). Mutually exclusive with TargetAverageValue.
   * @type Quantity
   */
  targetValue ?: Quantity

}
