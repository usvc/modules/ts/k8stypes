import { HorizontalPodAutoscalerCondition } from '../../autoscaling/v2beta2/HorizontalPodAutoscalerCondition';
import { MetricStatus } from '../../autoscaling/v2beta2/MetricStatus';
import { Time } from '../../meta/v1/Time';

/**
 * HorizontalPodAutoscalerStatus describes the current status of a horizontal pod autoscaler.
 * @interface HorizontalPodAutoscalerStatus
 * @see io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscalerStatus
 */
export interface HorizontalPodAutoscalerStatus {
  /**
   * conditions is the set of conditions required for this autoscaler to scale its target, and indicates whether or not those conditions are met.
   * @type HorizontalPodAutoscalerCondition[]
   */
  conditions : HorizontalPodAutoscalerCondition[]

  /**
   * currentMetrics is the last read state of the metrics used by this autoscaler.
   * @type MetricStatus[]
   */
  currentMetrics ?: MetricStatus[]

  /**
   * currentReplicas is current number of replicas of pods managed by this autoscaler, as last seen by the autoscaler.
   * @type number
   */
  currentReplicas : number

  /**
   * desiredReplicas is the desired number of replicas of pods managed by this autoscaler, as last calculated by the autoscaler.
   * @type number
   */
  desiredReplicas : number

  /**
   * lastScaleTime is the last time the HorizontalPodAutoscaler scaled the number of pods, used by the autoscaler to control how often the number of pods is changed.
   * @type Time
   */
  lastScaleTime ?: Time

  /**
   * observedGeneration is the most recent generation observed by this autoscaler.
   * @type number
   */
  observedGeneration ?: number

}
