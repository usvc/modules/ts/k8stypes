import { MetricIdentifier } from '../../autoscaling/v2beta2/MetricIdentifier';
import { MetricValueStatus } from '../../autoscaling/v2beta2/MetricValueStatus';

/**
 * PodsMetricStatus indicates the current value of a metric describing each pod in the current scale target (for example, transactions-processed-per-second).
 * @interface PodsMetricStatus
 * @see io.k8s.api.autoscaling.v2beta2.PodsMetricStatus
 */
export interface PodsMetricStatus {
  /**
   * current contains the current value for the given metric
   * @type MetricValueStatus
   */
  current : MetricValueStatus

  /**
   * metric identifies the target metric by name and selector
   * @type MetricIdentifier
   */
  metric : MetricIdentifier

}
