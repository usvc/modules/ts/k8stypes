import { CrossVersionObjectReference } from '../../autoscaling/v2beta2/CrossVersionObjectReference';
import { MetricIdentifier } from '../../autoscaling/v2beta2/MetricIdentifier';
import { MetricValueStatus } from '../../autoscaling/v2beta2/MetricValueStatus';

/**
 * ObjectMetricStatus indicates the current value of a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).
 * @interface ObjectMetricStatus
 * @see io.k8s.api.autoscaling.v2beta2.ObjectMetricStatus
 */
export interface ObjectMetricStatus {
  /**
   * current contains the current value for the given metric
   * @type MetricValueStatus
   */
  current : MetricValueStatus

  /**
   * no description provided
   * @type CrossVersionObjectReference
   */
  describedObject : CrossVersionObjectReference

  /**
   * metric identifies the target metric by name and selector
   * @type MetricIdentifier
   */
  metric : MetricIdentifier

}
