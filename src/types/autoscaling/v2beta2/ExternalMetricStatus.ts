import { MetricIdentifier } from '../../autoscaling/v2beta2/MetricIdentifier';
import { MetricValueStatus } from '../../autoscaling/v2beta2/MetricValueStatus';

/**
 * ExternalMetricStatus indicates the current value of a global metric not associated with any Kubernetes object.
 * @interface ExternalMetricStatus
 * @see io.k8s.api.autoscaling.v2beta2.ExternalMetricStatus
 */
export interface ExternalMetricStatus {
  /**
   * current contains the current value for the given metric
   * @type MetricValueStatus
   */
  current : MetricValueStatus

  /**
   * metric identifies the target metric by name and selector
   * @type MetricIdentifier
   */
  metric : MetricIdentifier

}
