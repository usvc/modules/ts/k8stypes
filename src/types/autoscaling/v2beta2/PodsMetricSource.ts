import { MetricIdentifier } from '../../autoscaling/v2beta2/MetricIdentifier';
import { MetricTarget } from '../../autoscaling/v2beta2/MetricTarget';

/**
 * PodsMetricSource indicates how to scale on a metric describing each pod in the current scale target (for example, transactions-processed-per-second). The values will be averaged together before being compared to the target value.
 * @interface PodsMetricSource
 * @see io.k8s.api.autoscaling.v2beta2.PodsMetricSource
 */
export interface PodsMetricSource {
  /**
   * metric identifies the target metric by name and selector
   * @type MetricIdentifier
   */
  metric : MetricIdentifier

  /**
   * target specifies the target value for the given metric
   * @type MetricTarget
   */
  target : MetricTarget

}
