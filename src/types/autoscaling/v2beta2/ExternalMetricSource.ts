import { MetricIdentifier } from '../../autoscaling/v2beta2/MetricIdentifier';
import { MetricTarget } from '../../autoscaling/v2beta2/MetricTarget';

/**
 * ExternalMetricSource indicates how to scale on a metric not associated with any Kubernetes object (for example length of queue in cloud messaging service, or QPS from loadbalancer running outside of cluster).
 * @interface ExternalMetricSource
 * @see io.k8s.api.autoscaling.v2beta2.ExternalMetricSource
 */
export interface ExternalMetricSource {
  /**
   * metric identifies the target metric by name and selector
   * @type MetricIdentifier
   */
  metric : MetricIdentifier

  /**
   * target specifies the target value for the given metric
   * @type MetricTarget
   */
  target : MetricTarget

}
