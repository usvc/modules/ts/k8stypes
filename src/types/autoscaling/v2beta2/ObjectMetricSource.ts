import { CrossVersionObjectReference } from '../../autoscaling/v2beta2/CrossVersionObjectReference';
import { MetricIdentifier } from '../../autoscaling/v2beta2/MetricIdentifier';
import { MetricTarget } from '../../autoscaling/v2beta2/MetricTarget';

/**
 * ObjectMetricSource indicates how to scale on a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).
 * @interface ObjectMetricSource
 * @see io.k8s.api.autoscaling.v2beta2.ObjectMetricSource
 */
export interface ObjectMetricSource {
  /**
   * no description provided
   * @type CrossVersionObjectReference
   */
  describedObject : CrossVersionObjectReference

  /**
   * metric identifies the target metric by name and selector
   * @type MetricIdentifier
   */
  metric : MetricIdentifier

  /**
   * target specifies the target value for the given metric
   * @type MetricTarget
   */
  target : MetricTarget

}
