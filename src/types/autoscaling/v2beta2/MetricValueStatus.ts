import { Quantity } from '../../api/resource/Quantity';

/**
 * MetricValueStatus holds the current value for a metric
 * @interface MetricValueStatus
 * @see io.k8s.api.autoscaling.v2beta2.MetricValueStatus
 */
export interface MetricValueStatus {
  /**
   * currentAverageUtilization is the current value of the average of the resource metric across all relevant pods, represented as a percentage of the requested value of the resource for the pods.
   * @type number
   */
  averageUtilization ?: number

  /**
   * averageValue is the current value of the average of the metric across all relevant pods (as a quantity)
   * @type Quantity
   */
  averageValue ?: Quantity

  /**
   * value is the current value of the metric (as a quantity).
   * @type Quantity
   */
  value ?: Quantity

}
