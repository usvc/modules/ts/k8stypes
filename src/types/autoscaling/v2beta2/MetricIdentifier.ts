import { LabelSelector } from '../../meta/v1/LabelSelector';

/**
 * MetricIdentifier defines the name and optionally selector for a metric
 * @interface MetricIdentifier
 * @see io.k8s.api.autoscaling.v2beta2.MetricIdentifier
 */
export interface MetricIdentifier {
  /**
   * name is the name of the given metric
   * @type string
   */
  name : string

  /**
   * selector is the string-encoded form of a standard kubernetes label selector for the given metric When set, it is passed as an additional parameter to the metrics server for more specific metrics scoping. When unset, just the metricName will be used to gather metrics.
   * @type LabelSelector
   */
  selector ?: LabelSelector

}
