import { HorizontalPodAutoscalerSpec } from '../../autoscaling/v2beta2/HorizontalPodAutoscalerSpec';
import { HorizontalPodAutoscalerStatus } from '../../autoscaling/v2beta2/HorizontalPodAutoscalerStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * HorizontalPodAutoscaler is the configuration for a horizontal pod autoscaler, which automatically manages the replica count of any resource implementing the scale subresource based on the metrics specified.
 * @interface HorizontalPodAutoscaler
 * @see io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscaler
 */
export interface HorizontalPodAutoscaler {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "autoscaling/v2beta2" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * metadata is the standard object metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * spec is the specification for the behaviour of the autoscaler. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status.
   * @type HorizontalPodAutoscalerSpec
   */
  spec ?: HorizontalPodAutoscalerSpec

  /**
   * status is the current information about the autoscaler.
   * @type HorizontalPodAutoscalerStatus
   */
  status ?: HorizontalPodAutoscalerStatus

}
