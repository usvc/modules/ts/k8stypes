import { MetricValueStatus } from '../../autoscaling/v2beta2/MetricValueStatus';

/**
 * ResourceMetricStatus indicates the current value of a resource metric known to Kubernetes, as specified in requests and limits, describing each pod in the current scale target (e.g. CPU or memory).  Such metrics are built in to Kubernetes, and have special scaling options on top of those available to normal per-pod metrics using the "pods" source.
 * @interface ResourceMetricStatus
 * @see io.k8s.api.autoscaling.v2beta2.ResourceMetricStatus
 */
export interface ResourceMetricStatus {
  /**
   * current contains the current value for the given metric
   * @type MetricValueStatus
   */
  current : MetricValueStatus

  /**
   * Name is the name of the resource in question.
   * @type string
   */
  name : string

}
