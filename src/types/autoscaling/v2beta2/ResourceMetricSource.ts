import { MetricTarget } from '../../autoscaling/v2beta2/MetricTarget';

/**
 * ResourceMetricSource indicates how to scale on a resource metric known to Kubernetes, as specified in requests and limits, describing each pod in the current scale target (e.g. CPU or memory).  The values will be averaged together before being compared to the target.  Such metrics are built in to Kubernetes, and have special scaling options on top of those available to normal per-pod metrics using the "pods" source.  Only one "target" type should be set.
 * @interface ResourceMetricSource
 * @see io.k8s.api.autoscaling.v2beta2.ResourceMetricSource
 */
export interface ResourceMetricSource {
  /**
   * name is the name of the resource in question.
   * @type string
   */
  name : string

  /**
   * target specifies the target value for the given metric
   * @type MetricTarget
   */
  target : MetricTarget

}
