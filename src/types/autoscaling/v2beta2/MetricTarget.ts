import { Quantity } from '../../api/resource/Quantity';

/**
 * MetricTarget defines the target value, average value, or average utilization of a specific metric
 * @interface MetricTarget
 * @see io.k8s.api.autoscaling.v2beta2.MetricTarget
 */
export interface MetricTarget {
  /**
   * averageUtilization is the target value of the average of the resource metric across all relevant pods, represented as a percentage of the requested value of the resource for the pods. Currently only valid for Resource metric source type
   * @type number
   */
  averageUtilization ?: number

  /**
   * averageValue is the target value of the average of the metric across all relevant pods (as a quantity)
   * @type Quantity
   */
  averageValue ?: Quantity

  /**
   * type represents whether the metric type is Utilization, Value, or AverageValue
   * @type string
   */
  type : string

  /**
   * value is the target value of the metric (as a quantity).
   * @type Quantity
   */
  value ?: Quantity

}
