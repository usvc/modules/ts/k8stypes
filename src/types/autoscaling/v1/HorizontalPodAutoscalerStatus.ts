import { Time } from '../../meta/v1/Time';

/**
 * current status of a horizontal pod autoscaler
 * @interface HorizontalPodAutoscalerStatus
 * @see io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerStatus
 */
export interface HorizontalPodAutoscalerStatus {
  /**
   * current average CPU utilization over all pods, represented as a percentage of requested CPU, e.g. 70 means that an average pod is using now 70% of its requested CPU.
   * @type number
   */
  currentCPUUtilizationPercentage ?: number

  /**
   * current number of replicas of pods managed by this autoscaler.
   * @type number
   */
  currentReplicas : number

  /**
   * desired number of replicas of pods managed by this autoscaler.
   * @type number
   */
  desiredReplicas : number

  /**
   * last time the HorizontalPodAutoscaler scaled the number of pods; used by the autoscaler to control how often the number of pods is changed.
   * @type Time
   */
  lastScaleTime ?: Time

  /**
   * most recent generation observed by this autoscaler.
   * @type number
   */
  observedGeneration ?: number

}
