/**
 * CrossVersionObjectReference contains enough information to let you identify the referred resource.
 * @interface CrossVersionObjectReference
 * @see io.k8s.api.autoscaling.v1.CrossVersionObjectReference
 */
export interface CrossVersionObjectReference {
  /**
   * API version of the referent
   * @type string
   */
  apiVersion : "autoscaling/v1" | string

  /**
   * Kind of the referent; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds"
   * @type string
   */
  kind : string

  /**
   * Name of the referent; More info: http://kubernetes.io/docs/user-guide/identifiers#names
   * @type string
   */
  name : string

}
