/**
 * ScaleSpec describes the attributes of a scale subresource.
 * @interface ScaleSpec
 * @see io.k8s.api.autoscaling.v1.ScaleSpec
 */
export interface ScaleSpec {
  /**
   * desired number of instances for the scaled object.
   * @type number
   */
  replicas ?: number

}
