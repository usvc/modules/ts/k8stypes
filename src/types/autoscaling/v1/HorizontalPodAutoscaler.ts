import { HorizontalPodAutoscalerSpec } from '../../autoscaling/v1/HorizontalPodAutoscalerSpec';
import { HorizontalPodAutoscalerStatus } from '../../autoscaling/v1/HorizontalPodAutoscalerStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * configuration of a horizontal pod autoscaler.
 * @interface HorizontalPodAutoscaler
 * @see io.k8s.api.autoscaling.v1.HorizontalPodAutoscaler
 */
export interface HorizontalPodAutoscaler {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "autoscaling/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * behaviour of autoscaler. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status.
   * @type HorizontalPodAutoscalerSpec
   */
  spec ?: HorizontalPodAutoscalerSpec

  /**
   * current information about the autoscaler.
   * @type HorizontalPodAutoscalerStatus
   */
  status ?: HorizontalPodAutoscalerStatus

}
