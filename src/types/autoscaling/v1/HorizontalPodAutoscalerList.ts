import { HorizontalPodAutoscaler } from '../../autoscaling/v1/HorizontalPodAutoscaler';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * list of horizontal pod autoscaler objects.
 * @interface HorizontalPodAutoscalerList
 * @see io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerList
 */
export interface HorizontalPodAutoscalerList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "autoscaling/v1" | string

  /**
   * list of horizontal pod autoscaler objects.
   * @type HorizontalPodAutoscaler[]
   */
  items : HorizontalPodAutoscaler[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata.
   * @type ListMeta
   */
  metadata ?: ListMeta

}
