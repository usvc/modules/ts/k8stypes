import { CrossVersionObjectReference } from '../../autoscaling/v1/CrossVersionObjectReference';

/**
 * specification of a horizontal pod autoscaler.
 * @interface HorizontalPodAutoscalerSpec
 * @see io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerSpec
 */
export interface HorizontalPodAutoscalerSpec {
  /**
   * upper limit for the number of pods that can be set by the autoscaler; cannot be smaller than MinReplicas.
   * @type number
   */
  maxReplicas : number

  /**
   * lower limit for the number of pods that can be set by the autoscaler, default 1.
   * @type number
   */
  minReplicas ?: number

  /**
   * reference to scaled resource; horizontal pod autoscaler will learn the current resource consumption and will set the desired number of pods by using its Scale subresource.
   * @type CrossVersionObjectReference
   */
  scaleTargetRef : CrossVersionObjectReference

  /**
   * target average CPU utilization (represented as a percentage of requested CPU) over all the pods; if not specified the default autoscaling policy will be used.
   * @type number
   */
  targetCPUUtilizationPercentage ?: number

}
