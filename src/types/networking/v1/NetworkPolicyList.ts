import { ListMeta } from '../../meta/v1/ListMeta';
import { NetworkPolicy } from '../../networking/v1/NetworkPolicy';

/**
 * NetworkPolicyList is a list of NetworkPolicy objects.
 * @interface NetworkPolicyList
 * @see io.k8s.api.networking.v1.NetworkPolicyList
 */
export interface NetworkPolicyList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "networking/v1" | string

  /**
   * Items is a list of schema objects.
   * @type NetworkPolicy[]
   */
  items : NetworkPolicy[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
