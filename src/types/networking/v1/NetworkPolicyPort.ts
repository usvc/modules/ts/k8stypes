import { IntOrString } from '../../util/intstr/IntOrString';

/**
 * NetworkPolicyPort describes a port to allow traffic on
 * @interface NetworkPolicyPort
 * @see io.k8s.api.networking.v1.NetworkPolicyPort
 */
export interface NetworkPolicyPort {
  /**
   * The port on the given protocol. This can either be a numerical or named port on a pod. If this field is not provided, this matches all port names and numbers.
   * @type IntOrString
   */
  port ?: IntOrString

  /**
   * The protocol (TCP, UDP, or SCTP) which traffic must match. If not specified, this field defaults to TCP.
   * @type string
   */
  protocol ?: string

}
