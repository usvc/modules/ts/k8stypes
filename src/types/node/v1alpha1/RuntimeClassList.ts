import { ListMeta } from '../../meta/v1/ListMeta';
import { RuntimeClass } from '../../node/v1alpha1/RuntimeClass';

/**
 * RuntimeClassList is a list of RuntimeClass objects.
 * @interface RuntimeClassList
 * @see io.k8s.api.node.v1alpha1.RuntimeClassList
 */
export interface RuntimeClassList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "node/v1alpha1" | string

  /**
   * Items is a list of schema objects.
   * @type RuntimeClass[]
   */
  items : RuntimeClass[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
