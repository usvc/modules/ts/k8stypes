import { MutatingWebhook } from '../../admissionregistration/v1beta1/MutatingWebhook';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * MutatingWebhookConfiguration describes the configuration of and admission webhook that accept or reject and may change the object.
 * @interface MutatingWebhookConfiguration
 * @see io.k8s.api.admissionregistration.v1beta1.MutatingWebhookConfiguration
 */
export interface MutatingWebhookConfiguration {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "admissionregistration/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object metadata; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Webhooks is a list of webhooks and the affected resources and operations.
   * @type MutatingWebhook[]
   */
  webhooks ?: MutatingWebhook[]

}
