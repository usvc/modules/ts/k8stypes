import { ListMeta } from '../../meta/v1/ListMeta';
import { ValidatingWebhookConfiguration } from '../../admissionregistration/v1beta1/ValidatingWebhookConfiguration';

/**
 * ValidatingWebhookConfigurationList is a list of ValidatingWebhookConfiguration.
 * @interface ValidatingWebhookConfigurationList
 * @see io.k8s.api.admissionregistration.v1beta1.ValidatingWebhookConfigurationList
 */
export interface ValidatingWebhookConfigurationList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "admissionregistration/v1beta1" | string

  /**
   * List of ValidatingWebhookConfiguration.
   * @type ValidatingWebhookConfiguration[]
   */
  items : ValidatingWebhookConfiguration[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type ListMeta
   */
  metadata ?: ListMeta

}
