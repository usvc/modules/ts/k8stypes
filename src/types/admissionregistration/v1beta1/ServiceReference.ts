/**
 * ServiceReference holds a reference to Service.legacy.k8s.io
 * @interface ServiceReference
 * @see io.k8s.api.admissionregistration.v1beta1.ServiceReference
 */
export interface ServiceReference {
  /**
   * `name` is the name of the service. Required
   * @type string
   */
  name : string

  /**
   * `namespace` is the namespace of the service. Required
   * @type string
   */
  namespace : string

  /**
   * `path` is an optional URL path which will be sent in any request to this service.
   * @type string
   */
  path ?: string

  /**
   * If specified, the port on the service that hosting webhook. Default to 443 for backward compatibility. `port` should be a valid port number (1-65535, inclusive).
   * @type number
   */
  port ?: number

}
