import { Event } from '../../events/v1beta1/Event';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * EventList is a list of Event objects.
 * @interface EventList
 * @see io.k8s.api.events.v1beta1.EventList
 */
export interface EventList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "events/v1beta1" | string

  /**
   * Items is a list of schema objects.
   * @type Event[]
   */
  items : Event[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
