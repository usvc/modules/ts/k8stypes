import { MicroTime } from '../../meta/v1/MicroTime';

/**
 * EventSeries contain information on series of events, i.e. thing that was/is happening continuously for some time.
 * @interface EventSeries
 * @see io.k8s.api.events.v1beta1.EventSeries
 */
export interface EventSeries {
  /**
   * Number of occurrences in this series up to the last heartbeat time
   * @type number
   */
  count : number

  /**
   * Time when last Event from the series was seen before last heartbeat.
   * @type MicroTime
   */
  lastObservedTime : MicroTime

  /**
   * Information whether this series is ongoing or finished. Deprecated. Planned removal for 1.18
   * @type string
   */
  state : string

}
