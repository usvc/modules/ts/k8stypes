import { EventSeries } from '../../events/v1beta1/EventSeries';
import { EventSource } from '../../core/v1/EventSource';
import { MicroTime } from '../../meta/v1/MicroTime';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { ObjectReference } from '../../core/v1/ObjectReference';
import { Time } from '../../meta/v1/Time';

/**
 * Event is a report of an event somewhere in the cluster. It generally denotes some state change in the system.
 * @interface Event
 * @see io.k8s.api.events.v1beta1.Event
 */
export interface Event {
  /**
   * What action was taken/failed regarding to the regarding object.
   * @type string
   */
  action ?: string

  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "events/v1beta1" | string

  /**
   * Deprecated field assuring backward compatibility with core.v1 Event type
   * @type number
   */
  deprecatedCount ?: number

  /**
   * Deprecated field assuring backward compatibility with core.v1 Event type
   * @type Time
   */
  deprecatedFirstTimestamp ?: Time

  /**
   * Deprecated field assuring backward compatibility with core.v1 Event type
   * @type Time
   */
  deprecatedLastTimestamp ?: Time

  /**
   * Deprecated field assuring backward compatibility with core.v1 Event type
   * @type EventSource
   */
  deprecatedSource ?: EventSource

  /**
   * Required. Time when this Event was first observed.
   * @type MicroTime
   */
  eventTime : MicroTime

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Optional. A human-readable description of the status of this operation. Maximal length of the note is 1kB, but libraries should be prepared to handle values up to 64kB.
   * @type string
   */
  note ?: string

  /**
   * Why the action was taken.
   * @type string
   */
  reason ?: string

  /**
   * The object this Event is about. In most cases it's an Object reporting controller implements. E.g. ReplicaSetController implements ReplicaSets and this event is emitted because it acts on some changes in a ReplicaSet object.
   * @type ObjectReference
   */
  regarding ?: ObjectReference

  /**
   * Optional secondary object for more complex actions. E.g. when regarding object triggers a creation or deletion of related object.
   * @type ObjectReference
   */
  related ?: ObjectReference

  /**
   * Name of the controller that emitted this Event, e.g. `kubernetes.io/kubelet`.
   * @type string
   */
  reportingController ?: string

  /**
   * ID of the controller instance, e.g. `kubelet-xyzf`.
   * @type string
   */
  reportingInstance ?: string

  /**
   * Data about the Event series this event represents or nil if it's a singleton Event.
   * @type EventSeries
   */
  series ?: EventSeries

  /**
   * Type of this event (Normal, Warning), new types could be added in the future.
   * @type string
   */
  type ?: string

}
