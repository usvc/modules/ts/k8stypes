import { NonResourceAttributes } from '../../authorization/v1/NonResourceAttributes';
import { ResourceAttributes } from '../../authorization/v1/ResourceAttributes';

/**
 * SubjectAccessReviewSpec is a description of the access request.  Exactly one of ResourceAuthorizationAttributes and NonResourceAuthorizationAttributes must be set
 * @interface SubjectAccessReviewSpec
 * @see io.k8s.api.authorization.v1.SubjectAccessReviewSpec
 */
export interface SubjectAccessReviewSpec {
  /**
   * Extra corresponds to the user.Info.GetExtra() method from the authenticator.  Since that is input to the authorizer it needs a reflection here.
   * @type {[key : string] : any}
   */
  extra ?: {[key : string] : any}

  /**
   * Groups is the groups you're testing for.
   * @type string[]
   */
  groups ?: string[]

  /**
   * NonResourceAttributes describes information for a non-resource access request
   * @type NonResourceAttributes
   */
  nonResourceAttributes ?: NonResourceAttributes

  /**
   * ResourceAuthorizationAttributes describes information for a resource access request
   * @type ResourceAttributes
   */
  resourceAttributes ?: ResourceAttributes

  /**
   * UID information about the requesting user.
   * @type string
   */
  uid ?: string

  /**
   * User is the user you're testing for. If you specify "User" but not "Groups", then is it interpreted as "What if User were not a member of any groups
   * @type string
   */
  user ?: string

}
