import { NonResourceAttributes } from '../../authorization/v1/NonResourceAttributes';
import { ResourceAttributes } from '../../authorization/v1/ResourceAttributes';

/**
 * SelfSubjectAccessReviewSpec is a description of the access request.  Exactly one of ResourceAuthorizationAttributes and NonResourceAuthorizationAttributes must be set
 * @interface SelfSubjectAccessReviewSpec
 * @see io.k8s.api.authorization.v1.SelfSubjectAccessReviewSpec
 */
export interface SelfSubjectAccessReviewSpec {
  /**
   * NonResourceAttributes describes information for a non-resource access request
   * @type NonResourceAttributes
   */
  nonResourceAttributes ?: NonResourceAttributes

  /**
   * ResourceAuthorizationAttributes describes information for a resource access request
   * @type ResourceAttributes
   */
  resourceAttributes ?: ResourceAttributes

}
