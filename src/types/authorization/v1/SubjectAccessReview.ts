import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { SubjectAccessReviewSpec } from '../../authorization/v1/SubjectAccessReviewSpec';
import { SubjectAccessReviewStatus } from '../../authorization/v1/SubjectAccessReviewStatus';

/**
 * SubjectAccessReview checks whether or not a user or group can perform an action.
 * @interface SubjectAccessReview
 * @see io.k8s.api.authorization.v1.SubjectAccessReview
 */
export interface SubjectAccessReview {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "authorization/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec holds information about the request being evaluated
   * @type SubjectAccessReviewSpec
   */
  spec : SubjectAccessReviewSpec

  /**
   * Status is filled in by the server and indicates whether the request is allowed or not
   * @type SubjectAccessReviewStatus
   */
  status ?: SubjectAccessReviewStatus

}
