/**
 * undefined
 * @interface SelfSubjectRulesReviewSpec
 * @see io.k8s.api.authorization.v1.SelfSubjectRulesReviewSpec
 */
export interface SelfSubjectRulesReviewSpec {
  /**
   * Namespace to evaluate rules for. Required.
   * @type string
   */
  namespace ?: string

}
