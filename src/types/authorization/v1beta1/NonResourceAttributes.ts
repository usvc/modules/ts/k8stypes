/**
 * NonResourceAttributes includes the authorization attributes available for non-resource requests to the Authorizer interface
 * @interface NonResourceAttributes
 * @see io.k8s.api.authorization.v1beta1.NonResourceAttributes
 */
export interface NonResourceAttributes {
  /**
   * Path is the URL path of the request
   * @type string
   */
  path ?: string

  /**
   * Verb is the standard HTTP verb
   * @type string
   */
  verb ?: string

}
