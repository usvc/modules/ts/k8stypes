/**
 * NonResourceRule holds information that describes a rule for the non-resource
 * @interface NonResourceRule
 * @see io.k8s.api.authorization.v1beta1.NonResourceRule
 */
export interface NonResourceRule {
  /**
   * NonResourceURLs is a set of partial urls that a user should have access to.  *s are allowed, but only as the full, final step in the path.  "*" means all.
   * @type string[]
   */
  nonResourceURLs ?: string[]

  /**
   * Verb is a list of kubernetes non-resource API verbs, like: get, post, put, delete, patch, head, options.  "*" means all.
   * @type string[]
   */
  verbs : string[]

}
