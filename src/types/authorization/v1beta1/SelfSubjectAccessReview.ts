import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { SelfSubjectAccessReviewSpec } from '../../authorization/v1beta1/SelfSubjectAccessReviewSpec';
import { SubjectAccessReviewStatus } from '../../authorization/v1beta1/SubjectAccessReviewStatus';

/**
 * SelfSubjectAccessReview checks whether or the current user can perform an action.  Not filling in a spec.namespace means "in all namespaces".  Self is a special case, because users should always be able to check whether they can perform an action
 * @interface SelfSubjectAccessReview
 * @see io.k8s.api.authorization.v1beta1.SelfSubjectAccessReview
 */
export interface SelfSubjectAccessReview {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "authorization/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec holds information about the request being evaluated.  user and groups must be empty
   * @type SelfSubjectAccessReviewSpec
   */
  spec : SelfSubjectAccessReviewSpec

  /**
   * Status is filled in by the server and indicates whether the request is allowed or not
   * @type SubjectAccessReviewStatus
   */
  status ?: SubjectAccessReviewStatus

}
