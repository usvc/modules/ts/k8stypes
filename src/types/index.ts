

/**
 * Version of Kubernetes this set of definitions was created from
 * @type string
 */
export const VERSION = 'v1.15.2';
