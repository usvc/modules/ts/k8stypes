import { ListMeta } from '../../meta/v1/ListMeta';
import { PodPreset } from '../../settings/v1alpha1/PodPreset';

/**
 * PodPresetList is a list of PodPreset objects.
 * @interface PodPresetList
 * @see io.k8s.api.settings.v1alpha1.PodPresetList
 */
export interface PodPresetList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "settings/v1alpha1" | string

  /**
   * Items is a list of schema objects.
   * @type PodPreset[]
   */
  items : PodPreset[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
