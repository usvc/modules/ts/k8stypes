import { EnvFromSource } from '../../core/v1/EnvFromSource';
import { EnvVar } from '../../core/v1/EnvVar';
import { LabelSelector } from '../../meta/v1/LabelSelector';
import { Volume } from '../../core/v1/Volume';
import { VolumeMount } from '../../core/v1/VolumeMount';

/**
 * PodPresetSpec is a description of a pod preset.
 * @interface PodPresetSpec
 * @see io.k8s.api.settings.v1alpha1.PodPresetSpec
 */
export interface PodPresetSpec {
  /**
   * Env defines the collection of EnvVar to inject into containers.
   * @type EnvVar[]
   */
  env ?: EnvVar[]

  /**
   * EnvFrom defines the collection of EnvFromSource to inject into containers.
   * @type EnvFromSource[]
   */
  envFrom ?: EnvFromSource[]

  /**
   * Selector is a label query over a set of resources, in this case pods. Required.
   * @type LabelSelector
   */
  selector ?: LabelSelector

  /**
   * VolumeMounts defines the collection of VolumeMount to inject into containers.
   * @type VolumeMount[]
   */
  volumeMounts ?: VolumeMount[]

  /**
   * Volumes defines the collection of Volume to inject into the pod.
   * @type Volume[]
   */
  volumes ?: Volume[]

}
