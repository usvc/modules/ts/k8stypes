import { RollingUpdateDaemonSet } from '../../extensions/v1beta1/RollingUpdateDaemonSet';

/**
 * undefined
 * @interface DaemonSetUpdateStrategy
 * @see io.k8s.api.extensions.v1beta1.DaemonSetUpdateStrategy
 */
export interface DaemonSetUpdateStrategy {
  /**
   * Rolling update config params. Present only if type = "RollingUpdate".
   * @type RollingUpdateDaemonSet
   */
  rollingUpdate ?: RollingUpdateDaemonSet

  /**
   * Type of daemon set update. Can be "RollingUpdate" or "OnDelete". Default is OnDelete.
   * @type string
   */
  type ?: string

}
