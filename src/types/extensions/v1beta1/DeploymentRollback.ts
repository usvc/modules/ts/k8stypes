import { RollbackConfig } from '../../extensions/v1beta1/RollbackConfig';

/**
 * DEPRECATED. DeploymentRollback stores the information required to rollback a deployment.
 * @interface DeploymentRollback
 * @see io.k8s.api.extensions.v1beta1.DeploymentRollback
 */
export interface DeploymentRollback {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "extensions/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Required: This must match the Name of a deployment.
   * @type string
   */
  name : string

  /**
   * The config of this deployment rollback.
   * @type RollbackConfig
   */
  rollbackTo : RollbackConfig

  /**
   * The annotations to be updated to a deployment
   * @type {[key : string] : string}
   */
  updatedAnnotations ?: {[key : string] : string}

}
