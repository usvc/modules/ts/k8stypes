import { IPBlock } from '../../extensions/v1beta1/IPBlock';
import { LabelSelector } from '../../meta/v1/LabelSelector';

/**
 * DEPRECATED 1.9 - This group version of NetworkPolicyPeer is deprecated by networking/v1/NetworkPolicyPeer.
 * @interface NetworkPolicyPeer
 * @see io.k8s.api.extensions.v1beta1.NetworkPolicyPeer
 */
export interface NetworkPolicyPeer {
  /**
   * IPBlock defines policy on a particular IPBlock. If this field is set then neither of the other fields can be.
   * @type IPBlock
   */
  ipBlock ?: IPBlock

  /**
   * Selects Namespaces using cluster-scoped labels. This field follows standard label selector semantics; if present but empty, it selects all namespaces.  If PodSelector is also set, then the NetworkPolicyPeer as a whole selects the Pods matching PodSelector in the Namespaces selected by NamespaceSelector. Otherwise it selects all Pods in the Namespaces selected by NamespaceSelector.
   * @type LabelSelector
   */
  namespaceSelector ?: LabelSelector

  /**
   * This is a label selector which selects Pods. This field follows standard label selector semantics; if present but empty, it selects all pods.  If NamespaceSelector is also set, then the NetworkPolicyPeer as a whole selects the Pods matching PodSelector in the Namespaces selected by NamespaceSelector. Otherwise it selects the Pods matching PodSelector in the policy's own Namespace.
   * @type LabelSelector
   */
  podSelector ?: LabelSelector

}
