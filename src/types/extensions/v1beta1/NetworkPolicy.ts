import { NetworkPolicySpec } from '../../extensions/v1beta1/NetworkPolicySpec';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * DEPRECATED 1.9 - This group version of NetworkPolicy is deprecated by networking/v1/NetworkPolicy. NetworkPolicy describes what network traffic is allowed for a set of Pods
 * @interface NetworkPolicy
 * @see io.k8s.api.extensions.v1beta1.NetworkPolicy
 */
export interface NetworkPolicy {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "extensions/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior for this NetworkPolicy.
   * @type NetworkPolicySpec
   */
  spec ?: NetworkPolicySpec

}
