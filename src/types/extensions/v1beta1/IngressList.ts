import { Ingress } from '../../extensions/v1beta1/Ingress';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * IngressList is a collection of Ingress.
 * @interface IngressList
 * @see io.k8s.api.extensions.v1beta1.IngressList
 */
export interface IngressList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "extensions/v1beta1" | string

  /**
   * Items is the list of Ingress.
   * @type Ingress[]
   */
  items : Ingress[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
