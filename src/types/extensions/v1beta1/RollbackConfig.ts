/**
 * DEPRECATED.
 * @interface RollbackConfig
 * @see io.k8s.api.extensions.v1beta1.RollbackConfig
 */
export interface RollbackConfig {
  /**
   * The revision to rollback to. If set to 0, rollback to the last revision.
   * @type number
   */
  revision ?: number

}
