import { Time } from '../../meta/v1/Time';

/**
 * DeploymentCondition describes the state of a deployment at a certain point.
 * @interface DeploymentCondition
 * @see io.k8s.api.extensions.v1beta1.DeploymentCondition
 */
export interface DeploymentCondition {
  /**
   * Last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * The last time this condition was updated.
   * @type Time
   */
  lastUpdateTime ?: Time

  /**
   * A human readable message indicating details about the transition.
   * @type string
   */
  message ?: string

  /**
   * The reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status of the condition, one of True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type of deployment condition.
   * @type string
   */
  type : string

}
