import { LoadBalancerStatus } from '../../core/v1/LoadBalancerStatus';

/**
 * IngressStatus describe the current state of the Ingress.
 * @interface IngressStatus
 * @see io.k8s.api.extensions.v1beta1.IngressStatus
 */
export interface IngressStatus {
  /**
   * LoadBalancer contains the current status of the load-balancer.
   * @type LoadBalancerStatus
   */
  loadBalancer ?: LoadBalancerStatus

}
