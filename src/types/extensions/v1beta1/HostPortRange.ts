/**
 * HostPortRange defines a range of host ports that will be enabled by a policy for pods to use.  It requires both the start and end to be defined. Deprecated: use HostPortRange from policy API Group instead.
 * @interface HostPortRange
 * @see io.k8s.api.extensions.v1beta1.HostPortRange
 */
export interface HostPortRange {
  /**
   * max is the end of the range, inclusive.
   * @type number
   */
  max : number

  /**
   * min is the start of the range, inclusive.
   * @type number
   */
  min : number

}
