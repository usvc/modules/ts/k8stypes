import { RollingUpdateDeployment } from '../../extensions/v1beta1/RollingUpdateDeployment';

/**
 * DeploymentStrategy describes how to replace existing pods with new ones.
 * @interface DeploymentStrategy
 * @see io.k8s.api.extensions.v1beta1.DeploymentStrategy
 */
export interface DeploymentStrategy {
  /**
   * Rolling update config params. Present only if DeploymentStrategyType = RollingUpdate.
   * @type RollingUpdateDeployment
   */
  rollingUpdate ?: RollingUpdateDeployment

  /**
   * Type of deployment. Can be "Recreate" or "RollingUpdate". Default is RollingUpdate.
   * @type string
   */
  type ?: string

}
