import { IDRange } from '../../extensions/v1beta1/IDRange';

/**
 * RunAsGroupStrategyOptions defines the strategy type and any options used to create the strategy. Deprecated: use RunAsGroupStrategyOptions from policy API Group instead.
 * @interface RunAsGroupStrategyOptions
 * @see io.k8s.api.extensions.v1beta1.RunAsGroupStrategyOptions
 */
export interface RunAsGroupStrategyOptions {
  /**
   * ranges are the allowed ranges of gids that may be used. If you would like to force a single gid then supply a single range with the same start and end. Required for MustRunAs.
   * @type IDRange[]
   */
  ranges ?: IDRange[]

  /**
   * rule is the strategy that will dictate the allowable RunAsGroup values that may be set.
   * @type string
   */
  rule : string

}
