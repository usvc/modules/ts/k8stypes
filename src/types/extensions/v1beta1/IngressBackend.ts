import { IntOrString } from '../../util/intstr/IntOrString';

/**
 * IngressBackend describes all endpoints for a given service and port.
 * @interface IngressBackend
 * @see io.k8s.api.extensions.v1beta1.IngressBackend
 */
export interface IngressBackend {
  /**
   * Specifies the name of the referenced service.
   * @type string
   */
  serviceName : string

  /**
   * Specifies the port of the referenced service.
   * @type IntOrString
   */
  servicePort : IntOrString

}
