import { SELinuxOptions } from '../../core/v1/SELinuxOptions';

/**
 * SELinuxStrategyOptions defines the strategy type and any options used to create the strategy. Deprecated: use SELinuxStrategyOptions from policy API Group instead.
 * @interface SELinuxStrategyOptions
 * @see io.k8s.api.extensions.v1beta1.SELinuxStrategyOptions
 */
export interface SELinuxStrategyOptions {
  /**
   * rule is the strategy that will dictate the allowable labels that may be set.
   * @type string
   */
  rule : string

  /**
   * seLinuxOptions required to run as; required for MustRunAs More info: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
   * @type SELinuxOptions
   */
  seLinuxOptions ?: SELinuxOptions

}
