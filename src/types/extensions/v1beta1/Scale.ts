import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { ScaleSpec } from '../../extensions/v1beta1/ScaleSpec';
import { ScaleStatus } from '../../extensions/v1beta1/ScaleStatus';

/**
 * represents a scaling request for a resource.
 * @interface Scale
 * @see io.k8s.api.extensions.v1beta1.Scale
 */
export interface Scale {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "extensions/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object metadata; More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * defines the behavior of the scale. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status.
   * @type ScaleSpec
   */
  spec ?: ScaleSpec

  /**
   * current status of the scale. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status. Read-only.
   * @type ScaleStatus
   */
  status ?: ScaleStatus

}
