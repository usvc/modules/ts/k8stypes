/**
 * AllowedFlexVolume represents a single Flexvolume that is allowed to be used. Deprecated: use AllowedFlexVolume from policy API Group instead.
 * @interface AllowedFlexVolume
 * @see io.k8s.api.extensions.v1beta1.AllowedFlexVolume
 */
export interface AllowedFlexVolume {
  /**
   * driver is the name of the Flexvolume driver.
   * @type string
   */
  driver : string

}
