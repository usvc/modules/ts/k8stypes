/**
 * AllowedCSIDriver represents a single inline CSI Driver that is allowed to be used.
 * @interface AllowedCSIDriver
 * @see io.k8s.api.extensions.v1beta1.AllowedCSIDriver
 */
export interface AllowedCSIDriver {
  /**
   * Name is the registered name of the CSI driver
   * @type string
   */
  name : string

}
