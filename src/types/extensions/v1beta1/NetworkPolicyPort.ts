import { IntOrString } from '../../util/intstr/IntOrString';

/**
 * DEPRECATED 1.9 - This group version of NetworkPolicyPort is deprecated by networking/v1/NetworkPolicyPort.
 * @interface NetworkPolicyPort
 * @see io.k8s.api.extensions.v1beta1.NetworkPolicyPort
 */
export interface NetworkPolicyPort {
  /**
   * If specified, the port on the given protocol.  This can either be a numerical or named port on a pod.  If this field is not provided, this matches all port names and numbers. If present, only traffic on the specified protocol AND port will be matched.
   * @type IntOrString
   */
  port ?: IntOrString

  /**
   * Optional.  The protocol (TCP, UDP, or SCTP) which traffic must match. If not specified, this field defaults to TCP.
   * @type string
   */
  protocol ?: string

}
