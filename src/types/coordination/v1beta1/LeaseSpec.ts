import { MicroTime } from '../../meta/v1/MicroTime';

/**
 * LeaseSpec is a specification of a Lease.
 * @interface LeaseSpec
 * @see io.k8s.api.coordination.v1beta1.LeaseSpec
 */
export interface LeaseSpec {
  /**
   * acquireTime is a time when the current lease was acquired.
   * @type MicroTime
   */
  acquireTime ?: MicroTime

  /**
   * holderIdentity contains the identity of the holder of a current lease.
   * @type string
   */
  holderIdentity ?: string

  /**
   * leaseDurationSeconds is a duration that candidates for a lease need to wait to force acquire it. This is measure against time of last observed RenewTime.
   * @type number
   */
  leaseDurationSeconds ?: number

  /**
   * leaseTransitions is the number of transitions of a lease between holders.
   * @type number
   */
  leaseTransitions ?: number

  /**
   * renewTime is a time when the current holder of a lease has last updated the lease.
   * @type MicroTime
   */
  renewTime ?: MicroTime

}
