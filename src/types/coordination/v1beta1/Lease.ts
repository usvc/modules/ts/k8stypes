import { LeaseSpec } from '../../coordination/v1beta1/LeaseSpec';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * Lease defines a lease concept.
 * @interface Lease
 * @see io.k8s.api.coordination.v1beta1.Lease
 */
export interface Lease {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "coordination/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the Lease. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type LeaseSpec
   */
  spec ?: LeaseSpec

}
