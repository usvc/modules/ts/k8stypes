import { Lease } from '../../coordination/v1beta1/Lease';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * LeaseList is a list of Lease objects.
 * @interface LeaseList
 * @see io.k8s.api.coordination.v1beta1.LeaseList
 */
export interface LeaseList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "coordination/v1beta1" | string

  /**
   * Items is a list of schema objects.
   * @type Lease[]
   */
  items : Lease[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
