/**
 * This information is immutable after the request is created. Only the Request and Usages fields can be set on creation, other fields are derived by Kubernetes and cannot be modified by users.
 * @interface CertificateSigningRequestSpec
 * @see io.k8s.api.certificates.v1beta1.CertificateSigningRequestSpec
 */
export interface CertificateSigningRequestSpec {
  /**
   * Extra information about the requesting user. See user.Info interface for details.
   * @type {[key : string] : any}
   */
  extra ?: {[key : string] : any}

  /**
   * Group information about the requesting user. See user.Info interface for details.
   * @type string[]
   */
  groups ?: string[]

  /**
   * Base64-encoded PKCS#10 CSR data
   * @type string
   */
  request : string

  /**
   * UID information about the requesting user. See user.Info interface for details.
   * @type string
   */
  uid ?: string

  /**
   * allowedUsages specifies a set of usage contexts the key will be valid for. See: https://tools.ietf.org/html/rfc5280#section-4.2.1.3      https://tools.ietf.org/html/rfc5280#section-4.2.1.12
   * @type string[]
   */
  usages ?: string[]

  /**
   * Information about the requesting user. See user.Info interface for details.
   * @type string
   */
  username ?: string

}
