import { Time } from '../../meta/v1/Time';

/**
 * undefined
 * @interface CertificateSigningRequestCondition
 * @see io.k8s.api.certificates.v1beta1.CertificateSigningRequestCondition
 */
export interface CertificateSigningRequestCondition {
  /**
   * timestamp for the last update to this condition
   * @type Time
   */
  lastUpdateTime ?: Time

  /**
   * human readable message with details about the request state
   * @type string
   */
  message ?: string

  /**
   * brief reason for the request state
   * @type string
   */
  reason ?: string

  /**
   * request approval state, currently Approved or Denied.
   * @type string
   */
  type : string

}
