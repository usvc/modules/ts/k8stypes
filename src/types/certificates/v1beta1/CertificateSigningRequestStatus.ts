import { CertificateSigningRequestCondition } from '../../certificates/v1beta1/CertificateSigningRequestCondition';

/**
 * undefined
 * @interface CertificateSigningRequestStatus
 * @see io.k8s.api.certificates.v1beta1.CertificateSigningRequestStatus
 */
export interface CertificateSigningRequestStatus {
  /**
   * If request was approved, the controller will place the issued certificate here.
   * @type string
   */
  certificate ?: string

  /**
   * Conditions applied to the request, such as approval or denial.
   * @type CertificateSigningRequestCondition[]
   */
  conditions ?: CertificateSigningRequestCondition[]

}
