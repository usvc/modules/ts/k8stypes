import { CertificateSigningRequestSpec } from '../../certificates/v1beta1/CertificateSigningRequestSpec';
import { CertificateSigningRequestStatus } from '../../certificates/v1beta1/CertificateSigningRequestStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * Describes a certificate signing request
 * @interface CertificateSigningRequest
 * @see io.k8s.api.certificates.v1beta1.CertificateSigningRequest
 */
export interface CertificateSigningRequest {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "certificates/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * The certificate request itself and any additional information.
   * @type CertificateSigningRequestSpec
   */
  spec ?: CertificateSigningRequestSpec

  /**
   * Derived information about the request.
   * @type CertificateSigningRequestStatus
   */
  status ?: CertificateSigningRequestStatus

}
