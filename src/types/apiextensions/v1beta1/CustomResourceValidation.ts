import { JSONSchemaProps } from '../../apiextensions/v1beta1/JSONSchemaProps';

/**
 * CustomResourceValidation is a list of validation methods for CustomResources.
 * @interface CustomResourceValidation
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceValidation
 */
export interface CustomResourceValidation {
  /**
   * OpenAPIV3Schema is the OpenAPI v3 schema to be validated against.
   * @type JSONSchemaProps
   */
  openAPIV3Schema ?: JSONSchemaProps

}
