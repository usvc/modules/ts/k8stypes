import { ExternalDocumentation } from '../../apiextensions/v1beta1/ExternalDocumentation';
import { JSON } from '../../apiextensions/v1beta1/JSON';
import { JSONSchemaProps } from '../../apiextensions/v1beta1/JSONSchemaProps';
import { JSONSchemaPropsOrArray } from '../../apiextensions/v1beta1/JSONSchemaPropsOrArray';
import { JSONSchemaPropsOrBool } from '../../apiextensions/v1beta1/JSONSchemaPropsOrBool';
import { JSONSchemaPropsOrStringArray } from '../../apiextensions/v1beta1/JSONSchemaPropsOrStringArray';

/**
 * JSONSchemaProps is a JSON-Schema following Specification Draft 4 (http://json-schema.org/).
 * @interface JSONSchemaProps
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaProps
 */
export interface JSONSchemaProps {
  /**
   * no description provided
   * @type string
   */
  $ref ?: string

  /**
   * no description provided
   * @type string
   */
  $schema ?: string

  /**
   * no description provided
   * @type JSONSchemaPropsOrBool
   */
  additionalItems ?: JSONSchemaPropsOrBool

  /**
   * no description provided
   * @type JSONSchemaPropsOrBool
   */
  additionalProperties ?: JSONSchemaPropsOrBool

  /**
   * no description provided
   * @type JSONSchemaProps[]
   */
  allOf ?: JSONSchemaProps[]

  /**
   * no description provided
   * @type JSONSchemaProps[]
   */
  anyOf ?: JSONSchemaProps[]

  /**
   * default is a default value for undefined object fields. Defaulting is an alpha feature under the CustomResourceDefaulting feature gate. Defaulting requires spec.preserveUnknownFields to be false.
   * @type JSON
   */
  default ?: JSON

  /**
   * no description provided
   * @type {[key : string] : JSONSchemaProps}
   */
  definitions ?: {[key : string] : JSONSchemaProps}

  /**
   * no description provided
   * @type {[key : string] : JSONSchemaPropsOrStringArray}
   */
  dependencies ?: {[key : string] : JSONSchemaPropsOrStringArray}

  /**
   * no description provided
   * @type string
   */
  description ?: string

  /**
   * no description provided
   * @type JSON[]
   */
  enum ?: JSON[]

  /**
   * no description provided
   * @type JSON
   */
  example ?: JSON

  /**
   * no description provided
   * @type boolean
   */
  exclusiveMaximum ?: boolean

  /**
   * no description provided
   * @type boolean
   */
  exclusiveMinimum ?: boolean

  /**
   * no description provided
   * @type ExternalDocumentation
   */
  externalDocs ?: ExternalDocumentation

  /**
   * no description provided
   * @type string
   */
  format ?: string

  /**
   * no description provided
   * @type string
   */
  id ?: string

  /**
   * no description provided
   * @type JSONSchemaPropsOrArray
   */
  items ?: JSONSchemaPropsOrArray

  /**
   * no description provided
   * @type number
   */
  maxItems ?: number

  /**
   * no description provided
   * @type number
   */
  maxLength ?: number

  /**
   * no description provided
   * @type number
   */
  maxProperties ?: number

  /**
   * no description provided
   * @type number
   */
  maximum ?: number

  /**
   * no description provided
   * @type number
   */
  minItems ?: number

  /**
   * no description provided
   * @type number
   */
  minLength ?: number

  /**
   * no description provided
   * @type number
   */
  minProperties ?: number

  /**
   * no description provided
   * @type number
   */
  minimum ?: number

  /**
   * no description provided
   * @type number
   */
  multipleOf ?: number

  /**
   * no description provided
   * @type JSONSchemaProps
   */
  not ?: JSONSchemaProps

  /**
   * no description provided
   * @type boolean
   */
  nullable ?: boolean

  /**
   * no description provided
   * @type JSONSchemaProps[]
   */
  oneOf ?: JSONSchemaProps[]

  /**
   * no description provided
   * @type string
   */
  pattern ?: string

  /**
   * no description provided
   * @type {[key : string] : JSONSchemaProps}
   */
  patternProperties ?: {[key : string] : JSONSchemaProps}

  /**
   * no description provided
   * @type {[key : string] : JSONSchemaProps}
   */
  properties ?: {[key : string] : JSONSchemaProps}

  /**
   * no description provided
   * @type string[]
   */
  required ?: string[]

  /**
   * no description provided
   * @type string
   */
  title ?: string

  /**
   * no description provided
   * @type string
   */
  type ?: string

  /**
   * no description provided
   * @type boolean
   */
  uniqueItems ?: boolean

  /**
   * x-kubernetes-embedded-resource defines that the value is an embedded Kubernetes runtime.Object, with TypeMeta and ObjectMeta. The type must be object. It is allowed to further restrict the embedded object. kind, apiVersion and metadata are validated automatically. x-kubernetes-preserve-unknown-fields is allowed to be true, but does not have to be if the object is fully specified (up to kind, apiVersion, metadata).
   * @type boolean
   */
  "x-kubernetes-embedded-resource" ?: boolean

  /**
   * x-kubernetes-int-or-string specifies that this value is either an integer or a string. If this is true, an empty type is allowed and type as child of anyOf is permitted if following one of the following patterns:  1) anyOf:    - type: integer    - type: string 2) allOf:    - anyOf:      - type: integer      - type: string    - ... zero or more
   * @type boolean
   */
  "x-kubernetes-int-or-string" ?: boolean

  /**
   * x-kubernetes-preserve-unknown-fields stops the API server decoding step from pruning fields which are not specified in the validation schema. This affects fields recursively, but switches back to normal pruning behaviour if nested properties or additionalProperties are specified in the schema. This can either be true or undefined. False is forbidden.
   * @type boolean
   */
  "x-kubernetes-preserve-unknown-fields" ?: boolean

}
