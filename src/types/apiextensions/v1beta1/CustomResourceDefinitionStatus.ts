import { CustomResourceDefinitionCondition } from '../../apiextensions/v1beta1/CustomResourceDefinitionCondition';
import { CustomResourceDefinitionNames } from '../../apiextensions/v1beta1/CustomResourceDefinitionNames';

/**
 * CustomResourceDefinitionStatus indicates the state of the CustomResourceDefinition
 * @interface CustomResourceDefinitionStatus
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceDefinitionStatus
 */
export interface CustomResourceDefinitionStatus {
  /**
   * AcceptedNames are the names that are actually being used to serve discovery They may be different than the names in spec.
   * @type CustomResourceDefinitionNames
   */
  acceptedNames : CustomResourceDefinitionNames

  /**
   * Conditions indicate state for particular aspects of a CustomResourceDefinition
   * @type CustomResourceDefinitionCondition[]
   */
  conditions : CustomResourceDefinitionCondition[]

  /**
   * StoredVersions are all versions of CustomResources that were ever persisted. Tracking these versions allows a migration path for stored versions in etcd. The field is mutable so the migration controller can first finish a migration to another version (i.e. that no old objects are left in the storage), and then remove the rest of the versions from this list. None of the versions in this list can be removed from the spec.Versions field.
   * @type string[]
   */
  storedVersions : string[]

}
