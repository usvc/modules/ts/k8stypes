/**
 * JSONSchemaPropsOrBool represents JSONSchemaProps or a boolean value. Defaults to true for the boolean property.
 * @type JSONSchemaPropsOrBool
 */
export type JSONSchemaPropsOrBool = any;
