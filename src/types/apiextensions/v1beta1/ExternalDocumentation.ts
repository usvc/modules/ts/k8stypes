/**
 * ExternalDocumentation allows referencing an external resource for extended documentation.
 * @interface ExternalDocumentation
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.ExternalDocumentation
 */
export interface ExternalDocumentation {
  /**
   * no description provided
   * @type string
   */
  description ?: string

  /**
   * no description provided
   * @type string
   */
  url ?: string

}
