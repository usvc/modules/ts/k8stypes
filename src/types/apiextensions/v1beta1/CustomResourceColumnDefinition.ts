/**
 * CustomResourceColumnDefinition specifies a column for server side printing.
 * @interface CustomResourceColumnDefinition
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceColumnDefinition
 */
export interface CustomResourceColumnDefinition {
  /**
   * JSONPath is a simple JSON path, i.e. with array notation.
   * @type string
   */
  JSONPath : string

  /**
   * description is a human readable description of this column.
   * @type string
   */
  description ?: string

  /**
   * format is an optional OpenAPI type definition for this column. The 'name' format is applied to the primary identifier column to assist in clients identifying column is the resource name. See https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#data-types for more.
   * @type string
   */
  format ?: string

  /**
   * name is a human readable name for the column.
   * @type string
   */
  name : string

  /**
   * priority is an integer defining the relative importance of this column compared to others. Lower numbers are considered higher priority. Columns that may be omitted in limited space scenarios should be given a higher priority.
   * @type number
   */
  priority ?: number

  /**
   * type is an OpenAPI type definition for this column. See https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#data-types for more.
   * @type string
   */
  type : string

}
