import { CustomResourceSubresourceScale } from '../../apiextensions/v1beta1/CustomResourceSubresourceScale';
import { CustomResourceSubresourceStatus } from '../../apiextensions/v1beta1/CustomResourceSubresourceStatus';

/**
 * CustomResourceSubresources defines the status and scale subresources for CustomResources.
 * @interface CustomResourceSubresources
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceSubresources
 */
export interface CustomResourceSubresources {
  /**
   * Scale denotes the scale subresource for CustomResources
   * @type CustomResourceSubresourceScale
   */
  scale ?: CustomResourceSubresourceScale

  /**
   * Status denotes the status subresource for CustomResources
   * @type CustomResourceSubresourceStatus
   */
  status ?: CustomResourceSubresourceStatus

}
