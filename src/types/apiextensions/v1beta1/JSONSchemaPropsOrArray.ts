/**
 * JSONSchemaPropsOrArray represents a value that can either be a JSONSchemaProps or an array of JSONSchemaProps. Mainly here for serialization purposes.
 * @type JSONSchemaPropsOrArray
 */
export type JSONSchemaPropsOrArray = any;
