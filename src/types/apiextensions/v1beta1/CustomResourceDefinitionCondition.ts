import { Time } from '../../meta/v1/Time';

/**
 * CustomResourceDefinitionCondition contains details for the current condition of this pod.
 * @interface CustomResourceDefinitionCondition
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceDefinitionCondition
 */
export interface CustomResourceDefinitionCondition {
  /**
   * Last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * Human-readable message indicating details about last transition.
   * @type string
   */
  message ?: string

  /**
   * Unique, one-word, CamelCase reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status is the status of the condition. Can be True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type is the type of the condition. Types include Established, NamesAccepted and Terminating.
   * @type string
   */
  type : string

}
