import { CustomResourceDefinition } from '../../apiextensions/v1beta1/CustomResourceDefinition';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * CustomResourceDefinitionList is a list of CustomResourceDefinition objects.
 * @interface CustomResourceDefinitionList
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceDefinitionList
 */
export interface CustomResourceDefinitionList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apiextensions/v1beta1" | string

  /**
   * Items individual CustomResourceDefinitions
   * @type CustomResourceDefinition[]
   */
  items : CustomResourceDefinition[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ListMeta
   */
  metadata ?: ListMeta

}
