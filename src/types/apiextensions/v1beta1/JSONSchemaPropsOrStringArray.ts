/**
 * JSONSchemaPropsOrStringArray represents a JSONSchemaProps or a string array.
 * @type JSONSchemaPropsOrStringArray
 */
export type JSONSchemaPropsOrStringArray = any;
