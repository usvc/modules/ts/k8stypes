import { CustomResourceColumnDefinition } from '../../apiextensions/v1beta1/CustomResourceColumnDefinition';
import { CustomResourceSubresources } from '../../apiextensions/v1beta1/CustomResourceSubresources';
import { CustomResourceValidation } from '../../apiextensions/v1beta1/CustomResourceValidation';

/**
 * CustomResourceDefinitionVersion describes a version for CRD.
 * @interface CustomResourceDefinitionVersion
 * @see io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceDefinitionVersion
 */
export interface CustomResourceDefinitionVersion {
  /**
   * AdditionalPrinterColumns are additional columns shown e.g. in kubectl next to the name. Defaults to a created-at column. Top-level and per-version columns are mutually exclusive. Per-version columns must not all be set to identical values (top-level columns should be used instead) This field is alpha-level and is only honored by servers that enable the CustomResourceWebhookConversion feature. NOTE: CRDs created prior to 1.13 populated the top-level additionalPrinterColumns field by default. To apply an update that changes to per-version additionalPrinterColumns, the top-level additionalPrinterColumns field must be explicitly set to null
   * @type CustomResourceColumnDefinition[]
   */
  additionalPrinterColumns ?: CustomResourceColumnDefinition[]

  /**
   * Name is the version name, e.g. “v1”, “v2beta1”, etc.
   * @type string
   */
  name : string

  /**
   * Schema describes the schema for CustomResource used in validation, pruning, and defaulting. Top-level and per-version schemas are mutually exclusive. Per-version schemas must not all be set to identical values (top-level validation schema should be used instead) This field is alpha-level and is only honored by servers that enable the CustomResourceWebhookConversion feature.
   * @type CustomResourceValidation
   */
  schema ?: CustomResourceValidation

  /**
   * Served is a flag enabling/disabling this version from being served via REST APIs
   * @type boolean
   */
  served : boolean

  /**
   * Storage flags the version as storage version. There must be exactly one flagged as storage version.
   * @type boolean
   */
  storage : boolean

  /**
   * Subresources describes the subresources for CustomResource Top-level and per-version subresources are mutually exclusive. Per-version subresources must not all be set to identical values (top-level subresources should be used instead) This field is alpha-level and is only honored by servers that enable the CustomResourceWebhookConversion feature.
   * @type CustomResourceSubresources
   */
  subresources ?: CustomResourceSubresources

}
