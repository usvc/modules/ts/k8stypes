import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PolicyRule } from '../../rbac/v1/PolicyRule';

/**
 * Role is a namespaced, logical grouping of PolicyRules that can be referenced as a unit by a RoleBinding.
 * @interface Role
 * @see io.k8s.api.rbac.v1.Role
 */
export interface Role {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Rules holds all the PolicyRules for this Role
   * @type PolicyRule[]
   */
  rules ?: PolicyRule[]

}
