/**
 * RoleRef contains information that points to the role being used
 * @interface RoleRef
 * @see io.k8s.api.rbac.v1.RoleRef
 */
export interface RoleRef {
  /**
   * APIGroup is the group for the resource being referenced
   * @type string
   */
  apiGroup : string

  /**
   * Kind is the type of resource being referenced
   * @type string
   */
  kind : string

  /**
   * Name is the name of resource being referenced
   * @type string
   */
  name : string

}
