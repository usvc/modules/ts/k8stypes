import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { RoleRef } from '../../rbac/v1/RoleRef';
import { Subject } from '../../rbac/v1/Subject';

/**
 * ClusterRoleBinding references a ClusterRole, but not contain it.  It can reference a ClusterRole in the global namespace, and adds who information via Subject.
 * @interface ClusterRoleBinding
 * @see io.k8s.api.rbac.v1.ClusterRoleBinding
 */
export interface ClusterRoleBinding {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * RoleRef can only reference a ClusterRole in the global namespace. If the RoleRef cannot be resolved, the Authorizer must return an error.
   * @type RoleRef
   */
  roleRef : RoleRef

  /**
   * Subjects holds references to the objects the role applies to.
   * @type Subject[]
   */
  subjects ?: Subject[]

}
