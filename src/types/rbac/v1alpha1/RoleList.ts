import { ListMeta } from '../../meta/v1/ListMeta';
import { Role } from '../../rbac/v1alpha1/Role';

/**
 * RoleList is a collection of Roles
 * @interface RoleList
 * @see io.k8s.api.rbac.v1alpha1.RoleList
 */
export interface RoleList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1alpha1" | string

  /**
   * Items is a list of Roles
   * @type Role[]
   */
  items : Role[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ListMeta
   */
  metadata ?: ListMeta

}
