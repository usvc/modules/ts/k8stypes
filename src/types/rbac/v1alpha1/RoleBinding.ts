import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { RoleRef } from '../../rbac/v1alpha1/RoleRef';
import { Subject } from '../../rbac/v1alpha1/Subject';

/**
 * RoleBinding references a role, but does not contain it.  It can reference a Role in the same namespace or a ClusterRole in the global namespace. It adds who information via Subjects and namespace information by which namespace it exists in.  RoleBindings in a given namespace only have effect in that namespace.
 * @interface RoleBinding
 * @see io.k8s.api.rbac.v1alpha1.RoleBinding
 */
export interface RoleBinding {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1alpha1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * RoleRef can reference a Role in the current namespace or a ClusterRole in the global namespace. If the RoleRef cannot be resolved, the Authorizer must return an error.
   * @type RoleRef
   */
  roleRef : RoleRef

  /**
   * Subjects holds references to the objects the role applies to.
   * @type Subject[]
   */
  subjects ?: Subject[]

}
