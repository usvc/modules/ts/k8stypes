import { ListMeta } from '../../meta/v1/ListMeta';
import { RoleBinding } from '../../rbac/v1alpha1/RoleBinding';

/**
 * RoleBindingList is a collection of RoleBindings
 * @interface RoleBindingList
 * @see io.k8s.api.rbac.v1alpha1.RoleBindingList
 */
export interface RoleBindingList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1alpha1" | string

  /**
   * Items is a list of RoleBindings
   * @type RoleBinding[]
   */
  items : RoleBinding[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ListMeta
   */
  metadata ?: ListMeta

}
