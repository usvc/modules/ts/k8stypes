import { LabelSelector } from '../../meta/v1/LabelSelector';

/**
 * AggregationRule describes how to locate ClusterRoles to aggregate into the ClusterRole
 * @interface AggregationRule
 * @see io.k8s.api.rbac.v1alpha1.AggregationRule
 */
export interface AggregationRule {
  /**
   * ClusterRoleSelectors holds a list of selectors which will be used to find ClusterRoles and create the rules. If any of the selectors match, then the ClusterRole's permissions will be added
   * @type LabelSelector[]
   */
  clusterRoleSelectors ?: LabelSelector[]

}
