import { AggregationRule } from '../../rbac/v1beta1/AggregationRule';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PolicyRule } from '../../rbac/v1beta1/PolicyRule';

/**
 * ClusterRole is a cluster level, logical grouping of PolicyRules that can be referenced as a unit by a RoleBinding or ClusterRoleBinding.
 * @interface ClusterRole
 * @see io.k8s.api.rbac.v1beta1.ClusterRole
 */
export interface ClusterRole {
  /**
   * AggregationRule is an optional field that describes how to build the Rules for this ClusterRole. If AggregationRule is set, then the Rules are controller managed and direct changes to Rules will be stomped by the controller.
   * @type AggregationRule
   */
  aggregationRule ?: AggregationRule

  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Rules holds all the PolicyRules for this ClusterRole
   * @type PolicyRule[]
   */
  rules ?: PolicyRule[]

}
