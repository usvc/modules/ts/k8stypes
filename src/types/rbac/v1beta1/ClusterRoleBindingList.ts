import { ClusterRoleBinding } from '../../rbac/v1beta1/ClusterRoleBinding';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * ClusterRoleBindingList is a collection of ClusterRoleBindings
 * @interface ClusterRoleBindingList
 * @see io.k8s.api.rbac.v1beta1.ClusterRoleBindingList
 */
export interface ClusterRoleBindingList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "rbac/v1beta1" | string

  /**
   * Items is a list of ClusterRoleBindings
   * @type ClusterRoleBinding[]
   */
  items : ClusterRoleBinding[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata.
   * @type ListMeta
   */
  metadata ?: ListMeta

}
