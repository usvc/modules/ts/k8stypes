import { APIService } from '../../apiregistration/v1/APIService';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * APIServiceList is a list of APIService objects.
 * @interface APIServiceList
 * @see io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIServiceList
 */
export interface APIServiceList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apiregistration/v1" | string

  /**
   * no description provided
   * @type APIService[]
   */
  items : APIService[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ListMeta
   */
  metadata ?: ListMeta

}
