import { Time } from '../../meta/v1/Time';

/**
 * APIServiceCondition describes the state of an APIService at a particular point
 * @interface APIServiceCondition
 * @see io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIServiceCondition
 */
export interface APIServiceCondition {
  /**
   * Last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * Human-readable message indicating details about last transition.
   * @type string
   */
  message ?: string

  /**
   * Unique, one-word, CamelCase reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status is the status of the condition. Can be True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type is the type of the condition.
   * @type string
   */
  type : string

}
