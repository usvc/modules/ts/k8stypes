import { APIServiceCondition } from '../../apiregistration/v1/APIServiceCondition';

/**
 * APIServiceStatus contains derived information about an API server
 * @interface APIServiceStatus
 * @see io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIServiceStatus
 */
export interface APIServiceStatus {
  /**
   * Current service state of apiService.
   * @type APIServiceCondition[]
   */
  conditions ?: APIServiceCondition[]

}
