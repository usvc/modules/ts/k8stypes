/**
 * ServiceReference holds a reference to Service.legacy.k8s.io
 * @interface ServiceReference
 * @see io.k8s.kube-aggregator.pkg.apis.apiregistration.v1beta1.ServiceReference
 */
export interface ServiceReference {
  /**
   * Name is the name of the service
   * @type string
   */
  name ?: string

  /**
   * Namespace is the namespace of the service
   * @type string
   */
  namespace ?: string

  /**
   * If specified, the port on the service that hosting webhook. Default to 443 for backward compatibility. `port` should be a valid port number (1-65535, inclusive).
   * @type number
   */
  port ?: number

}
