import { APIServiceSpec } from '../../apiregistration/v1beta1/APIServiceSpec';
import { APIServiceStatus } from '../../apiregistration/v1beta1/APIServiceStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * APIService represents a server for a particular GroupVersion. Name must be "version.group".
 * @interface APIService
 * @see io.k8s.kube-aggregator.pkg.apis.apiregistration.v1beta1.APIService
 */
export interface APIService {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apiregistration/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec contains information for locating and communicating with a server
   * @type APIServiceSpec
   */
  spec ?: APIServiceSpec

  /**
   * Status contains derived information about an API server
   * @type APIServiceStatus
   */
  status ?: APIServiceStatus

}
