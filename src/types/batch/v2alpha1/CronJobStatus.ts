import { ObjectReference } from '../../core/v1/ObjectReference';
import { Time } from '../../meta/v1/Time';

/**
 * CronJobStatus represents the current state of a cron job.
 * @interface CronJobStatus
 * @see io.k8s.api.batch.v2alpha1.CronJobStatus
 */
export interface CronJobStatus {
  /**
   * A list of pointers to currently running jobs.
   * @type ObjectReference[]
   */
  active ?: ObjectReference[]

  /**
   * Information when was the last time the job was successfully scheduled.
   * @type Time
   */
  lastScheduleTime ?: Time

}
