import { JobTemplateSpec } from '../../batch/v1beta1/JobTemplateSpec';

/**
 * CronJobSpec describes how the job execution will look like and when it will actually run.
 * @interface CronJobSpec
 * @see io.k8s.api.batch.v1beta1.CronJobSpec
 */
export interface CronJobSpec {
  /**
   * Specifies how to treat concurrent executions of a Job. Valid values are: - "Allow" (default): allows CronJobs to run concurrently; - "Forbid": forbids concurrent runs, skipping next run if previous run hasn't finished yet; - "Replace": cancels currently running job and replaces it with a new one
   * @type string
   */
  concurrencyPolicy ?: string

  /**
   * The number of failed finished jobs to retain. This is a pointer to distinguish between explicit zero and not specified. Defaults to 1.
   * @type number
   */
  failedJobsHistoryLimit ?: number

  /**
   * Specifies the job that will be created when executing a CronJob.
   * @type JobTemplateSpec
   */
  jobTemplate : JobTemplateSpec

  /**
   * The schedule in Cron format, see https://en.wikipedia.org/wiki/Cron.
   * @type string
   */
  schedule : string

  /**
   * Optional deadline in seconds for starting the job if it misses scheduled time for any reason.  Missed jobs executions will be counted as failed ones.
   * @type number
   */
  startingDeadlineSeconds ?: number

  /**
   * The number of successful finished jobs to retain. This is a pointer to distinguish between explicit zero and not specified. Defaults to 3.
   * @type number
   */
  successfulJobsHistoryLimit ?: number

  /**
   * This flag tells the controller to suspend subsequent executions, it does not apply to already started executions.  Defaults to false.
   * @type boolean
   */
  suspend ?: boolean

}
