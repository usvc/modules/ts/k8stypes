import { JobSpec } from '../../batch/v1/JobSpec';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * JobTemplateSpec describes the data a Job should have when created from a template
 * @interface JobTemplateSpec
 * @see io.k8s.api.batch.v1beta1.JobTemplateSpec
 */
export interface JobTemplateSpec {
  /**
   * Standard object's metadata of the jobs created from this template. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of the job. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type JobSpec
   */
  spec ?: JobSpec

}
