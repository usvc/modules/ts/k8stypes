import { CronJobSpec } from '../../batch/v1beta1/CronJobSpec';
import { CronJobStatus } from '../../batch/v1beta1/CronJobStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * CronJob represents the configuration of a single cron job.
 * @interface CronJob
 * @see io.k8s.api.batch.v1beta1.CronJob
 */
export interface CronJob {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "batch/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of a cron job, including the schedule. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type CronJobSpec
   */
  spec ?: CronJobSpec

  /**
   * Current status of a cron job. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type CronJobStatus
   */
  status ?: CronJobStatus

}
