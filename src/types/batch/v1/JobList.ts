import { Job } from '../../batch/v1/Job';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * JobList is a collection of jobs.
 * @interface JobList
 * @see io.k8s.api.batch.v1.JobList
 */
export interface JobList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "batch/v1" | string

  /**
   * items is the list of Jobs.
   * @type Job[]
   */
  items : Job[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
