import { JobSpec } from '../../batch/v1/JobSpec';
import { JobStatus } from '../../batch/v1/JobStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * Job represents the configuration of a single job.
 * @interface Job
 * @see io.k8s.api.batch.v1.Job
 */
export interface Job {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "batch/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of a job. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type JobSpec
   */
  spec ?: JobSpec

  /**
   * Current status of a job. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type JobStatus
   */
  status ?: JobStatus

}
