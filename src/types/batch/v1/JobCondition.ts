import { Time } from '../../meta/v1/Time';

/**
 * JobCondition describes current state of a job.
 * @interface JobCondition
 * @see io.k8s.api.batch.v1.JobCondition
 */
export interface JobCondition {
  /**
   * Last time the condition was checked.
   * @type Time
   */
  lastProbeTime ?: Time

  /**
   * Last time the condition transit from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * Human readable message indicating details about last transition.
   * @type string
   */
  message ?: string

  /**
   * (brief) reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status of the condition, one of True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type of job condition, Complete or Failed.
   * @type string
   */
  type : string

}
