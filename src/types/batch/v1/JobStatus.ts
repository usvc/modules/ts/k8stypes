import { JobCondition } from '../../batch/v1/JobCondition';
import { Time } from '../../meta/v1/Time';

/**
 * JobStatus represents the current state of a Job.
 * @interface JobStatus
 * @see io.k8s.api.batch.v1.JobStatus
 */
export interface JobStatus {
  /**
   * The number of actively running pods.
   * @type number
   */
  active ?: number

  /**
   * Represents time when the job was completed. It is not guaranteed to be set in happens-before order across separate operations. It is represented in RFC3339 form and is in UTC.
   * @type Time
   */
  completionTime ?: Time

  /**
   * The latest available observations of an object's current state. More info: https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/
   * @type JobCondition[]
   */
  conditions ?: JobCondition[]

  /**
   * The number of pods which reached phase Failed.
   * @type number
   */
  failed ?: number

  /**
   * Represents time when the job was acknowledged by the job controller. It is not guaranteed to be set in happens-before order across separate operations. It is represented in RFC3339 form and is in UTC.
   * @type Time
   */
  startTime ?: Time

  /**
   * The number of pods which reached phase Succeeded.
   * @type number
   */
  succeeded ?: number

}
