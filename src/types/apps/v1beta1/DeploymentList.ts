import { Deployment } from '../../apps/v1beta1/Deployment';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * DeploymentList is a list of Deployments.
 * @interface DeploymentList
 * @see io.k8s.api.apps.v1beta1.DeploymentList
 */
export interface DeploymentList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1beta1" | string

  /**
   * Items is the list of Deployments.
   * @type Deployment[]
   */
  items : Deployment[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata.
   * @type ListMeta
   */
  metadata ?: ListMeta

}
