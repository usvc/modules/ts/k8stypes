import { ControllerRevision } from '../../apps/v1beta1/ControllerRevision';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * ControllerRevisionList is a resource containing a list of ControllerRevision objects.
 * @interface ControllerRevisionList
 * @see io.k8s.api.apps.v1beta1.ControllerRevisionList
 */
export interface ControllerRevisionList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1beta1" | string

  /**
   * Items is the list of ControllerRevisions
   * @type ControllerRevision[]
   */
  items : ControllerRevision[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
