import { StatefulSetCondition } from '../../apps/v1beta1/StatefulSetCondition';

/**
 * StatefulSetStatus represents the current state of a StatefulSet.
 * @interface StatefulSetStatus
 * @see io.k8s.api.apps.v1beta1.StatefulSetStatus
 */
export interface StatefulSetStatus {
  /**
   * collisionCount is the count of hash collisions for the StatefulSet. The StatefulSet controller uses this field as a collision avoidance mechanism when it needs to create the name for the newest ControllerRevision.
   * @type number
   */
  collisionCount ?: number

  /**
   * Represents the latest available observations of a statefulset's current state.
   * @type StatefulSetCondition[]
   */
  conditions ?: StatefulSetCondition[]

  /**
   * currentReplicas is the number of Pods created by the StatefulSet controller from the StatefulSet version indicated by currentRevision.
   * @type number
   */
  currentReplicas ?: number

  /**
   * currentRevision, if not empty, indicates the version of the StatefulSet used to generate Pods in the sequence [0,currentReplicas).
   * @type string
   */
  currentRevision ?: string

  /**
   * observedGeneration is the most recent generation observed for this StatefulSet. It corresponds to the StatefulSet's generation, which is updated on mutation by the API Server.
   * @type number
   */
  observedGeneration ?: number

  /**
   * readyReplicas is the number of Pods created by the StatefulSet controller that have a Ready Condition.
   * @type number
   */
  readyReplicas ?: number

  /**
   * replicas is the number of Pods created by the StatefulSet controller.
   * @type number
   */
  replicas : number

  /**
   * updateRevision, if not empty, indicates the version of the StatefulSet used to generate Pods in the sequence [replicas-updatedReplicas,replicas)
   * @type string
   */
  updateRevision ?: string

  /**
   * updatedReplicas is the number of Pods created by the StatefulSet controller from the StatefulSet version indicated by updateRevision.
   * @type number
   */
  updatedReplicas ?: number

}
