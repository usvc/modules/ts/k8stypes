import { DeploymentStrategy } from '../../apps/v1beta1/DeploymentStrategy';
import { LabelSelector } from '../../meta/v1/LabelSelector';
import { PodTemplateSpec } from '../../core/v1/PodTemplateSpec';
import { RollbackConfig } from '../../apps/v1beta1/RollbackConfig';

/**
 * DeploymentSpec is the specification of the desired behavior of the Deployment.
 * @interface DeploymentSpec
 * @see io.k8s.api.apps.v1beta1.DeploymentSpec
 */
export interface DeploymentSpec {
  /**
   * Minimum number of seconds for which a newly created pod should be ready without any of its container crashing, for it to be considered available. Defaults to 0 (pod will be considered available as soon as it is ready)
   * @type number
   */
  minReadySeconds ?: number

  /**
   * Indicates that the deployment is paused.
   * @type boolean
   */
  paused ?: boolean

  /**
   * The maximum time in seconds for a deployment to make progress before it is considered to be failed. The deployment controller will continue to process failed deployments and a condition with a ProgressDeadlineExceeded reason will be surfaced in the deployment status. Note that progress will not be estimated during the time a deployment is paused. Defaults to 600s.
   * @type number
   */
  progressDeadlineSeconds ?: number

  /**
   * Number of desired pods. This is a pointer to distinguish between explicit zero and not specified. Defaults to 1.
   * @type number
   */
  replicas ?: number

  /**
   * The number of old ReplicaSets to retain to allow rollback. This is a pointer to distinguish between explicit zero and not specified. Defaults to 2.
   * @type number
   */
  revisionHistoryLimit ?: number

  /**
   * DEPRECATED. The config this deployment is rolling back to. Will be cleared after rollback is done.
   * @type RollbackConfig
   */
  rollbackTo ?: RollbackConfig

  /**
   * Label selector for pods. Existing ReplicaSets whose pods are selected by this will be the ones affected by this deployment.
   * @type LabelSelector
   */
  selector ?: LabelSelector

  /**
   * The deployment strategy to use to replace existing pods with new ones.
   * @type DeploymentStrategy
   */
  strategy ?: DeploymentStrategy

  /**
   * Template describes the pods that will be created.
   * @type PodTemplateSpec
   */
  template : PodTemplateSpec

}
