/**
 * ScaleSpec describes the attributes of a scale subresource
 * @interface ScaleSpec
 * @see io.k8s.api.apps.v1beta1.ScaleSpec
 */
export interface ScaleSpec {
  /**
   * desired number of instances for the scaled object.
   * @type number
   */
  replicas ?: number

}
