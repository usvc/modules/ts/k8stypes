import { DeploymentSpec } from '../../apps/v1beta1/DeploymentSpec';
import { DeploymentStatus } from '../../apps/v1beta1/DeploymentStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * DEPRECATED - This group version of Deployment is deprecated by apps/v1beta2/Deployment. See the release notes for more information. Deployment enables declarative updates for Pods and ReplicaSets.
 * @interface Deployment
 * @see io.k8s.api.apps.v1beta1.Deployment
 */
export interface Deployment {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object metadata.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of the Deployment.
   * @type DeploymentSpec
   */
  spec ?: DeploymentSpec

  /**
   * Most recently observed status of the Deployment.
   * @type DeploymentStatus
   */
  status ?: DeploymentStatus

}
