import { RollingUpdateStatefulSetStrategy } from '../../apps/v1beta1/RollingUpdateStatefulSetStrategy';

/**
 * StatefulSetUpdateStrategy indicates the strategy that the StatefulSet controller will use to perform updates. It includes any additional parameters necessary to perform the update for the indicated strategy.
 * @interface StatefulSetUpdateStrategy
 * @see io.k8s.api.apps.v1beta1.StatefulSetUpdateStrategy
 */
export interface StatefulSetUpdateStrategy {
  /**
   * RollingUpdate is used to communicate parameters when Type is RollingUpdateStatefulSetStrategyType.
   * @type RollingUpdateStatefulSetStrategy
   */
  rollingUpdate ?: RollingUpdateStatefulSetStrategy

  /**
   * Type indicates the type of the StatefulSetUpdateStrategy.
   * @type string
   */
  type ?: string

}
