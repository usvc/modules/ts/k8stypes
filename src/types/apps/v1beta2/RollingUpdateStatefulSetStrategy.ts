/**
 * RollingUpdateStatefulSetStrategy is used to communicate parameter for RollingUpdateStatefulSetStrategyType.
 * @interface RollingUpdateStatefulSetStrategy
 * @see io.k8s.api.apps.v1beta2.RollingUpdateStatefulSetStrategy
 */
export interface RollingUpdateStatefulSetStrategy {
  /**
   * Partition indicates the ordinal at which the StatefulSet should be partitioned. Default value is 0.
   * @type number
   */
  partition ?: number

}
