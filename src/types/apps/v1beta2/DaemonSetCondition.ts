import { Time } from '../../meta/v1/Time';

/**
 * DaemonSetCondition describes the state of a DaemonSet at a certain point.
 * @interface DaemonSetCondition
 * @see io.k8s.api.apps.v1beta2.DaemonSetCondition
 */
export interface DaemonSetCondition {
  /**
   * Last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * A human readable message indicating details about the transition.
   * @type string
   */
  message ?: string

  /**
   * The reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status of the condition, one of True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type of DaemonSet condition.
   * @type string
   */
  type : string

}
