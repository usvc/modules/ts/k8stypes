import { ListMeta } from '../../meta/v1/ListMeta';
import { ReplicaSet } from '../../apps/v1beta2/ReplicaSet';

/**
 * ReplicaSetList is a collection of ReplicaSets.
 * @interface ReplicaSetList
 * @see io.k8s.api.apps.v1beta2.ReplicaSetList
 */
export interface ReplicaSetList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1beta2" | string

  /**
   * List of ReplicaSets. More info: https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller
   * @type ReplicaSet[]
   */
  items : ReplicaSet[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
   * @type ListMeta
   */
  metadata ?: ListMeta

}
