import { DeploymentCondition } from '../../apps/v1beta2/DeploymentCondition';

/**
 * DeploymentStatus is the most recently observed status of the Deployment.
 * @interface DeploymentStatus
 * @see io.k8s.api.apps.v1beta2.DeploymentStatus
 */
export interface DeploymentStatus {
  /**
   * Total number of available pods (ready for at least minReadySeconds) targeted by this deployment.
   * @type number
   */
  availableReplicas ?: number

  /**
   * Count of hash collisions for the Deployment. The Deployment controller uses this field as a collision avoidance mechanism when it needs to create the name for the newest ReplicaSet.
   * @type number
   */
  collisionCount ?: number

  /**
   * Represents the latest available observations of a deployment's current state.
   * @type DeploymentCondition[]
   */
  conditions ?: DeploymentCondition[]

  /**
   * The generation observed by the deployment controller.
   * @type number
   */
  observedGeneration ?: number

  /**
   * Total number of ready pods targeted by this deployment.
   * @type number
   */
  readyReplicas ?: number

  /**
   * Total number of non-terminated pods targeted by this deployment (their labels match the selector).
   * @type number
   */
  replicas ?: number

  /**
   * Total number of unavailable pods targeted by this deployment. This is the total number of pods that are still required for the deployment to have 100% available capacity. They may either be pods that are running but not yet available or pods that still have not been created.
   * @type number
   */
  unavailableReplicas ?: number

  /**
   * Total number of non-terminated pods targeted by this deployment that have the desired template spec.
   * @type number
   */
  updatedReplicas ?: number

}
