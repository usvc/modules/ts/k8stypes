import { RollingUpdateDaemonSet } from '../../apps/v1beta2/RollingUpdateDaemonSet';

/**
 * DaemonSetUpdateStrategy is a struct used to control the update strategy for a DaemonSet.
 * @interface DaemonSetUpdateStrategy
 * @see io.k8s.api.apps.v1beta2.DaemonSetUpdateStrategy
 */
export interface DaemonSetUpdateStrategy {
  /**
   * Rolling update config params. Present only if type = "RollingUpdate".
   * @type RollingUpdateDaemonSet
   */
  rollingUpdate ?: RollingUpdateDaemonSet

  /**
   * Type of daemon set update. Can be "RollingUpdate" or "OnDelete". Default is RollingUpdate.
   * @type string
   */
  type ?: string

}
