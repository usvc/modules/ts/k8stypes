import { DaemonSetCondition } from '../../apps/v1beta2/DaemonSetCondition';

/**
 * DaemonSetStatus represents the current status of a daemon set.
 * @interface DaemonSetStatus
 * @see io.k8s.api.apps.v1beta2.DaemonSetStatus
 */
export interface DaemonSetStatus {
  /**
   * Count of hash collisions for the DaemonSet. The DaemonSet controller uses this field as a collision avoidance mechanism when it needs to create the name for the newest ControllerRevision.
   * @type number
   */
  collisionCount ?: number

  /**
   * Represents the latest available observations of a DaemonSet's current state.
   * @type DaemonSetCondition[]
   */
  conditions ?: DaemonSetCondition[]

  /**
   * The number of nodes that are running at least 1 daemon pod and are supposed to run the daemon pod. More info: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
   * @type number
   */
  currentNumberScheduled : number

  /**
   * The total number of nodes that should be running the daemon pod (including nodes correctly running the daemon pod). More info: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
   * @type number
   */
  desiredNumberScheduled : number

  /**
   * The number of nodes that should be running the daemon pod and have one or more of the daemon pod running and available (ready for at least spec.minReadySeconds)
   * @type number
   */
  numberAvailable ?: number

  /**
   * The number of nodes that are running the daemon pod, but are not supposed to run the daemon pod. More info: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
   * @type number
   */
  numberMisscheduled : number

  /**
   * The number of nodes that should be running the daemon pod and have one or more of the daemon pod running and ready.
   * @type number
   */
  numberReady : number

  /**
   * The number of nodes that should be running the daemon pod and have none of the daemon pod running and available (ready for at least spec.minReadySeconds)
   * @type number
   */
  numberUnavailable ?: number

  /**
   * The most recent generation observed by the daemon set controller.
   * @type number
   */
  observedGeneration ?: number

  /**
   * The total number of nodes that are running updated daemon pod
   * @type number
   */
  updatedNumberScheduled ?: number

}
