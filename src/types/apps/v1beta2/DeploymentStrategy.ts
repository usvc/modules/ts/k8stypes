import { RollingUpdateDeployment } from '../../apps/v1beta2/RollingUpdateDeployment';

/**
 * DeploymentStrategy describes how to replace existing pods with new ones.
 * @interface DeploymentStrategy
 * @see io.k8s.api.apps.v1beta2.DeploymentStrategy
 */
export interface DeploymentStrategy {
  /**
   * Rolling update config params. Present only if DeploymentStrategyType = RollingUpdate.
   * @type RollingUpdateDeployment
   */
  rollingUpdate ?: RollingUpdateDeployment

  /**
   * Type of deployment. Can be "Recreate" or "RollingUpdate". Default is RollingUpdate.
   * @type string
   */
  type ?: string

}
