import { ListMeta } from '../../meta/v1/ListMeta';
import { StatefulSet } from '../../apps/v1beta2/StatefulSet';

/**
 * StatefulSetList is a collection of StatefulSets.
 * @interface StatefulSetList
 * @see io.k8s.api.apps.v1beta2.StatefulSetList
 */
export interface StatefulSetList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1beta2" | string

  /**
   * no description provided
   * @type StatefulSet[]
   */
  items : StatefulSet[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ListMeta
   */
  metadata ?: ListMeta

}
