import { ReplicaSetCondition } from '../../apps/v1/ReplicaSetCondition';

/**
 * ReplicaSetStatus represents the current status of a ReplicaSet.
 * @interface ReplicaSetStatus
 * @see io.k8s.api.apps.v1.ReplicaSetStatus
 */
export interface ReplicaSetStatus {
  /**
   * The number of available replicas (ready for at least minReadySeconds) for this replica set.
   * @type number
   */
  availableReplicas ?: number

  /**
   * Represents the latest available observations of a replica set's current state.
   * @type ReplicaSetCondition[]
   */
  conditions ?: ReplicaSetCondition[]

  /**
   * The number of pods that have labels matching the labels of the pod template of the replicaset.
   * @type number
   */
  fullyLabeledReplicas ?: number

  /**
   * ObservedGeneration reflects the generation of the most recently observed ReplicaSet.
   * @type number
   */
  observedGeneration ?: number

  /**
   * The number of ready replicas for this replica set.
   * @type number
   */
  readyReplicas ?: number

  /**
   * Replicas is the most recently oberved number of replicas. More info: https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/#what-is-a-replicationcontroller
   * @type number
   */
  replicas : number

}
