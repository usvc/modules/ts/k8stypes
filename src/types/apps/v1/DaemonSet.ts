import { DaemonSetSpec } from '../../apps/v1/DaemonSetSpec';
import { DaemonSetStatus } from '../../apps/v1/DaemonSetStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * DaemonSet represents the configuration of a daemon set.
 * @interface DaemonSet
 * @see io.k8s.api.apps.v1.DaemonSet
 */
export interface DaemonSet {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * The desired behavior of this daemon set. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type DaemonSetSpec
   */
  spec ?: DaemonSetSpec

  /**
   * The current status of this daemon set. This data may be out of date by some window of time. Populated by the system. Read-only. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type DaemonSetStatus
   */
  status ?: DaemonSetStatus

}
