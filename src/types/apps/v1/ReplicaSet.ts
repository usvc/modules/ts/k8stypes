import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { ReplicaSetSpec } from '../../apps/v1/ReplicaSetSpec';
import { ReplicaSetStatus } from '../../apps/v1/ReplicaSetStatus';

/**
 * ReplicaSet ensures that a specified number of pod replicas are running at any given time.
 * @interface ReplicaSet
 * @see io.k8s.api.apps.v1.ReplicaSet
 */
export interface ReplicaSet {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * If the Labels of a ReplicaSet are empty, they are defaulted to be the same as the Pod(s) that the ReplicaSet manages. Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the specification of the desired behavior of the ReplicaSet. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type ReplicaSetSpec
   */
  spec ?: ReplicaSetSpec

  /**
   * Status is the most recently observed status of the ReplicaSet. This data may be out of date by some window of time. Populated by the system. Read-only. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type ReplicaSetStatus
   */
  status ?: ReplicaSetStatus

}
