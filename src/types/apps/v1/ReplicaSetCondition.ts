import { Time } from '../../meta/v1/Time';

/**
 * ReplicaSetCondition describes the state of a replica set at a certain point.
 * @interface ReplicaSetCondition
 * @see io.k8s.api.apps.v1.ReplicaSetCondition
 */
export interface ReplicaSetCondition {
  /**
   * The last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * A human readable message indicating details about the transition.
   * @type string
   */
  message ?: string

  /**
   * The reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status of the condition, one of True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type of replica set condition.
   * @type string
   */
  type : string

}
