import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { StatefulSetSpec } from '../../apps/v1/StatefulSetSpec';
import { StatefulSetStatus } from '../../apps/v1/StatefulSetStatus';

/**
 * StatefulSet represents a set of pods with consistent identities. Identities are defined as:  - Network: A single stable DNS and hostname.  - Storage: As many VolumeClaims as requested. The StatefulSet guarantees that a given network identity will always map to the same storage identity.
 * @interface StatefulSet
 * @see io.k8s.api.apps.v1.StatefulSet
 */
export interface StatefulSet {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "apps/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the desired identities of pods in this set.
   * @type StatefulSetSpec
   */
  spec ?: StatefulSetSpec

  /**
   * Status is the current status of Pods in this StatefulSet. This data may be out of date by some window of time.
   * @type StatefulSetStatus
   */
  status ?: StatefulSetStatus

}
