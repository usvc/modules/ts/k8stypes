/**
 * GroupVersion contains the "group/version" and "version" string of a version. It is made a struct to keep extensibility.
 * @interface GroupVersionForDiscovery
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.GroupVersionForDiscovery
 */
export interface GroupVersionForDiscovery {
  /**
   * groupVersion specifies the API group and version in the form "group/version"
   * @type string
   */
  groupVersion : string

  /**
   * version specifies the version in the form of "version". This is to save the clients the trouble of splitting the GroupVersion.
   * @type string
   */
  version : string

}
