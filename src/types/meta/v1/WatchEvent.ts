import { RawExtension } from '../../pkg/runtime/RawExtension';

/**
 * Event represents a single event to a watched resource.
 * @interface WatchEvent
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.WatchEvent
 */
export interface WatchEvent {
  /**
   * Object is:  * If Type is Added or Modified: the new state of the object.  * If Type is Deleted: the state of the object immediately before deletion.  * If Type is Error: *Status is recommended; other types may make sense    depending on context.
   * @type RawExtension
   */
  object : RawExtension

  /**
   * no description provided
   * @type string
   */
  type : string

}
