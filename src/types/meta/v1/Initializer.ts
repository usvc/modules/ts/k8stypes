/**
 * Initializer is information about an initializer that has not yet completed.
 * @interface Initializer
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.Initializer
 */
export interface Initializer {
  /**
   * name of the process that is responsible for initializing this object.
   * @type string
   */
  name : string

}
