/**
 * Preconditions must be fulfilled before an operation (update, delete, etc.) is carried out.
 * @interface Preconditions
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.Preconditions
 */
export interface Preconditions {
  /**
   * Specifies the target ResourceVersion
   * @type string
   */
  resourceVersion ?: string

  /**
   * Specifies the target UID.
   * @type string
   */
  uid ?: string

}
