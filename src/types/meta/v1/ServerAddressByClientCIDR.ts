/**
 * ServerAddressByClientCIDR helps the client to determine the server address that they should use, depending on the clientCIDR that they match.
 * @interface ServerAddressByClientCIDR
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.ServerAddressByClientCIDR
 */
export interface ServerAddressByClientCIDR {
  /**
   * The CIDR with which clients can match their IP to figure out the server address that they should use.
   * @type string
   */
  clientCIDR : string

  /**
   * Address of this server, suitable for a client that matches the above CIDR. This can be a hostname, hostname:port, IP or IP:port.
   * @type string
   */
  serverAddress : string

}
