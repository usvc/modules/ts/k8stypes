/**
 * OwnerReference contains enough information to let you identify an owning object. An owning object must be in the same namespace as the dependent, or be cluster-scoped, so there is no namespace field.
 * @interface OwnerReference
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.OwnerReference
 */
export interface OwnerReference {
  /**
   * API version of the referent.
   * @type string
   */
  apiVersion : "meta/v1" | string

  /**
   * If true, AND if the owner has the "foregroundDeletion" finalizer, then the owner cannot be deleted from the key-value store until this reference is removed. Defaults to false. To set this field, a user needs "delete" permission of the owner, otherwise 422 (Unprocessable Entity) will be returned.
   * @type boolean
   */
  blockOwnerDeletion ?: boolean

  /**
   * If true, this reference points to the managing controller.
   * @type boolean
   */
  controller ?: boolean

  /**
   * Kind of the referent. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind : string

  /**
   * Name of the referent. More info: http://kubernetes.io/docs/user-guide/identifiers#names
   * @type string
   */
  name : string

  /**
   * UID of the referent. More info: http://kubernetes.io/docs/user-guide/identifiers#uids
   * @type string
   */
  uid : string

}
