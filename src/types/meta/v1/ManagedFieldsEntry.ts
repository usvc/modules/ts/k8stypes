import { Fields } from '../../meta/v1/Fields';
import { Time } from '../../meta/v1/Time';

/**
 * ManagedFieldsEntry is a workflow-id, a FieldSet and the group version of the resource that the fieldset applies to.
 * @interface ManagedFieldsEntry
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.ManagedFieldsEntry
 */
export interface ManagedFieldsEntry {
  /**
   * APIVersion defines the version of this resource that this field set applies to. The format is "group/version" just like the top-level APIVersion field. It is necessary to track the version of a field set because it cannot be automatically converted.
   * @type string
   */
  apiVersion : "meta/v1" | string

  /**
   * Fields identifies a set of fields.
   * @type Fields
   */
  fields ?: Fields

  /**
   * Manager is an identifier of the workflow managing these fields.
   * @type string
   */
  manager ?: string

  /**
   * Operation is the type of operation which lead to this ManagedFieldsEntry being created. The only valid values for this field are 'Apply' and 'Update'.
   * @type string
   */
  operation ?: string

  /**
   * Time is timestamp of when these fields were set. It should always be empty if Operation is 'Apply'
   * @type Time
   */
  time ?: Time

}
