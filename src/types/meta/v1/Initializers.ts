import { Initializer } from '../../meta/v1/Initializer';
import { Status } from '../../meta/v1/Status';

/**
 * Initializers tracks the progress of initialization.
 * @interface Initializers
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.Initializers
 */
export interface Initializers {
  /**
   * Pending is a list of initializers that must execute in order before this object is visible. When the last pending initializer is removed, and no failing result is set, the initializers struct will be set to nil and the object is considered as initialized and visible to all clients.
   * @type Initializer[]
   */
  pending : Initializer[]

  /**
   * If result is set with the Failure field, the object will be persisted to storage and then deleted, ensuring that other clients can observe the deletion.
   * @type Status
   */
  result ?: Status

}
