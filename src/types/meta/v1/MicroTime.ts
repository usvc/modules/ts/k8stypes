/**
 * MicroTime is version of Time with microsecond level precision.
 * @type MicroTime
 */
export type MicroTime = string;
