import { LabelSelectorRequirement } from '../../meta/v1/LabelSelectorRequirement';

/**
 * A label selector is a label query over a set of resources. The result of matchLabels and matchExpressions are ANDed. An empty label selector matches all objects. A null label selector matches no objects.
 * @interface LabelSelector
 * @see io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector
 */
export interface LabelSelector {
  /**
   * matchExpressions is a list of label selector requirements. The requirements are ANDed.
   * @type LabelSelectorRequirement[]
   */
  matchExpressions ?: LabelSelectorRequirement[]

  /**
   * matchLabels is a map of {key,value} pairs. A single {key,value} in the matchLabels map is equivalent to an element of matchExpressions, whose key field is "key", the operator is "In", and the values array contains only "value". The requirements are ANDed.
   * @type {[key : string] : string}
   */
  matchLabels ?: {[key : string] : string}

}
