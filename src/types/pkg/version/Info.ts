/**
 * Info contains versioning information. how we'll want to distribute that information.
 * @interface Info
 * @see io.k8s.apimachinery.pkg.version.Info
 */
export interface Info {
  /**
   * no description provided
   * @type string
   */
  buildDate : string

  /**
   * no description provided
   * @type string
   */
  compiler : string

  /**
   * no description provided
   * @type string
   */
  gitCommit : string

  /**
   * no description provided
   * @type string
   */
  gitTreeState : string

  /**
   * no description provided
   * @type string
   */
  gitVersion : string

  /**
   * no description provided
   * @type string
   */
  goVersion : string

  /**
   * no description provided
   * @type string
   */
  major : string

  /**
   * no description provided
   * @type string
   */
  minor : string

  /**
   * no description provided
   * @type string
   */
  platform : string

}
