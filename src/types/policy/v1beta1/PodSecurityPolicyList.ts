import { ListMeta } from '../../meta/v1/ListMeta';
import { PodSecurityPolicy } from '../../policy/v1beta1/PodSecurityPolicy';

/**
 * PodSecurityPolicyList is a list of PodSecurityPolicy objects.
 * @interface PodSecurityPolicyList
 * @see io.k8s.api.policy.v1beta1.PodSecurityPolicyList
 */
export interface PodSecurityPolicyList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "policy/v1beta1" | string

  /**
   * items is a list of schema objects.
   * @type PodSecurityPolicy[]
   */
  items : PodSecurityPolicy[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
