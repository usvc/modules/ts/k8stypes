import { ListMeta } from '../../meta/v1/ListMeta';
import { PodDisruptionBudget } from '../../policy/v1beta1/PodDisruptionBudget';

/**
 * PodDisruptionBudgetList is a collection of PodDisruptionBudgets.
 * @interface PodDisruptionBudgetList
 * @see io.k8s.api.policy.v1beta1.PodDisruptionBudgetList
 */
export interface PodDisruptionBudgetList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "policy/v1beta1" | string

  /**
   * no description provided
   * @type PodDisruptionBudget[]
   */
  items : PodDisruptionBudget[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ListMeta
   */
  metadata ?: ListMeta

}
