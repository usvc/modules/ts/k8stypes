import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PodDisruptionBudgetSpec } from '../../policy/v1beta1/PodDisruptionBudgetSpec';
import { PodDisruptionBudgetStatus } from '../../policy/v1beta1/PodDisruptionBudgetStatus';

/**
 * PodDisruptionBudget is an object to define the max disruption that can be caused to a collection of pods
 * @interface PodDisruptionBudget
 * @see io.k8s.api.policy.v1beta1.PodDisruptionBudget
 */
export interface PodDisruptionBudget {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "policy/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of the PodDisruptionBudget.
   * @type PodDisruptionBudgetSpec
   */
  spec ?: PodDisruptionBudgetSpec

  /**
   * Most recently observed status of the PodDisruptionBudget.
   * @type PodDisruptionBudgetStatus
   */
  status ?: PodDisruptionBudgetStatus

}
