import { DeleteOptions } from '../../meta/v1/DeleteOptions';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * Eviction evicts a pod from its node subject to certain policies and safety constraints. This is a subresource of Pod.  A request to cause such an eviction is created by POSTing to .../pods/<pod name>/evictions.
 * @interface Eviction
 * @see io.k8s.api.policy.v1beta1.Eviction
 */
export interface Eviction {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "policy/v1beta1" | string

  /**
   * DeleteOptions may be provided
   * @type DeleteOptions
   */
  deleteOptions ?: DeleteOptions

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * ObjectMeta describes the pod that is being evicted.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

}
