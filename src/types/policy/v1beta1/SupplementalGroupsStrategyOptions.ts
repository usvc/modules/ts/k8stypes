import { IDRange } from '../../policy/v1beta1/IDRange';

/**
 * SupplementalGroupsStrategyOptions defines the strategy type and options used to create the strategy.
 * @interface SupplementalGroupsStrategyOptions
 * @see io.k8s.api.policy.v1beta1.SupplementalGroupsStrategyOptions
 */
export interface SupplementalGroupsStrategyOptions {
  /**
   * ranges are the allowed ranges of supplemental groups.  If you would like to force a single supplemental group then supply a single range with the same start and end. Required for MustRunAs.
   * @type IDRange[]
   */
  ranges ?: IDRange[]

  /**
   * rule is the strategy that will dictate what supplemental groups is used in the SecurityContext.
   * @type string
   */
  rule ?: string

}
