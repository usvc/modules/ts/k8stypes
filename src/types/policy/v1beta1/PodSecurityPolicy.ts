import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PodSecurityPolicySpec } from '../../policy/v1beta1/PodSecurityPolicySpec';

/**
 * PodSecurityPolicy governs the ability to make requests that affect the Security Context that will be applied to a pod and container.
 * @interface PodSecurityPolicy
 * @see io.k8s.api.policy.v1beta1.PodSecurityPolicy
 */
export interface PodSecurityPolicy {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "policy/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * spec defines the policy enforced.
   * @type PodSecurityPolicySpec
   */
  spec ?: PodSecurityPolicySpec

}
