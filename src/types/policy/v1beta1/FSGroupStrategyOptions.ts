import { IDRange } from '../../policy/v1beta1/IDRange';

/**
 * FSGroupStrategyOptions defines the strategy type and options used to create the strategy.
 * @interface FSGroupStrategyOptions
 * @see io.k8s.api.policy.v1beta1.FSGroupStrategyOptions
 */
export interface FSGroupStrategyOptions {
  /**
   * ranges are the allowed ranges of fs groups.  If you would like to force a single fs group then supply a single range with the same start and end. Required for MustRunAs.
   * @type IDRange[]
   */
  ranges ?: IDRange[]

  /**
   * rule is the strategy that will dictate what FSGroup is used in the SecurityContext.
   * @type string
   */
  rule ?: string

}
