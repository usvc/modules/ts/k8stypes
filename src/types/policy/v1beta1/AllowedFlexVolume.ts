/**
 * AllowedFlexVolume represents a single Flexvolume that is allowed to be used.
 * @interface AllowedFlexVolume
 * @see io.k8s.api.policy.v1beta1.AllowedFlexVolume
 */
export interface AllowedFlexVolume {
  /**
   * driver is the name of the Flexvolume driver.
   * @type string
   */
  driver : string

}
