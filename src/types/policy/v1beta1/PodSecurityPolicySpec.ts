import { AllowedCSIDriver } from '../../policy/v1beta1/AllowedCSIDriver';
import { AllowedFlexVolume } from '../../policy/v1beta1/AllowedFlexVolume';
import { AllowedHostPath } from '../../policy/v1beta1/AllowedHostPath';
import { FSGroupStrategyOptions } from '../../policy/v1beta1/FSGroupStrategyOptions';
import { HostPortRange } from '../../policy/v1beta1/HostPortRange';
import { RunAsGroupStrategyOptions } from '../../policy/v1beta1/RunAsGroupStrategyOptions';
import { RunAsUserStrategyOptions } from '../../policy/v1beta1/RunAsUserStrategyOptions';
import { RuntimeClassStrategyOptions } from '../../policy/v1beta1/RuntimeClassStrategyOptions';
import { SELinuxStrategyOptions } from '../../policy/v1beta1/SELinuxStrategyOptions';
import { SupplementalGroupsStrategyOptions } from '../../policy/v1beta1/SupplementalGroupsStrategyOptions';

/**
 * PodSecurityPolicySpec defines the policy enforced.
 * @interface PodSecurityPolicySpec
 * @see io.k8s.api.policy.v1beta1.PodSecurityPolicySpec
 */
export interface PodSecurityPolicySpec {
  /**
   * allowPrivilegeEscalation determines if a pod can request to allow privilege escalation. If unspecified, defaults to true.
   * @type boolean
   */
  allowPrivilegeEscalation ?: boolean

  /**
   * AllowedCSIDrivers is a whitelist of inline CSI drivers that must be explicitly set to be embedded within a pod spec. An empty value indicates that any CSI driver can be used for inline ephemeral volumes. This is an alpha field, and is only honored if the API server enables the CSIInlineVolume feature gate.
   * @type AllowedCSIDriver[]
   */
  allowedCSIDrivers ?: AllowedCSIDriver[]

  /**
   * allowedCapabilities is a list of capabilities that can be requested to add to the container. Capabilities in this field may be added at the pod author's discretion. You must not list a capability in both allowedCapabilities and requiredDropCapabilities.
   * @type string[]
   */
  allowedCapabilities ?: string[]

  /**
   * allowedFlexVolumes is a whitelist of allowed Flexvolumes.  Empty or nil indicates that all Flexvolumes may be used.  This parameter is effective only when the usage of the Flexvolumes is allowed in the "volumes" field.
   * @type AllowedFlexVolume[]
   */
  allowedFlexVolumes ?: AllowedFlexVolume[]

  /**
   * allowedHostPaths is a white list of allowed host paths. Empty indicates that all host paths may be used.
   * @type AllowedHostPath[]
   */
  allowedHostPaths ?: AllowedHostPath[]

  /**
   * AllowedProcMountTypes is a whitelist of allowed ProcMountTypes. Empty or nil indicates that only the DefaultProcMountType may be used. This requires the ProcMountType feature flag to be enabled.
   * @type string[]
   */
  allowedProcMountTypes ?: string[]

  /**
   * allowedUnsafeSysctls is a list of explicitly allowed unsafe sysctls, defaults to none. Each entry is either a plain sysctl name or ends in "*" in which case it is considered as a prefix of allowed sysctls. Single * means all unsafe sysctls are allowed. Kubelet has to whitelist all allowed unsafe sysctls explicitly to avoid rejection.  Examples: e.g. "foo/*" allows "foo/bar", "foo/baz", etc. e.g. "foo.*" allows "foo.bar", "foo.baz", etc.
   * @type string[]
   */
  allowedUnsafeSysctls ?: string[]

  /**
   * defaultAddCapabilities is the default set of capabilities that will be added to the container unless the pod spec specifically drops the capability.  You may not list a capability in both defaultAddCapabilities and requiredDropCapabilities. Capabilities added here are implicitly allowed, and need not be included in the allowedCapabilities list.
   * @type string[]
   */
  defaultAddCapabilities ?: string[]

  /**
   * defaultAllowPrivilegeEscalation controls the default setting for whether a process can gain more privileges than its parent process.
   * @type boolean
   */
  defaultAllowPrivilegeEscalation ?: boolean

  /**
   * forbiddenSysctls is a list of explicitly forbidden sysctls, defaults to none. Each entry is either a plain sysctl name or ends in "*" in which case it is considered as a prefix of forbidden sysctls. Single * means all sysctls are forbidden.  Examples: e.g. "foo/*" forbids "foo/bar", "foo/baz", etc. e.g. "foo.*" forbids "foo.bar", "foo.baz", etc.
   * @type string[]
   */
  forbiddenSysctls ?: string[]

  /**
   * fsGroup is the strategy that will dictate what fs group is used by the SecurityContext.
   * @type FSGroupStrategyOptions
   */
  fsGroup : FSGroupStrategyOptions

  /**
   * hostIPC determines if the policy allows the use of HostIPC in the pod spec.
   * @type boolean
   */
  hostIPC ?: boolean

  /**
   * hostNetwork determines if the policy allows the use of HostNetwork in the pod spec.
   * @type boolean
   */
  hostNetwork ?: boolean

  /**
   * hostPID determines if the policy allows the use of HostPID in the pod spec.
   * @type boolean
   */
  hostPID ?: boolean

  /**
   * hostPorts determines which host port ranges are allowed to be exposed.
   * @type HostPortRange[]
   */
  hostPorts ?: HostPortRange[]

  /**
   * privileged determines if a pod can request to be run as privileged.
   * @type boolean
   */
  privileged ?: boolean

  /**
   * readOnlyRootFilesystem when set to true will force containers to run with a read only root file system.  If the container specifically requests to run with a non-read only root file system the PSP should deny the pod. If set to false the container may run with a read only root file system if it wishes but it will not be forced to.
   * @type boolean
   */
  readOnlyRootFilesystem ?: boolean

  /**
   * requiredDropCapabilities are the capabilities that will be dropped from the container.  These are required to be dropped and cannot be added.
   * @type string[]
   */
  requiredDropCapabilities ?: string[]

  /**
   * RunAsGroup is the strategy that will dictate the allowable RunAsGroup values that may be set. If this field is omitted, the pod's RunAsGroup can take any value. This field requires the RunAsGroup feature gate to be enabled.
   * @type RunAsGroupStrategyOptions
   */
  runAsGroup ?: RunAsGroupStrategyOptions

  /**
   * runAsUser is the strategy that will dictate the allowable RunAsUser values that may be set.
   * @type RunAsUserStrategyOptions
   */
  runAsUser : RunAsUserStrategyOptions

  /**
   * runtimeClass is the strategy that will dictate the allowable RuntimeClasses for a pod. If this field is omitted, the pod's runtimeClassName field is unrestricted. Enforcement of this field depends on the RuntimeClass feature gate being enabled.
   * @type RuntimeClassStrategyOptions
   */
  runtimeClass ?: RuntimeClassStrategyOptions

  /**
   * seLinux is the strategy that will dictate the allowable labels that may be set.
   * @type SELinuxStrategyOptions
   */
  seLinux : SELinuxStrategyOptions

  /**
   * supplementalGroups is the strategy that will dictate what supplemental groups are used by the SecurityContext.
   * @type SupplementalGroupsStrategyOptions
   */
  supplementalGroups : SupplementalGroupsStrategyOptions

  /**
   * volumes is a white list of allowed volume plugins. Empty indicates that no volumes may be used. To allow all volumes you may use '*'.
   * @type string[]
   */
  volumes ?: string[]

}
