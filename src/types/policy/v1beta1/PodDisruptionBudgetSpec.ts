import { IntOrString } from '../../util/intstr/IntOrString';
import { LabelSelector } from '../../meta/v1/LabelSelector';

/**
 * PodDisruptionBudgetSpec is a description of a PodDisruptionBudget.
 * @interface PodDisruptionBudgetSpec
 * @see io.k8s.api.policy.v1beta1.PodDisruptionBudgetSpec
 */
export interface PodDisruptionBudgetSpec {
  /**
   * An eviction is allowed if at most "maxUnavailable" pods selected by "selector" are unavailable after the eviction, i.e. even in absence of the evicted pod. For example, one can prevent all voluntary evictions by specifying 0. This is a mutually exclusive setting with "minAvailable".
   * @type IntOrString
   */
  maxUnavailable ?: IntOrString

  /**
   * An eviction is allowed if at least "minAvailable" pods selected by "selector" will still be available after the eviction, i.e. even in the absence of the evicted pod.  So for example you can prevent all voluntary evictions by specifying "100%".
   * @type IntOrString
   */
  minAvailable ?: IntOrString

  /**
   * Label query over pods whose evictions are managed by the disruption budget.
   * @type LabelSelector
   */
  selector ?: LabelSelector

}
