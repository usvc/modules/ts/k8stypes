/**
 * IDRange provides a min/max of an allowed range of IDs.
 * @interface IDRange
 * @see io.k8s.api.policy.v1beta1.IDRange
 */
export interface IDRange {
  /**
   * max is the end of the range, inclusive.
   * @type number
   */
  max : number

  /**
   * min is the start of the range, inclusive.
   * @type number
   */
  min : number

}
