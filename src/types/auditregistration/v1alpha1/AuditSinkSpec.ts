import { Policy } from '../../auditregistration/v1alpha1/Policy';
import { Webhook } from '../../auditregistration/v1alpha1/Webhook';

/**
 * AuditSinkSpec holds the spec for the audit sink
 * @interface AuditSinkSpec
 * @see io.k8s.api.auditregistration.v1alpha1.AuditSinkSpec
 */
export interface AuditSinkSpec {
  /**
   * Policy defines the policy for selecting which events should be sent to the webhook required
   * @type Policy
   */
  policy : Policy

  /**
   * Webhook to send events required
   * @type Webhook
   */
  webhook : Webhook

}
