import { AuditSinkSpec } from '../../auditregistration/v1alpha1/AuditSinkSpec';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * AuditSink represents a cluster level audit sink
 * @interface AuditSink
 * @see io.k8s.api.auditregistration.v1alpha1.AuditSink
 */
export interface AuditSink {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "auditregistration/v1alpha1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the audit configuration spec
   * @type AuditSinkSpec
   */
  spec ?: AuditSinkSpec

}
