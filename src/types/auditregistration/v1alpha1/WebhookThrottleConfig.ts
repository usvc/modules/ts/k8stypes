/**
 * WebhookThrottleConfig holds the configuration for throttling events
 * @interface WebhookThrottleConfig
 * @see io.k8s.api.auditregistration.v1alpha1.WebhookThrottleConfig
 */
export interface WebhookThrottleConfig {
  /**
   * ThrottleBurst is the maximum number of events sent at the same moment default 15 QPS
   * @type number
   */
  burst ?: number

  /**
   * ThrottleQPS maximum number of batches per second default 10 QPS
   * @type number
   */
  qps ?: number

}
