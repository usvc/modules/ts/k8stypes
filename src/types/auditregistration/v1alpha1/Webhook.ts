import { WebhookClientConfig } from '../../auditregistration/v1alpha1/WebhookClientConfig';
import { WebhookThrottleConfig } from '../../auditregistration/v1alpha1/WebhookThrottleConfig';

/**
 * Webhook holds the configuration of the webhook
 * @interface Webhook
 * @see io.k8s.api.auditregistration.v1alpha1.Webhook
 */
export interface Webhook {
  /**
   * ClientConfig holds the connection parameters for the webhook required
   * @type WebhookClientConfig
   */
  clientConfig : WebhookClientConfig

  /**
   * Throttle holds the options for throttling the webhook
   * @type WebhookThrottleConfig
   */
  throttle ?: WebhookThrottleConfig

}
