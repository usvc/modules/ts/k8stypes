/**
 * Policy defines the configuration of how audit events are logged
 * @interface Policy
 * @see io.k8s.api.auditregistration.v1alpha1.Policy
 */
export interface Policy {
  /**
   * The Level that all requests are recorded at. available options: None, Metadata, Request, RequestResponse required
   * @type string
   */
  level : string

  /**
   * Stages is a list of stages for which events are created.
   * @type string[]
   */
  stages ?: string[]

}
