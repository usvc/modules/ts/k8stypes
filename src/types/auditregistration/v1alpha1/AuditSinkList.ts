import { AuditSink } from '../../auditregistration/v1alpha1/AuditSink';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * AuditSinkList is a list of AuditSink items.
 * @interface AuditSinkList
 * @see io.k8s.api.auditregistration.v1alpha1.AuditSinkList
 */
export interface AuditSinkList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "auditregistration/v1alpha1" | string

  /**
   * List of audit configurations.
   * @type AuditSink[]
   */
  items : AuditSink[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ListMeta
   */
  metadata ?: ListMeta

}
