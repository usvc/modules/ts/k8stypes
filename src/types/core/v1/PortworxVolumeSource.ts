/**
 * PortworxVolumeSource represents a Portworx volume resource.
 * @interface PortworxVolumeSource
 * @see io.k8s.api.core.v1.PortworxVolumeSource
 */
export interface PortworxVolumeSource {
  /**
   * FSType represents the filesystem type to mount Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs". Implicitly inferred to be "ext4" if unspecified.
   * @type string
   */
  fsType ?: string

  /**
   * Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * VolumeID uniquely identifies a Portworx volume
   * @type string
   */
  volumeID : string

}
