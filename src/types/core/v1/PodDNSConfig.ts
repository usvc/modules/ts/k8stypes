import { PodDNSConfigOption } from '../../core/v1/PodDNSConfigOption';

/**
 * PodDNSConfig defines the DNS parameters of a pod in addition to those generated from DNSPolicy.
 * @interface PodDNSConfig
 * @see io.k8s.api.core.v1.PodDNSConfig
 */
export interface PodDNSConfig {
  /**
   * A list of DNS name server IP addresses. This will be appended to the base nameservers generated from DNSPolicy. Duplicated nameservers will be removed.
   * @type string[]
   */
  nameservers ?: string[]

  /**
   * A list of DNS resolver options. This will be merged with the base options generated from DNSPolicy. Duplicated entries will be removed. Resolution options given in Options will override those that appear in the base DNSPolicy.
   * @type PodDNSConfigOption[]
   */
  options ?: PodDNSConfigOption[]

  /**
   * A list of DNS search domains for host-name lookup. This will be appended to the base search paths generated from DNSPolicy. Duplicated search paths will be removed.
   * @type string[]
   */
  searches ?: string[]

}
