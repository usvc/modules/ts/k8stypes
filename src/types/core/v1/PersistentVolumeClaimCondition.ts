import { Time } from '../../meta/v1/Time';

/**
 * PersistentVolumeClaimCondition contails details about state of pvc
 * @interface PersistentVolumeClaimCondition
 * @see io.k8s.api.core.v1.PersistentVolumeClaimCondition
 */
export interface PersistentVolumeClaimCondition {
  /**
   * Last time we probed the condition.
   * @type Time
   */
  lastProbeTime ?: Time

  /**
   * Last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * Human-readable message indicating details about last transition.
   * @type string
   */
  message ?: string

  /**
   * Unique, this should be a short, machine understandable string that gives the reason for condition's last transition. If it reports "ResizeStarted" that means the underlying persistent volume is being resized.
   * @type string
   */
  reason ?: string

  /**
   * no description provided
   * @type string
   */
  status : string

  /**
   * no description provided
   * @type string
   */
  type : string

}
