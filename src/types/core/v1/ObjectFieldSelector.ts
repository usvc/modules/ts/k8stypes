/**
 * ObjectFieldSelector selects an APIVersioned field of an object.
 * @interface ObjectFieldSelector
 * @see io.k8s.api.core.v1.ObjectFieldSelector
 */
export interface ObjectFieldSelector {
  /**
   * Version of the schema the FieldPath is written in terms of, defaults to "v1".
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Path of the field to select in the specified API version.
   * @type string
   */
  fieldPath : string

}
