import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PodSpec } from '../../core/v1/PodSpec';

/**
 * PodTemplateSpec describes the data a pod should have when created from a template
 * @interface PodTemplateSpec
 * @see io.k8s.api.core.v1.PodTemplateSpec
 */
export interface PodTemplateSpec {
  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of the pod. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type PodSpec
   */
  spec ?: PodSpec

}
