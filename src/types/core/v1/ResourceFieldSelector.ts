import { Quantity } from '../../api/resource/Quantity';

/**
 * ResourceFieldSelector represents container resources (cpu, memory) and their output format
 * @interface ResourceFieldSelector
 * @see io.k8s.api.core.v1.ResourceFieldSelector
 */
export interface ResourceFieldSelector {
  /**
   * Container name: required for volumes, optional for env vars
   * @type string
   */
  containerName ?: string

  /**
   * Specifies the output format of the exposed resources, defaults to "1"
   * @type Quantity
   */
  divisor ?: Quantity

  /**
   * Required: resource to select
   * @type string
   */
  resource : string

}
