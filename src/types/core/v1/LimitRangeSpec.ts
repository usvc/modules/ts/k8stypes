import { LimitRangeItem } from '../../core/v1/LimitRangeItem';

/**
 * LimitRangeSpec defines a min/max usage limit for resources that match on kind.
 * @interface LimitRangeSpec
 * @see io.k8s.api.core.v1.LimitRangeSpec
 */
export interface LimitRangeSpec {
  /**
   * Limits is the list of LimitRangeItem objects that are enforced.
   * @type LimitRangeItem[]
   */
  limits : LimitRangeItem[]

}
