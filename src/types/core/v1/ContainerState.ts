import { ContainerStateRunning } from '../../core/v1/ContainerStateRunning';
import { ContainerStateTerminated } from '../../core/v1/ContainerStateTerminated';
import { ContainerStateWaiting } from '../../core/v1/ContainerStateWaiting';

/**
 * ContainerState holds a possible state of container. Only one of its members may be specified. If none of them is specified, the default one is ContainerStateWaiting.
 * @interface ContainerState
 * @see io.k8s.api.core.v1.ContainerState
 */
export interface ContainerState {
  /**
   * Details about a running container
   * @type ContainerStateRunning
   */
  running ?: ContainerStateRunning

  /**
   * Details about a terminated container
   * @type ContainerStateTerminated
   */
  terminated ?: ContainerStateTerminated

  /**
   * Details about a waiting container
   * @type ContainerStateWaiting
   */
  waiting ?: ContainerStateWaiting

}
