/**
 * NamespaceStatus is information about the current status of a Namespace.
 * @interface NamespaceStatus
 * @see io.k8s.api.core.v1.NamespaceStatus
 */
export interface NamespaceStatus {
  /**
   * Phase is the current lifecycle phase of the namespace. More info: https://kubernetes.io/docs/tasks/administer-cluster/namespaces/
   * @type string
   */
  phase ?: string

}
