import { LoadBalancerIngress } from '../../core/v1/LoadBalancerIngress';

/**
 * LoadBalancerStatus represents the status of a load-balancer.
 * @interface LoadBalancerStatus
 * @see io.k8s.api.core.v1.LoadBalancerStatus
 */
export interface LoadBalancerStatus {
  /**
   * Ingress is a list containing ingress points for the load-balancer. Traffic intended for the service should be sent to these ingress points.
   * @type LoadBalancerIngress[]
   */
  ingress ?: LoadBalancerIngress[]

}
