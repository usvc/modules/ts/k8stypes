import { LoadBalancerStatus } from '../../core/v1/LoadBalancerStatus';

/**
 * ServiceStatus represents the current status of a service.
 * @interface ServiceStatus
 * @see io.k8s.api.core.v1.ServiceStatus
 */
export interface ServiceStatus {
  /**
   * LoadBalancer contains the current status of the load-balancer, if one is present.
   * @type LoadBalancerStatus
   */
  loadBalancer ?: LoadBalancerStatus

}
