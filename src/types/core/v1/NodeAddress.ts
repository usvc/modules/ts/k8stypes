/**
 * NodeAddress contains information for the node's address.
 * @interface NodeAddress
 * @see io.k8s.api.core.v1.NodeAddress
 */
export interface NodeAddress {
  /**
   * The node address.
   * @type string
   */
  address : string

  /**
   * Node address type, one of Hostname, ExternalIP or InternalIP.
   * @type string
   */
  type : string

}
