/**
 * PodDNSConfigOption defines DNS resolver options of a pod.
 * @interface PodDNSConfigOption
 * @see io.k8s.api.core.v1.PodDNSConfigOption
 */
export interface PodDNSConfigOption {
  /**
   * Required.
   * @type string
   */
  name ?: string

  /**
   * no description provided
   * @type string
   */
  value ?: string

}
