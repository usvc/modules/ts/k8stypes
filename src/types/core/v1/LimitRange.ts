import { LimitRangeSpec } from '../../core/v1/LimitRangeSpec';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * LimitRange sets resource usage limits for each kind of resource in a Namespace.
 * @interface LimitRange
 * @see io.k8s.api.core.v1.LimitRange
 */
export interface LimitRange {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the limits enforced. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type LimitRangeSpec
   */
  spec ?: LimitRangeSpec

}
