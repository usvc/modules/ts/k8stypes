import { Time } from '../../meta/v1/Time';

/**
 * ReplicationControllerCondition describes the state of a replication controller at a certain point.
 * @interface ReplicationControllerCondition
 * @see io.k8s.api.core.v1.ReplicationControllerCondition
 */
export interface ReplicationControllerCondition {
  /**
   * The last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * A human readable message indicating details about the transition.
   * @type string
   */
  message ?: string

  /**
   * The reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status of the condition, one of True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type of replication controller condition.
   * @type string
   */
  type : string

}
