import { ContainerState } from '../../core/v1/ContainerState';

/**
 * ContainerStatus contains details for the current status of this container.
 * @interface ContainerStatus
 * @see io.k8s.api.core.v1.ContainerStatus
 */
export interface ContainerStatus {
  /**
   * Container's ID in the format 'docker://<container_id>'.
   * @type string
   */
  containerID ?: string

  /**
   * The image the container is running. More info: https://kubernetes.io/docs/concepts/containers/images
   * @type string
   */
  image : string

  /**
   * ImageID of the container's image.
   * @type string
   */
  imageID : string

  /**
   * Details about the container's last termination condition.
   * @type ContainerState
   */
  lastState ?: ContainerState

  /**
   * This must be a DNS_LABEL. Each container in a pod must have a unique name. Cannot be updated.
   * @type string
   */
  name : string

  /**
   * Specifies whether the container has passed its readiness probe.
   * @type boolean
   */
  ready : boolean

  /**
   * The number of times the container has been restarted, currently based on the number of dead containers that have not yet been removed. Note that this is calculated from dead containers. But those containers are subject to garbage collection. This value will get capped at 5 by GC.
   * @type number
   */
  restartCount : number

  /**
   * Details about the container's current condition.
   * @type ContainerState
   */
  state ?: ContainerState

}
