import { DownwardAPIVolumeFile } from '../../core/v1/DownwardAPIVolumeFile';

/**
 * DownwardAPIVolumeSource represents a volume containing downward API info. Downward API volumes support ownership management and SELinux relabeling.
 * @interface DownwardAPIVolumeSource
 * @see io.k8s.api.core.v1.DownwardAPIVolumeSource
 */
export interface DownwardAPIVolumeSource {
  /**
   * Optional: mode bits to use on created files by default. Must be a value between 0 and 0777. Defaults to 0644. Directories within the path are not affected by this setting. This might be in conflict with other options that affect the file mode, like fsGroup, and the result can be other mode bits set.
   * @type number
   */
  defaultMode ?: number

  /**
   * Items is a list of downward API volume file
   * @type DownwardAPIVolumeFile[]
   */
  items ?: DownwardAPIVolumeFile[]

}
