import { ConfigMapProjection } from '../../core/v1/ConfigMapProjection';
import { DownwardAPIProjection } from '../../core/v1/DownwardAPIProjection';
import { SecretProjection } from '../../core/v1/SecretProjection';
import { ServiceAccountTokenProjection } from '../../core/v1/ServiceAccountTokenProjection';

/**
 * Projection that may be projected along with other supported volume types
 * @interface VolumeProjection
 * @see io.k8s.api.core.v1.VolumeProjection
 */
export interface VolumeProjection {
  /**
   * information about the configMap data to project
   * @type ConfigMapProjection
   */
  configMap ?: ConfigMapProjection

  /**
   * information about the downwardAPI data to project
   * @type DownwardAPIProjection
   */
  downwardAPI ?: DownwardAPIProjection

  /**
   * information about the secret data to project
   * @type SecretProjection
   */
  secret ?: SecretProjection

  /**
   * information about the serviceAccountToken data to project
   * @type ServiceAccountTokenProjection
   */
  serviceAccountToken ?: ServiceAccountTokenProjection

}
