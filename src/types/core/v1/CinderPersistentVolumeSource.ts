import { SecretReference } from '../../core/v1/SecretReference';

/**
 * Represents a cinder volume resource in Openstack. A Cinder volume must exist before mounting to a container. The volume must also be in the same region as the kubelet. Cinder volumes support ownership management and SELinux relabeling.
 * @interface CinderPersistentVolumeSource
 * @see io.k8s.api.core.v1.CinderPersistentVolumeSource
 */
export interface CinderPersistentVolumeSource {
  /**
   * Filesystem type to mount. Must be a filesystem type supported by the host operating system. Examples: "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified. More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
   * @type string
   */
  fsType ?: string

  /**
   * Optional: Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts. More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Optional: points to a secret object containing parameters used to connect to OpenStack.
   * @type SecretReference
   */
  secretRef ?: SecretReference

  /**
   * volume id used to identify the volume in cinder More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
   * @type string
   */
  volumeID : string

}
