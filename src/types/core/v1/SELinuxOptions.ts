/**
 * SELinuxOptions are the labels to be applied to the container
 * @interface SELinuxOptions
 * @see io.k8s.api.core.v1.SELinuxOptions
 */
export interface SELinuxOptions {
  /**
   * Level is SELinux level label that applies to the container.
   * @type string
   */
  level ?: string

  /**
   * Role is a SELinux role label that applies to the container.
   * @type string
   */
  role ?: string

  /**
   * Type is a SELinux type label that applies to the container.
   * @type string
   */
  type ?: string

  /**
   * User is a SELinux user label that applies to the container.
   * @type string
   */
  user ?: string

}
