/**
 * SecretEnvSource selects a Secret to populate the environment variables with.  The contents of the target Secret's Data field will represent the key-value pairs as environment variables.
 * @interface SecretEnvSource
 * @see io.k8s.api.core.v1.SecretEnvSource
 */
export interface SecretEnvSource {
  /**
   * Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
   * @type string
   */
  name ?: string

  /**
   * Specify whether the Secret must be defined
   * @type boolean
   */
  optional ?: boolean

}
