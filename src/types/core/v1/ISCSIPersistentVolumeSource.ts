import { SecretReference } from '../../core/v1/SecretReference';

/**
 * ISCSIPersistentVolumeSource represents an ISCSI disk. ISCSI volumes can only be mounted as read/write once. ISCSI volumes support ownership management and SELinux relabeling.
 * @interface ISCSIPersistentVolumeSource
 * @see io.k8s.api.core.v1.ISCSIPersistentVolumeSource
 */
export interface ISCSIPersistentVolumeSource {
  /**
   * whether support iSCSI Discovery CHAP authentication
   * @type boolean
   */
  chapAuthDiscovery ?: boolean

  /**
   * whether support iSCSI Session CHAP authentication
   * @type boolean
   */
  chapAuthSession ?: boolean

  /**
   * Filesystem type of the volume that you want to mount. Tip: Ensure that the filesystem type is supported by the host operating system. Examples: "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified. More info: https://kubernetes.io/docs/concepts/storage/volumes#iscsi
   * @type string
   */
  fsType ?: string

  /**
   * Custom iSCSI Initiator Name. If initiatorName is specified with iscsiInterface simultaneously, new iSCSI interface <target portal>:<volume name> will be created for the connection.
   * @type string
   */
  initiatorName ?: string

  /**
   * Target iSCSI Qualified Name.
   * @type string
   */
  iqn : string

  /**
   * iSCSI Interface Name that uses an iSCSI transport. Defaults to 'default' (tcp).
   * @type string
   */
  iscsiInterface ?: string

  /**
   * iSCSI Target Lun number.
   * @type number
   */
  lun : number

  /**
   * iSCSI Target Portal List. The Portal is either an IP or ip_addr:port if the port is other than default (typically TCP ports 860 and 3260).
   * @type string[]
   */
  portals ?: string[]

  /**
   * ReadOnly here will force the ReadOnly setting in VolumeMounts. Defaults to false.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * CHAP Secret for iSCSI target and initiator authentication
   * @type SecretReference
   */
  secretRef ?: SecretReference

  /**
   * iSCSI Target Portal. The Portal is either an IP or ip_addr:port if the port is other than default (typically TCP ports 860 and 3260).
   * @type string
   */
  targetPortal : string

}
