import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { ResourceQuotaSpec } from '../../core/v1/ResourceQuotaSpec';
import { ResourceQuotaStatus } from '../../core/v1/ResourceQuotaStatus';

/**
 * ResourceQuota sets aggregate quota restrictions enforced per namespace
 * @interface ResourceQuota
 * @see io.k8s.api.core.v1.ResourceQuota
 */
export interface ResourceQuota {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the desired quota. https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type ResourceQuotaSpec
   */
  spec ?: ResourceQuotaSpec

  /**
   * Status defines the actual enforced quota and its current usage. https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type ResourceQuotaStatus
   */
  status ?: ResourceQuotaStatus

}
