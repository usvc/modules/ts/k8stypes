import { ExecAction } from '../../core/v1/ExecAction';
import { HTTPGetAction } from '../../core/v1/HTTPGetAction';
import { TCPSocketAction } from '../../core/v1/TCPSocketAction';

/**
 * Probe describes a health check to be performed against a container to determine whether it is alive or ready to receive traffic.
 * @interface Probe
 * @see io.k8s.api.core.v1.Probe
 */
export interface Probe {
  /**
   * One and only one of the following should be specified. Exec specifies the action to take.
   * @type ExecAction
   */
  exec ?: ExecAction

  /**
   * Minimum consecutive failures for the probe to be considered failed after having succeeded. Defaults to 3. Minimum value is 1.
   * @type number
   */
  failureThreshold ?: number

  /**
   * HTTPGet specifies the http request to perform.
   * @type HTTPGetAction
   */
  httpGet ?: HTTPGetAction

  /**
   * Number of seconds after the container has started before liveness probes are initiated. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#container-probes
   * @type number
   */
  initialDelaySeconds ?: number

  /**
   * How often (in seconds) to perform the probe. Default to 10 seconds. Minimum value is 1.
   * @type number
   */
  periodSeconds ?: number

  /**
   * Minimum consecutive successes for the probe to be considered successful after having failed. Defaults to 1. Must be 1 for liveness. Minimum value is 1.
   * @type number
   */
  successThreshold ?: number

  /**
   * TCPSocket specifies an action involving a TCP port. TCP hooks not yet supported
   * @type TCPSocketAction
   */
  tcpSocket ?: TCPSocketAction

  /**
   * Number of seconds after which the probe times out. Defaults to 1 second. Minimum value is 1. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#container-probes
   * @type number
   */
  timeoutSeconds ?: number

}
