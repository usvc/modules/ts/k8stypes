import { NodeSelectorRequirement } from '../../core/v1/NodeSelectorRequirement';

/**
 * A null or empty node selector term matches no objects. The requirements of them are ANDed. The TopologySelectorTerm type implements a subset of the NodeSelectorTerm.
 * @interface NodeSelectorTerm
 * @see io.k8s.api.core.v1.NodeSelectorTerm
 */
export interface NodeSelectorTerm {
  /**
   * A list of node selector requirements by node's labels.
   * @type NodeSelectorRequirement[]
   */
  matchExpressions ?: NodeSelectorRequirement[]

  /**
   * A list of node selector requirements by node's fields.
   * @type NodeSelectorRequirement[]
   */
  matchFields ?: NodeSelectorRequirement[]

}
