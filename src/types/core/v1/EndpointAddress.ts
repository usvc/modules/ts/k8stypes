import { ObjectReference } from '../../core/v1/ObjectReference';

/**
 * EndpointAddress is a tuple that describes single IP address.
 * @interface EndpointAddress
 * @see io.k8s.api.core.v1.EndpointAddress
 */
export interface EndpointAddress {
  /**
   * The Hostname of this endpoint
   * @type string
   */
  hostname ?: string

  /**
   * The IP of this endpoint. May not be loopback (127.0.0.0/8), link-local (169.254.0.0/16), or link-local multicast ((224.0.0.0/24). IPv6 is also accepted but not fully supported on all platforms. Also, certain kubernetes components, like kube-proxy, are not IPv6 ready.
   * @type string
   */
  ip : string

  /**
   * Optional: Node hosting this endpoint. This can be used to determine endpoints local to a node.
   * @type string
   */
  nodeName ?: string

  /**
   * Reference to object providing the endpoint.
   * @type ObjectReference
   */
  targetRef ?: ObjectReference

}
