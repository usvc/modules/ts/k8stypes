/**
 * SecretReference represents a Secret Reference. It has enough information to retrieve secret in any namespace
 * @interface SecretReference
 * @see io.k8s.api.core.v1.SecretReference
 */
export interface SecretReference {
  /**
   * Name is unique within a namespace to reference a secret resource.
   * @type string
   */
  name ?: string

  /**
   * Namespace defines the space within which the secret name must be unique.
   * @type string
   */
  namespace ?: string

}
