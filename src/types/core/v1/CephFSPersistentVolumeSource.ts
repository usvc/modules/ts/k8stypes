import { SecretReference } from '../../core/v1/SecretReference';

/**
 * Represents a Ceph Filesystem mount that lasts the lifetime of a pod Cephfs volumes do not support ownership management or SELinux relabeling.
 * @interface CephFSPersistentVolumeSource
 * @see io.k8s.api.core.v1.CephFSPersistentVolumeSource
 */
export interface CephFSPersistentVolumeSource {
  /**
   * Required: Monitors is a collection of Ceph monitors More info: https://releases.k8s.io/HEAD/examples/volumes/cephfs/README.md#how-to-use-it
   * @type string[]
   */
  monitors : string[]

  /**
   * Optional: Used as the mounted root, rather than the full Ceph tree, default is /
   * @type string
   */
  path ?: string

  /**
   * Optional: Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts. More info: https://releases.k8s.io/HEAD/examples/volumes/cephfs/README.md#how-to-use-it
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Optional: SecretFile is the path to key ring for User, default is /etc/ceph/user.secret More info: https://releases.k8s.io/HEAD/examples/volumes/cephfs/README.md#how-to-use-it
   * @type string
   */
  secretFile ?: string

  /**
   * Optional: SecretRef is reference to the authentication secret for User, default is empty. More info: https://releases.k8s.io/HEAD/examples/volumes/cephfs/README.md#how-to-use-it
   * @type SecretReference
   */
  secretRef ?: SecretReference

  /**
   * Optional: User is the rados user name, default is admin More info: https://releases.k8s.io/HEAD/examples/volumes/cephfs/README.md#how-to-use-it
   * @type string
   */
  user ?: string

}
