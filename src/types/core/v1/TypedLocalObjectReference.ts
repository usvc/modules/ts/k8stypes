/**
 * TypedLocalObjectReference contains enough information to let you locate the typed referenced object inside the same namespace.
 * @interface TypedLocalObjectReference
 * @see io.k8s.api.core.v1.TypedLocalObjectReference
 */
export interface TypedLocalObjectReference {
  /**
   * APIGroup is the group for the resource being referenced. If APIGroup is not specified, the specified Kind must be in the core API group. For any other third-party types, APIGroup is required.
   * @type string
   */
  apiGroup ?: string

  /**
   * Kind is the type of resource being referenced
   * @type string
   */
  kind : string

  /**
   * Name is the name of resource being referenced
   * @type string
   */
  name : string

}
