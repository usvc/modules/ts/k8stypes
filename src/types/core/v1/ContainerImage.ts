/**
 * Describe a container image
 * @interface ContainerImage
 * @see io.k8s.api.core.v1.ContainerImage
 */
export interface ContainerImage {
  /**
   * Names by which this image is known. e.g. ["k8s.gcr.io/hyperkube:v1.0.7", "dockerhub.io/google_containers/hyperkube:v1.0.7"]
   * @type string[]
   */
  names : string[]

  /**
   * The size of the image in bytes.
   * @type number
   */
  sizeBytes ?: number

}
