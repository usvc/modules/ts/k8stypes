/**
 * EndpointPort is a tuple that describes a single port.
 * @interface EndpointPort
 * @see io.k8s.api.core.v1.EndpointPort
 */
export interface EndpointPort {
  /**
   * The name of this port (corresponds to ServicePort.Name). Must be a DNS_LABEL. Optional only if one port is defined.
   * @type string
   */
  name ?: string

  /**
   * The port number of the endpoint.
   * @type number
   */
  port : number

  /**
   * The IP protocol for this port. Must be UDP, TCP, or SCTP. Default is TCP.
   * @type string
   */
  protocol ?: string

}
