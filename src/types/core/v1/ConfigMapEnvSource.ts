/**
 * ConfigMapEnvSource selects a ConfigMap to populate the environment variables with.  The contents of the target ConfigMap's Data field will represent the key-value pairs as environment variables.
 * @interface ConfigMapEnvSource
 * @see io.k8s.api.core.v1.ConfigMapEnvSource
 */
export interface ConfigMapEnvSource {
  /**
   * Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
   * @type string
   */
  name ?: string

  /**
   * Specify whether the ConfigMap must be defined
   * @type boolean
   */
  optional ?: boolean

}
