import { ReplicationControllerCondition } from '../../core/v1/ReplicationControllerCondition';

/**
 * ReplicationControllerStatus represents the current status of a replication controller.
 * @interface ReplicationControllerStatus
 * @see io.k8s.api.core.v1.ReplicationControllerStatus
 */
export interface ReplicationControllerStatus {
  /**
   * The number of available replicas (ready for at least minReadySeconds) for this replication controller.
   * @type number
   */
  availableReplicas ?: number

  /**
   * Represents the latest available observations of a replication controller's current state.
   * @type ReplicationControllerCondition[]
   */
  conditions ?: ReplicationControllerCondition[]

  /**
   * The number of pods that have labels matching the labels of the pod template of the replication controller.
   * @type number
   */
  fullyLabeledReplicas ?: number

  /**
   * ObservedGeneration reflects the generation of the most recently observed replication controller.
   * @type number
   */
  observedGeneration ?: number

  /**
   * The number of ready replicas for this replication controller.
   * @type number
   */
  readyReplicas ?: number

  /**
   * Replicas is the most recently oberved number of replicas. More info: https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller#what-is-a-replicationcontroller
   * @type number
   */
  replicas : number

}
