import { IntOrString } from '../../util/intstr/IntOrString';

/**
 * TCPSocketAction describes an action based on opening a socket
 * @interface TCPSocketAction
 * @see io.k8s.api.core.v1.TCPSocketAction
 */
export interface TCPSocketAction {
  /**
   * Optional: Host name to connect to, defaults to the pod IP.
   * @type string
   */
  host ?: string

  /**
   * Number or name of the port to access on the container. Number must be in the range 1 to 65535. Name must be an IANA_SVC_NAME.
   * @type IntOrString
   */
  port : IntOrString

}
