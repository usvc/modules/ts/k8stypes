import { AttachedVolume } from '../../core/v1/AttachedVolume';
import { ContainerImage } from '../../core/v1/ContainerImage';
import { NodeAddress } from '../../core/v1/NodeAddress';
import { NodeCondition } from '../../core/v1/NodeCondition';
import { NodeConfigStatus } from '../../core/v1/NodeConfigStatus';
import { NodeDaemonEndpoints } from '../../core/v1/NodeDaemonEndpoints';
import { NodeSystemInfo } from '../../core/v1/NodeSystemInfo';
import { Quantity } from '../../api/resource/Quantity';

/**
 * NodeStatus is information about the current status of a node.
 * @interface NodeStatus
 * @see io.k8s.api.core.v1.NodeStatus
 */
export interface NodeStatus {
  /**
   * List of addresses reachable to the node. Queried from cloud provider, if available. More info: https://kubernetes.io/docs/concepts/nodes/node/#addresses
   * @type NodeAddress[]
   */
  addresses ?: NodeAddress[]

  /**
   * Allocatable represents the resources of a node that are available for scheduling. Defaults to Capacity.
   * @type {[key : string] : Quantity}
   */
  allocatable ?: {[key : string] : Quantity}

  /**
   * Capacity represents the total resources of a node. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#capacity
   * @type {[key : string] : Quantity}
   */
  capacity ?: {[key : string] : Quantity}

  /**
   * Conditions is an array of current observed node conditions. More info: https://kubernetes.io/docs/concepts/nodes/node/#condition
   * @type NodeCondition[]
   */
  conditions ?: NodeCondition[]

  /**
   * Status of the config assigned to the node via the dynamic Kubelet config feature.
   * @type NodeConfigStatus
   */
  config ?: NodeConfigStatus

  /**
   * Endpoints of daemons running on the Node.
   * @type NodeDaemonEndpoints
   */
  daemonEndpoints ?: NodeDaemonEndpoints

  /**
   * List of container images on this node
   * @type ContainerImage[]
   */
  images ?: ContainerImage[]

  /**
   * Set of ids/uuids to uniquely identify the node. More info: https://kubernetes.io/docs/concepts/nodes/node/#info
   * @type NodeSystemInfo
   */
  nodeInfo ?: NodeSystemInfo

  /**
   * NodePhase is the recently observed lifecycle phase of the node. More info: https://kubernetes.io/docs/concepts/nodes/node/#phase The field is never populated, and now is deprecated.
   * @type string
   */
  phase ?: string

  /**
   * List of volumes that are attached to the node.
   * @type AttachedVolume[]
   */
  volumesAttached ?: AttachedVolume[]

  /**
   * List of attachable volumes in use (mounted) by the node.
   * @type string[]
   */
  volumesInUse ?: string[]

}
