import { AWSElasticBlockStoreVolumeSource } from '../../core/v1/AWSElasticBlockStoreVolumeSource';
import { AzureDiskVolumeSource } from '../../core/v1/AzureDiskVolumeSource';
import { AzureFileVolumeSource } from '../../core/v1/AzureFileVolumeSource';
import { CephFSVolumeSource } from '../../core/v1/CephFSVolumeSource';
import { CinderVolumeSource } from '../../core/v1/CinderVolumeSource';
import { ConfigMapVolumeSource } from '../../core/v1/ConfigMapVolumeSource';
import { CSIVolumeSource } from '../../core/v1/CSIVolumeSource';
import { DownwardAPIVolumeSource } from '../../core/v1/DownwardAPIVolumeSource';
import { EmptyDirVolumeSource } from '../../core/v1/EmptyDirVolumeSource';
import { FCVolumeSource } from '../../core/v1/FCVolumeSource';
import { FlexVolumeSource } from '../../core/v1/FlexVolumeSource';
import { FlockerVolumeSource } from '../../core/v1/FlockerVolumeSource';
import { GCEPersistentDiskVolumeSource } from '../../core/v1/GCEPersistentDiskVolumeSource';
import { GitRepoVolumeSource } from '../../core/v1/GitRepoVolumeSource';
import { GlusterfsVolumeSource } from '../../core/v1/GlusterfsVolumeSource';
import { HostPathVolumeSource } from '../../core/v1/HostPathVolumeSource';
import { ISCSIVolumeSource } from '../../core/v1/ISCSIVolumeSource';
import { NFSVolumeSource } from '../../core/v1/NFSVolumeSource';
import { PersistentVolumeClaimVolumeSource } from '../../core/v1/PersistentVolumeClaimVolumeSource';
import { PhotonPersistentDiskVolumeSource } from '../../core/v1/PhotonPersistentDiskVolumeSource';
import { PortworxVolumeSource } from '../../core/v1/PortworxVolumeSource';
import { ProjectedVolumeSource } from '../../core/v1/ProjectedVolumeSource';
import { QuobyteVolumeSource } from '../../core/v1/QuobyteVolumeSource';
import { RBDVolumeSource } from '../../core/v1/RBDVolumeSource';
import { ScaleIOVolumeSource } from '../../core/v1/ScaleIOVolumeSource';
import { SecretVolumeSource } from '../../core/v1/SecretVolumeSource';
import { StorageOSVolumeSource } from '../../core/v1/StorageOSVolumeSource';
import { VsphereVirtualDiskVolumeSource } from '../../core/v1/VsphereVirtualDiskVolumeSource';

/**
 * Volume represents a named volume in a pod that may be accessed by any container in the pod.
 * @interface Volume
 * @see io.k8s.api.core.v1.Volume
 */
export interface Volume {
  /**
   * AWSElasticBlockStore represents an AWS Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://kubernetes.io/docs/concepts/storage/volumes#awselasticblockstore
   * @type AWSElasticBlockStoreVolumeSource
   */
  awsElasticBlockStore ?: AWSElasticBlockStoreVolumeSource

  /**
   * AzureDisk represents an Azure Data Disk mount on the host and bind mount to the pod.
   * @type AzureDiskVolumeSource
   */
  azureDisk ?: AzureDiskVolumeSource

  /**
   * AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
   * @type AzureFileVolumeSource
   */
  azureFile ?: AzureFileVolumeSource

  /**
   * CephFS represents a Ceph FS mount on the host that shares a pod's lifetime
   * @type CephFSVolumeSource
   */
  cephfs ?: CephFSVolumeSource

  /**
   * Cinder represents a cinder volume attached and mounted on kubelets host machine More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
   * @type CinderVolumeSource
   */
  cinder ?: CinderVolumeSource

  /**
   * ConfigMap represents a configMap that should populate this volume
   * @type ConfigMapVolumeSource
   */
  configMap ?: ConfigMapVolumeSource

  /**
   * CSI (Container Storage Interface) represents storage that is handled by an external CSI driver (Alpha feature).
   * @type CSIVolumeSource
   */
  csi ?: CSIVolumeSource

  /**
   * DownwardAPI represents downward API about the pod that should populate this volume
   * @type DownwardAPIVolumeSource
   */
  downwardAPI ?: DownwardAPIVolumeSource

  /**
   * EmptyDir represents a temporary directory that shares a pod's lifetime. More info: https://kubernetes.io/docs/concepts/storage/volumes#emptydir
   * @type EmptyDirVolumeSource
   */
  emptyDir ?: EmptyDirVolumeSource

  /**
   * FC represents a Fibre Channel resource that is attached to a kubelet's host machine and then exposed to the pod.
   * @type FCVolumeSource
   */
  fc ?: FCVolumeSource

  /**
   * FlexVolume represents a generic volume resource that is provisioned/attached using an exec based plugin.
   * @type FlexVolumeSource
   */
  flexVolume ?: FlexVolumeSource

  /**
   * Flocker represents a Flocker volume attached to a kubelet's host machine. This depends on the Flocker control service being running
   * @type FlockerVolumeSource
   */
  flocker ?: FlockerVolumeSource

  /**
   * GCEPersistentDisk represents a GCE Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://kubernetes.io/docs/concepts/storage/volumes#gcepersistentdisk
   * @type GCEPersistentDiskVolumeSource
   */
  gcePersistentDisk ?: GCEPersistentDiskVolumeSource

  /**
   * GitRepo represents a git repository at a particular revision. DEPRECATED: GitRepo is deprecated. To provision a container with a git repo, mount an EmptyDir into an InitContainer that clones the repo using git, then mount the EmptyDir into the Pod's container.
   * @type GitRepoVolumeSource
   */
  gitRepo ?: GitRepoVolumeSource

  /**
   * Glusterfs represents a Glusterfs mount on the host that shares a pod's lifetime. More info: https://releases.k8s.io/HEAD/examples/volumes/glusterfs/README.md
   * @type GlusterfsVolumeSource
   */
  glusterfs ?: GlusterfsVolumeSource

  /**
   * HostPath represents a pre-existing file or directory on the host machine that is directly exposed to the container. This is generally used for system agents or other privileged things that are allowed to see the host machine. Most containers will NOT need this. More info: https://kubernetes.io/docs/concepts/storage/volumes#hostpath
   * @type HostPathVolumeSource
   */
  hostPath ?: HostPathVolumeSource

  /**
   * ISCSI represents an ISCSI Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://releases.k8s.io/HEAD/examples/volumes/iscsi/README.md
   * @type ISCSIVolumeSource
   */
  iscsi ?: ISCSIVolumeSource

  /**
   * Volume's name. Must be a DNS_LABEL and unique within the pod. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
   * @type string
   */
  name : string

  /**
   * NFS represents an NFS mount on the host that shares a pod's lifetime More info: https://kubernetes.io/docs/concepts/storage/volumes#nfs
   * @type NFSVolumeSource
   */
  nfs ?: NFSVolumeSource

  /**
   * PersistentVolumeClaimVolumeSource represents a reference to a PersistentVolumeClaim in the same namespace. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
   * @type PersistentVolumeClaimVolumeSource
   */
  persistentVolumeClaim ?: PersistentVolumeClaimVolumeSource

  /**
   * PhotonPersistentDisk represents a PhotonController persistent disk attached and mounted on kubelets host machine
   * @type PhotonPersistentDiskVolumeSource
   */
  photonPersistentDisk ?: PhotonPersistentDiskVolumeSource

  /**
   * PortworxVolume represents a portworx volume attached and mounted on kubelets host machine
   * @type PortworxVolumeSource
   */
  portworxVolume ?: PortworxVolumeSource

  /**
   * Items for all in one resources secrets, configmaps, and downward API
   * @type ProjectedVolumeSource
   */
  projected ?: ProjectedVolumeSource

  /**
   * Quobyte represents a Quobyte mount on the host that shares a pod's lifetime
   * @type QuobyteVolumeSource
   */
  quobyte ?: QuobyteVolumeSource

  /**
   * RBD represents a Rados Block Device mount on the host that shares a pod's lifetime. More info: https://releases.k8s.io/HEAD/examples/volumes/rbd/README.md
   * @type RBDVolumeSource
   */
  rbd ?: RBDVolumeSource

  /**
   * ScaleIO represents a ScaleIO persistent volume attached and mounted on Kubernetes nodes.
   * @type ScaleIOVolumeSource
   */
  scaleIO ?: ScaleIOVolumeSource

  /**
   * Secret represents a secret that should populate this volume. More info: https://kubernetes.io/docs/concepts/storage/volumes#secret
   * @type SecretVolumeSource
   */
  secret ?: SecretVolumeSource

  /**
   * StorageOS represents a StorageOS volume attached and mounted on Kubernetes nodes.
   * @type StorageOSVolumeSource
   */
  storageos ?: StorageOSVolumeSource

  /**
   * VsphereVolume represents a vSphere volume attached and mounted on kubelets host machine
   * @type VsphereVirtualDiskVolumeSource
   */
  vsphereVolume ?: VsphereVirtualDiskVolumeSource

}
