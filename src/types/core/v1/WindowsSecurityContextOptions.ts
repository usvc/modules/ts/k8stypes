/**
 * WindowsSecurityContextOptions contain Windows-specific options and credentials.
 * @interface WindowsSecurityContextOptions
 * @see io.k8s.api.core.v1.WindowsSecurityContextOptions
 */
export interface WindowsSecurityContextOptions {
  /**
   * GMSACredentialSpec is where the GMSA admission webhook (https://github.com/kubernetes-sigs/windows-gmsa) inlines the contents of the GMSA credential spec named by the GMSACredentialSpecName field. This field is alpha-level and is only honored by servers that enable the WindowsGMSA feature flag.
   * @type string
   */
  gmsaCredentialSpec ?: string

  /**
   * GMSACredentialSpecName is the name of the GMSA credential spec to use. This field is alpha-level and is only honored by servers that enable the WindowsGMSA feature flag.
   * @type string
   */
  gmsaCredentialSpecName ?: string

}
