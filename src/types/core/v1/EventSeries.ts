import { MicroTime } from '../../meta/v1/MicroTime';

/**
 * EventSeries contain information on series of events, i.e. thing that was/is happening continuously for some time.
 * @interface EventSeries
 * @see io.k8s.api.core.v1.EventSeries
 */
export interface EventSeries {
  /**
   * Number of occurrences in this series up to the last heartbeat time
   * @type number
   */
  count ?: number

  /**
   * Time of the last occurrence observed
   * @type MicroTime
   */
  lastObservedTime ?: MicroTime

  /**
   * State of this Series: Ongoing or Finished Deprecated. Planned removal for 1.18
   * @type string
   */
  state ?: string

}
