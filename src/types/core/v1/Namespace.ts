import { NamespaceSpec } from '../../core/v1/NamespaceSpec';
import { NamespaceStatus } from '../../core/v1/NamespaceStatus';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * Namespace provides a scope for Names. Use of multiple namespaces is optional.
 * @interface Namespace
 * @see io.k8s.api.core.v1.Namespace
 */
export interface Namespace {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the behavior of the Namespace. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type NamespaceSpec
   */
  spec ?: NamespaceSpec

  /**
   * Status describes the current status of a Namespace. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type NamespaceStatus
   */
  status ?: NamespaceStatus

}
