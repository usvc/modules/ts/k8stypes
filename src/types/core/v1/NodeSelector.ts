import { NodeSelectorTerm } from '../../core/v1/NodeSelectorTerm';

/**
 * A node selector represents the union of the results of one or more label queries over a set of nodes; that is, it represents the OR of the selectors represented by the node selector terms.
 * @interface NodeSelector
 * @see io.k8s.api.core.v1.NodeSelector
 */
export interface NodeSelector {
  /**
   * Required. A list of node selector terms. The terms are ORed.
   * @type NodeSelectorTerm[]
   */
  nodeSelectorTerms : NodeSelectorTerm[]

}
