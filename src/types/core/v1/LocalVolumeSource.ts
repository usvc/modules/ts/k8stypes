/**
 * Local represents directly-attached storage with node affinity (Beta feature)
 * @interface LocalVolumeSource
 * @see io.k8s.api.core.v1.LocalVolumeSource
 */
export interface LocalVolumeSource {
  /**
   * Filesystem type to mount. It applies only when the Path is a block device. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". The default value is to auto-select a fileystem if unspecified.
   * @type string
   */
  fsType ?: string

  /**
   * The full path to the volume on the node. It can be either a directory or block device (disk, partition, ...).
   * @type string
   */
  path : string

}
