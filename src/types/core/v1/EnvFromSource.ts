import { ConfigMapEnvSource } from '../../core/v1/ConfigMapEnvSource';
import { SecretEnvSource } from '../../core/v1/SecretEnvSource';

/**
 * EnvFromSource represents the source of a set of ConfigMaps
 * @interface EnvFromSource
 * @see io.k8s.api.core.v1.EnvFromSource
 */
export interface EnvFromSource {
  /**
   * The ConfigMap to select from
   * @type ConfigMapEnvSource
   */
  configMapRef ?: ConfigMapEnvSource

  /**
   * An optional identifier to prepend to each key in the ConfigMap. Must be a C_IDENTIFIER.
   * @type string
   */
  prefix ?: string

  /**
   * The Secret to select from
   * @type SecretEnvSource
   */
  secretRef ?: SecretEnvSource

}
