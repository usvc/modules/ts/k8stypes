/**
 * Sysctl defines a kernel parameter to be set
 * @interface Sysctl
 * @see io.k8s.api.core.v1.Sysctl
 */
export interface Sysctl {
  /**
   * Name of a property to set
   * @type string
   */
  name : string

  /**
   * Value of a property to set
   * @type string
   */
  value : string

}
