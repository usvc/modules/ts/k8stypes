import { ScopedResourceSelectorRequirement } from '../../core/v1/ScopedResourceSelectorRequirement';

/**
 * A scope selector represents the AND of the selectors represented by the scoped-resource selector requirements.
 * @interface ScopeSelector
 * @see io.k8s.api.core.v1.ScopeSelector
 */
export interface ScopeSelector {
  /**
   * A list of scope selector requirements by scope of the resources.
   * @type ScopedResourceSelectorRequirement[]
   */
  matchExpressions ?: ScopedResourceSelectorRequirement[]

}
