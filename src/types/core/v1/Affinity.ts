import { NodeAffinity } from '../../core/v1/NodeAffinity';
import { PodAffinity } from '../../core/v1/PodAffinity';
import { PodAntiAffinity } from '../../core/v1/PodAntiAffinity';

/**
 * Affinity is a group of affinity scheduling rules.
 * @interface Affinity
 * @see io.k8s.api.core.v1.Affinity
 */
export interface Affinity {
  /**
   * Describes node affinity scheduling rules for the pod.
   * @type NodeAffinity
   */
  nodeAffinity ?: NodeAffinity

  /**
   * Describes pod affinity scheduling rules (e.g. co-locate this pod in the same node, zone, etc. as some other pod(s)).
   * @type PodAffinity
   */
  podAffinity ?: PodAffinity

  /**
   * Describes pod anti-affinity scheduling rules (e.g. avoid putting this pod in the same node, zone, etc. as some other pod(s)).
   * @type PodAntiAffinity
   */
  podAntiAffinity ?: PodAntiAffinity

}
