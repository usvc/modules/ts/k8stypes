/**
 * VolumeMount describes a mounting of a Volume within a container.
 * @interface VolumeMount
 * @see io.k8s.api.core.v1.VolumeMount
 */
export interface VolumeMount {
  /**
   * Path within the container at which the volume should be mounted.  Must not contain ':'.
   * @type string
   */
  mountPath : string

  /**
   * mountPropagation determines how mounts are propagated from the host to container and the other way around. When not set, MountPropagationNone is used. This field is beta in 1.10.
   * @type string
   */
  mountPropagation ?: string

  /**
   * This must match the Name of a Volume.
   * @type string
   */
  name : string

  /**
   * Mounted read-only if true, read-write otherwise (false or unspecified). Defaults to false.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Path within the volume from which the container's volume should be mounted. Defaults to "" (volume's root).
   * @type string
   */
  subPath ?: string

  /**
   * Expanded path within the volume from which the container's volume should be mounted. Behaves similarly to SubPath but environment variable references $(VAR_NAME) are expanded using the container's environment. Defaults to "" (volume's root). SubPathExpr and SubPath are mutually exclusive. This field is beta in 1.15.
   * @type string
   */
  subPathExpr ?: string

}
