import { Quantity } from '../../api/resource/Quantity';

/**
 * Represents an empty directory for a pod. Empty directory volumes support ownership management and SELinux relabeling.
 * @interface EmptyDirVolumeSource
 * @see io.k8s.api.core.v1.EmptyDirVolumeSource
 */
export interface EmptyDirVolumeSource {
  /**
   * What type of storage medium should back this directory. The default is "" which means to use the node's default medium. Must be an empty string (default) or Memory. More info: https://kubernetes.io/docs/concepts/storage/volumes#emptydir
   * @type string
   */
  medium ?: string

  /**
   * Total amount of local storage required for this EmptyDir volume. The size limit is also applicable for memory medium. The maximum usage on memory medium EmptyDir would be the minimum value between the SizeLimit specified here and the sum of memory limits of all containers in a pod. The default is nil which means that the limit is undefined. More info: http://kubernetes.io/docs/user-guide/volumes#emptydir
   * @type Quantity
   */
  sizeLimit ?: Quantity

}
