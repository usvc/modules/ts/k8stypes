import { ComponentCondition } from '../../core/v1/ComponentCondition';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * ComponentStatus (and ComponentStatusList) holds the cluster validation info.
 * @interface ComponentStatus
 * @see io.k8s.api.core.v1.ComponentStatus
 */
export interface ComponentStatus {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * List of component conditions observed
   * @type ComponentCondition[]
   */
  conditions ?: ComponentCondition[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

}
