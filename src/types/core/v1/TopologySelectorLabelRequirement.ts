/**
 * A topology selector requirement is a selector that matches given label. This is an alpha feature and may change in the future.
 * @interface TopologySelectorLabelRequirement
 * @see io.k8s.api.core.v1.TopologySelectorLabelRequirement
 */
export interface TopologySelectorLabelRequirement {
  /**
   * The label key that the selector applies to.
   * @type string
   */
  key : string

  /**
   * An array of string values. One value must match the label to be selected. Each entry in Values is ORed.
   * @type string[]
   */
  values : string[]

}
