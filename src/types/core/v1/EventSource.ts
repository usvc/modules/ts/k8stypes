/**
 * EventSource contains information for an event.
 * @interface EventSource
 * @see io.k8s.api.core.v1.EventSource
 */
export interface EventSource {
  /**
   * Component from which the event is generated.
   * @type string
   */
  component ?: string

  /**
   * Node name on which the event is generated.
   * @type string
   */
  host ?: string

}
