/**
 * HTTPHeader describes a custom header to be used in HTTP probes
 * @interface HTTPHeader
 * @see io.k8s.api.core.v1.HTTPHeader
 */
export interface HTTPHeader {
  /**
   * The header field name
   * @type string
   */
  name : string

  /**
   * The header field value
   * @type string
   */
  value : string

}
