import { Time } from '../../meta/v1/Time';

/**
 * ContainerStateTerminated is a terminated state of a container.
 * @interface ContainerStateTerminated
 * @see io.k8s.api.core.v1.ContainerStateTerminated
 */
export interface ContainerStateTerminated {
  /**
   * Container's ID in the format 'docker://<container_id>'
   * @type string
   */
  containerID ?: string

  /**
   * Exit status from the last termination of the container
   * @type number
   */
  exitCode : number

  /**
   * Time at which the container last terminated
   * @type Time
   */
  finishedAt ?: Time

  /**
   * Message regarding the last termination of the container
   * @type string
   */
  message ?: string

  /**
   * (brief) reason from the last termination of the container
   * @type string
   */
  reason ?: string

  /**
   * Signal from the last termination of the container
   * @type number
   */
  signal ?: number

  /**
   * Time at which previous execution of the container started
   * @type Time
   */
  startedAt ?: Time

}
