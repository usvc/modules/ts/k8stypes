import { Quantity } from '../../api/resource/Quantity';

/**
 * LimitRangeItem defines a min/max usage limit for any resource that matches on kind.
 * @interface LimitRangeItem
 * @see io.k8s.api.core.v1.LimitRangeItem
 */
export interface LimitRangeItem {
  /**
   * Default resource requirement limit value by resource name if resource limit is omitted.
   * @type {[key : string] : Quantity}
   */
  default ?: {[key : string] : Quantity}

  /**
   * DefaultRequest is the default resource requirement request value by resource name if resource request is omitted.
   * @type {[key : string] : Quantity}
   */
  defaultRequest ?: {[key : string] : Quantity}

  /**
   * Max usage constraints on this kind by resource name.
   * @type {[key : string] : Quantity}
   */
  max ?: {[key : string] : Quantity}

  /**
   * MaxLimitRequestRatio if specified, the named resource must have a request and limit that are both non-zero where limit divided by request is less than or equal to the enumerated value; this represents the max burst for the named resource.
   * @type {[key : string] : Quantity}
   */
  maxLimitRequestRatio ?: {[key : string] : Quantity}

  /**
   * Min usage constraints on this kind by resource name.
   * @type {[key : string] : Quantity}
   */
  min ?: {[key : string] : Quantity}

  /**
   * Type of resource that this limit applies to.
   * @type string
   */
  type ?: string

}
