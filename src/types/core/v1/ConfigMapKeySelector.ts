/**
 * Selects a key from a ConfigMap.
 * @interface ConfigMapKeySelector
 * @see io.k8s.api.core.v1.ConfigMapKeySelector
 */
export interface ConfigMapKeySelector {
  /**
   * The key to select.
   * @type string
   */
  key : string

  /**
   * Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
   * @type string
   */
  name ?: string

  /**
   * Specify whether the ConfigMap or its key must be defined
   * @type boolean
   */
  optional ?: boolean

}
