/**
 * Represents a Flocker volume mounted by the Flocker agent. One and only one of datasetName and datasetUUID should be set. Flocker volumes do not support ownership management or SELinux relabeling.
 * @interface FlockerVolumeSource
 * @see io.k8s.api.core.v1.FlockerVolumeSource
 */
export interface FlockerVolumeSource {
  /**
   * Name of the dataset stored as metadata -> name on the dataset for Flocker should be considered as deprecated
   * @type string
   */
  datasetName ?: string

  /**
   * UUID of the dataset. This is unique identifier of a Flocker dataset
   * @type string
   */
  datasetUUID ?: string

}
