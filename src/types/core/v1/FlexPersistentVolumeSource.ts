import { SecretReference } from '../../core/v1/SecretReference';

/**
 * FlexPersistentVolumeSource represents a generic persistent volume resource that is provisioned/attached using an exec based plugin.
 * @interface FlexPersistentVolumeSource
 * @see io.k8s.api.core.v1.FlexPersistentVolumeSource
 */
export interface FlexPersistentVolumeSource {
  /**
   * Driver is the name of the driver to use for this volume.
   * @type string
   */
  driver : string

  /**
   * Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". The default filesystem depends on FlexVolume script.
   * @type string
   */
  fsType ?: string

  /**
   * Optional: Extra command options if any.
   * @type {[key : string] : string}
   */
  options ?: {[key : string] : string}

  /**
   * Optional: Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Optional: SecretRef is reference to the secret object containing sensitive information to pass to the plugin scripts. This may be empty if no secret object is specified. If the secret object contains more than one secret, all secrets are passed to the plugin scripts.
   * @type SecretReference
   */
  secretRef ?: SecretReference

}
