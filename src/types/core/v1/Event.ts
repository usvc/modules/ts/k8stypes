import { EventSeries } from '../../core/v1/EventSeries';
import { EventSource } from '../../core/v1/EventSource';
import { MicroTime } from '../../meta/v1/MicroTime';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { ObjectReference } from '../../core/v1/ObjectReference';
import { Time } from '../../meta/v1/Time';

/**
 * Event is a report of an event somewhere in the cluster.
 * @interface Event
 * @see io.k8s.api.core.v1.Event
 */
export interface Event {
  /**
   * What action was taken/failed regarding to the Regarding object.
   * @type string
   */
  action ?: string

  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * The number of times this event has occurred.
   * @type number
   */
  count ?: number

  /**
   * Time when this Event was first observed.
   * @type MicroTime
   */
  eventTime ?: MicroTime

  /**
   * The time at which the event was first recorded. (Time of server receipt is in TypeMeta.)
   * @type Time
   */
  firstTimestamp ?: Time

  /**
   * The object that this event is about.
   * @type ObjectReference
   */
  involvedObject : ObjectReference

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * The time at which the most recent occurrence of this event was recorded.
   * @type Time
   */
  lastTimestamp ?: Time

  /**
   * A human-readable description of the status of this operation.
   * @type string
   */
  message ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata : ObjectMeta

  /**
   * This should be a short, machine understandable string that gives the reason for the transition into the object's current status.
   * @type string
   */
  reason ?: string

  /**
   * Optional secondary object for more complex actions.
   * @type ObjectReference
   */
  related ?: ObjectReference

  /**
   * Name of the controller that emitted this Event, e.g. `kubernetes.io/kubelet`.
   * @type string
   */
  reportingComponent ?: string

  /**
   * ID of the controller instance, e.g. `kubelet-xyzf`.
   * @type string
   */
  reportingInstance ?: string

  /**
   * Data about the Event series this event represents or nil if it's a singleton Event.
   * @type EventSeries
   */
  series ?: EventSeries

  /**
   * The component reporting this event. Should be a short machine understandable string.
   * @type EventSource
   */
  source ?: EventSource

  /**
   * Type of this event (Normal, Warning), new types could be added in the future
   * @type string
   */
  type ?: string

}
