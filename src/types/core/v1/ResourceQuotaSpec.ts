import { Quantity } from '../../api/resource/Quantity';
import { ScopeSelector } from '../../core/v1/ScopeSelector';

/**
 * ResourceQuotaSpec defines the desired hard limits to enforce for Quota.
 * @interface ResourceQuotaSpec
 * @see io.k8s.api.core.v1.ResourceQuotaSpec
 */
export interface ResourceQuotaSpec {
  /**
   * hard is the set of desired hard limits for each named resource. More info: https://kubernetes.io/docs/concepts/policy/resource-quotas/
   * @type {[key : string] : Quantity}
   */
  hard ?: {[key : string] : Quantity}

  /**
   * scopeSelector is also a collection of filters like scopes that must match each object tracked by a quota but expressed using ScopeSelectorOperator in combination with possible values. For a resource to match, both scopes AND scopeSelector (if specified in spec), must be matched.
   * @type ScopeSelector
   */
  scopeSelector ?: ScopeSelector

  /**
   * A collection of filters that must match each object tracked by a quota. If not specified, the quota matches all objects.
   * @type string[]
   */
  scopes ?: string[]

}
