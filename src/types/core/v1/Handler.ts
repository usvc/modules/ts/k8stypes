import { ExecAction } from '../../core/v1/ExecAction';
import { HTTPGetAction } from '../../core/v1/HTTPGetAction';
import { TCPSocketAction } from '../../core/v1/TCPSocketAction';

/**
 * Handler defines a specific action that should be taken
 * @interface Handler
 * @see io.k8s.api.core.v1.Handler
 */
export interface Handler {
  /**
   * One and only one of the following should be specified. Exec specifies the action to take.
   * @type ExecAction
   */
  exec ?: ExecAction

  /**
   * HTTPGet specifies the http request to perform.
   * @type HTTPGetAction
   */
  httpGet ?: HTTPGetAction

  /**
   * TCPSocket specifies an action involving a TCP port. TCP hooks not yet supported
   * @type TCPSocketAction
   */
  tcpSocket ?: TCPSocketAction

}
