/**
 * ContainerPort represents a network port in a single container.
 * @interface ContainerPort
 * @see io.k8s.api.core.v1.ContainerPort
 */
export interface ContainerPort {
  /**
   * Number of port to expose on the pod's IP address. This must be a valid port number, 0 < x < 65536.
   * @type number
   */
  containerPort : number

  /**
   * What host IP to bind the external port to.
   * @type string
   */
  hostIP ?: string

  /**
   * Number of port to expose on the host. If specified, this must be a valid port number, 0 < x < 65536. If HostNetwork is specified, this must match ContainerPort. Most containers do not need this.
   * @type number
   */
  hostPort ?: number

  /**
   * If specified, this must be an IANA_SVC_NAME and unique within the pod. Each named port in a pod must have a unique name. Name for the port that can be referred to by services.
   * @type string
   */
  name ?: string

  /**
   * Protocol for port. Must be UDP, TCP, or SCTP. Defaults to "TCP".
   * @type string
   */
  protocol ?: string

}
