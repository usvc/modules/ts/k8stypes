/**
 * Adds and removes POSIX capabilities from running containers.
 * @interface Capabilities
 * @see io.k8s.api.core.v1.Capabilities
 */
export interface Capabilities {
  /**
   * Added capabilities
   * @type string[]
   */
  add ?: string[]

  /**
   * Removed capabilities
   * @type string[]
   */
  drop ?: string[]

}
