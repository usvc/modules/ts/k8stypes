import { PersistentVolumeClaimCondition } from '../../core/v1/PersistentVolumeClaimCondition';
import { Quantity } from '../../api/resource/Quantity';

/**
 * PersistentVolumeClaimStatus is the current status of a persistent volume claim.
 * @interface PersistentVolumeClaimStatus
 * @see io.k8s.api.core.v1.PersistentVolumeClaimStatus
 */
export interface PersistentVolumeClaimStatus {
  /**
   * AccessModes contains the actual access modes the volume backing the PVC has. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes-1
   * @type string[]
   */
  accessModes ?: string[]

  /**
   * Represents the actual resources of the underlying volume.
   * @type {[key : string] : Quantity}
   */
  capacity ?: {[key : string] : Quantity}

  /**
   * Current Condition of persistent volume claim. If underlying persistent volume is being resized then the Condition will be set to 'ResizeStarted'.
   * @type PersistentVolumeClaimCondition[]
   */
  conditions ?: PersistentVolumeClaimCondition[]

  /**
   * Phase represents the current phase of PersistentVolumeClaim.
   * @type string
   */
  phase ?: string

}
