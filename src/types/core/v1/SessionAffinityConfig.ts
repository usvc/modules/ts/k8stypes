import { ClientIPConfig } from '../../core/v1/ClientIPConfig';

/**
 * SessionAffinityConfig represents the configurations of session affinity.
 * @interface SessionAffinityConfig
 * @see io.k8s.api.core.v1.SessionAffinityConfig
 */
export interface SessionAffinityConfig {
  /**
   * clientIP contains the configurations of Client IP based session affinity.
   * @type ClientIPConfig
   */
  clientIP ?: ClientIPConfig

}
