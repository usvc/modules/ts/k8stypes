import { Time } from '../../meta/v1/Time';

/**
 * ContainerStateRunning is a running state of a container.
 * @interface ContainerStateRunning
 * @see io.k8s.api.core.v1.ContainerStateRunning
 */
export interface ContainerStateRunning {
  /**
   * Time at which the container was last (re-)started
   * @type Time
   */
  startedAt ?: Time

}
