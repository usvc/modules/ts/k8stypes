import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PersistentVolumeClaimSpec } from '../../core/v1/PersistentVolumeClaimSpec';
import { PersistentVolumeClaimStatus } from '../../core/v1/PersistentVolumeClaimStatus';

/**
 * PersistentVolumeClaim is a user's request for and claim to a persistent volume
 * @interface PersistentVolumeClaim
 * @see io.k8s.api.core.v1.PersistentVolumeClaim
 */
export interface PersistentVolumeClaim {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec defines the desired characteristics of a volume requested by a pod author. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
   * @type PersistentVolumeClaimSpec
   */
  spec ?: PersistentVolumeClaimSpec

  /**
   * Status represents the current information/status of a persistent volume claim. Read-only. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
   * @type PersistentVolumeClaimStatus
   */
  status ?: PersistentVolumeClaimStatus

}
