import { NodeConfigSource } from '../../core/v1/NodeConfigSource';
import { Taint } from '../../core/v1/Taint';

/**
 * NodeSpec describes the attributes that a node is created with.
 * @interface NodeSpec
 * @see io.k8s.api.core.v1.NodeSpec
 */
export interface NodeSpec {
  /**
   * If specified, the source to get node configuration from The DynamicKubeletConfig feature gate must be enabled for the Kubelet to use this field
   * @type NodeConfigSource
   */
  configSource ?: NodeConfigSource

  /**
   * Deprecated. Not all kubelets will set this field. Remove field after 1.13. see: https://issues.k8s.io/61966
   * @type string
   */
  externalID ?: string

  /**
   * PodCIDR represents the pod IP range assigned to the node.
   * @type string
   */
  podCIDR ?: string

  /**
   * ID of the node assigned by the cloud provider in the format: <ProviderName>://<ProviderSpecificNodeID>
   * @type string
   */
  providerID ?: string

  /**
   * If specified, the node's taints.
   * @type Taint[]
   */
  taints ?: Taint[]

  /**
   * Unschedulable controls node schedulability of new pods. By default, node is schedulable. More info: https://kubernetes.io/docs/concepts/nodes/node/#manual-node-administration
   * @type boolean
   */
  unschedulable ?: boolean

}
