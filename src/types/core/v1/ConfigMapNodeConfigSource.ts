/**
 * ConfigMapNodeConfigSource contains the information to reference a ConfigMap as a config source for the Node.
 * @interface ConfigMapNodeConfigSource
 * @see io.k8s.api.core.v1.ConfigMapNodeConfigSource
 */
export interface ConfigMapNodeConfigSource {
  /**
   * KubeletConfigKey declares which key of the referenced ConfigMap corresponds to the KubeletConfiguration structure This field is required in all cases.
   * @type string
   */
  kubeletConfigKey : string

  /**
   * Name is the metadata.name of the referenced ConfigMap. This field is required in all cases.
   * @type string
   */
  name : string

  /**
   * Namespace is the metadata.namespace of the referenced ConfigMap. This field is required in all cases.
   * @type string
   */
  namespace : string

  /**
   * ResourceVersion is the metadata.ResourceVersion of the referenced ConfigMap. This field is forbidden in Node.Spec, and required in Node.Status.
   * @type string
   */
  resourceVersion ?: string

  /**
   * UID is the metadata.UID of the referenced ConfigMap. This field is forbidden in Node.Spec, and required in Node.Status.
   * @type string
   */
  uid ?: string

}
