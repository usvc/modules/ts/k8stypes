import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { PodSpec } from '../../core/v1/PodSpec';
import { PodStatus } from '../../core/v1/PodStatus';

/**
 * Pod is a collection of containers that can run on a host. This resource is created by clients and scheduled onto hosts.
 * @interface Pod
 * @see io.k8s.api.core.v1.Pod
 */
export interface Pod {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired behavior of the pod. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type PodSpec
   */
  spec ?: PodSpec

  /**
   * Most recently observed status of the pod. This data may not be up to date. Populated by the system. Read-only. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
   * @type PodStatus
   */
  status ?: PodStatus

}
