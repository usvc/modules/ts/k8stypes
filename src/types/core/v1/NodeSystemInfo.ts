/**
 * NodeSystemInfo is a set of ids/uuids to uniquely identify the node.
 * @interface NodeSystemInfo
 * @see io.k8s.api.core.v1.NodeSystemInfo
 */
export interface NodeSystemInfo {
  /**
   * The Architecture reported by the node
   * @type string
   */
  architecture : string

  /**
   * Boot ID reported by the node.
   * @type string
   */
  bootID : string

  /**
   * ContainerRuntime Version reported by the node through runtime remote API (e.g. docker://1.5.0).
   * @type string
   */
  containerRuntimeVersion : string

  /**
   * Kernel Version reported by the node from 'uname -r' (e.g. 3.16.0-0.bpo.4-amd64).
   * @type string
   */
  kernelVersion : string

  /**
   * KubeProxy Version reported by the node.
   * @type string
   */
  kubeProxyVersion : string

  /**
   * Kubelet Version reported by the node.
   * @type string
   */
  kubeletVersion : string

  /**
   * MachineID reported by the node. For unique machine identification in the cluster this field is preferred. Learn more from man(5) machine-id: http://man7.org/linux/man-pages/man5/machine-id.5.html
   * @type string
   */
  machineID : string

  /**
   * The Operating System reported by the node
   * @type string
   */
  operatingSystem : string

  /**
   * OS Image reported by the node from /etc/os-release (e.g. Debian GNU/Linux 7 (wheezy)).
   * @type string
   */
  osImage : string

  /**
   * SystemUUID reported by the node. For unique machine identification MachineID is preferred. This field is specific to Red Hat hosts https://access.redhat.com/documentation/en-US/Red_Hat_Subscription_Management/1/html/RHSM/getting-system-uuid.html
   * @type string
   */
  systemUUID : string

}
