import { AWSElasticBlockStoreVolumeSource } from '../../core/v1/AWSElasticBlockStoreVolumeSource';
import { AzureDiskVolumeSource } from '../../core/v1/AzureDiskVolumeSource';
import { AzureFilePersistentVolumeSource } from '../../core/v1/AzureFilePersistentVolumeSource';
import { CephFSPersistentVolumeSource } from '../../core/v1/CephFSPersistentVolumeSource';
import { CinderPersistentVolumeSource } from '../../core/v1/CinderPersistentVolumeSource';
import { CSIPersistentVolumeSource } from '../../core/v1/CSIPersistentVolumeSource';
import { FCVolumeSource } from '../../core/v1/FCVolumeSource';
import { FlexPersistentVolumeSource } from '../../core/v1/FlexPersistentVolumeSource';
import { FlockerVolumeSource } from '../../core/v1/FlockerVolumeSource';
import { GCEPersistentDiskVolumeSource } from '../../core/v1/GCEPersistentDiskVolumeSource';
import { GlusterfsPersistentVolumeSource } from '../../core/v1/GlusterfsPersistentVolumeSource';
import { HostPathVolumeSource } from '../../core/v1/HostPathVolumeSource';
import { ISCSIPersistentVolumeSource } from '../../core/v1/ISCSIPersistentVolumeSource';
import { LocalVolumeSource } from '../../core/v1/LocalVolumeSource';
import { NFSVolumeSource } from '../../core/v1/NFSVolumeSource';
import { ObjectReference } from '../../core/v1/ObjectReference';
import { PhotonPersistentDiskVolumeSource } from '../../core/v1/PhotonPersistentDiskVolumeSource';
import { PortworxVolumeSource } from '../../core/v1/PortworxVolumeSource';
import { Quantity } from '../../api/resource/Quantity';
import { QuobyteVolumeSource } from '../../core/v1/QuobyteVolumeSource';
import { RBDPersistentVolumeSource } from '../../core/v1/RBDPersistentVolumeSource';
import { ScaleIOPersistentVolumeSource } from '../../core/v1/ScaleIOPersistentVolumeSource';
import { StorageOSPersistentVolumeSource } from '../../core/v1/StorageOSPersistentVolumeSource';
import { VolumeNodeAffinity } from '../../core/v1/VolumeNodeAffinity';
import { VsphereVirtualDiskVolumeSource } from '../../core/v1/VsphereVirtualDiskVolumeSource';

/**
 * PersistentVolumeSpec is the specification of a persistent volume.
 * @interface PersistentVolumeSpec
 * @see io.k8s.api.core.v1.PersistentVolumeSpec
 */
export interface PersistentVolumeSpec {
  /**
   * AccessModes contains all ways the volume can be mounted. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes
   * @type string[]
   */
  accessModes ?: string[]

  /**
   * AWSElasticBlockStore represents an AWS Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://kubernetes.io/docs/concepts/storage/volumes#awselasticblockstore
   * @type AWSElasticBlockStoreVolumeSource
   */
  awsElasticBlockStore ?: AWSElasticBlockStoreVolumeSource

  /**
   * AzureDisk represents an Azure Data Disk mount on the host and bind mount to the pod.
   * @type AzureDiskVolumeSource
   */
  azureDisk ?: AzureDiskVolumeSource

  /**
   * AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
   * @type AzureFilePersistentVolumeSource
   */
  azureFile ?: AzureFilePersistentVolumeSource

  /**
   * A description of the persistent volume's resources and capacity. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#capacity
   * @type {[key : string] : Quantity}
   */
  capacity ?: {[key : string] : Quantity}

  /**
   * CephFS represents a Ceph FS mount on the host that shares a pod's lifetime
   * @type CephFSPersistentVolumeSource
   */
  cephfs ?: CephFSPersistentVolumeSource

  /**
   * Cinder represents a cinder volume attached and mounted on kubelets host machine More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
   * @type CinderPersistentVolumeSource
   */
  cinder ?: CinderPersistentVolumeSource

  /**
   * ClaimRef is part of a bi-directional binding between PersistentVolume and PersistentVolumeClaim. Expected to be non-nil when bound. claim.VolumeName is the authoritative bind between PV and PVC. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#binding
   * @type ObjectReference
   */
  claimRef ?: ObjectReference

  /**
   * CSI represents storage that is handled by an external CSI driver (Beta feature).
   * @type CSIPersistentVolumeSource
   */
  csi ?: CSIPersistentVolumeSource

  /**
   * FC represents a Fibre Channel resource that is attached to a kubelet's host machine and then exposed to the pod.
   * @type FCVolumeSource
   */
  fc ?: FCVolumeSource

  /**
   * FlexVolume represents a generic volume resource that is provisioned/attached using an exec based plugin.
   * @type FlexPersistentVolumeSource
   */
  flexVolume ?: FlexPersistentVolumeSource

  /**
   * Flocker represents a Flocker volume attached to a kubelet's host machine and exposed to the pod for its usage. This depends on the Flocker control service being running
   * @type FlockerVolumeSource
   */
  flocker ?: FlockerVolumeSource

  /**
   * GCEPersistentDisk represents a GCE Disk resource that is attached to a kubelet's host machine and then exposed to the pod. Provisioned by an admin. More info: https://kubernetes.io/docs/concepts/storage/volumes#gcepersistentdisk
   * @type GCEPersistentDiskVolumeSource
   */
  gcePersistentDisk ?: GCEPersistentDiskVolumeSource

  /**
   * Glusterfs represents a Glusterfs volume that is attached to a host and exposed to the pod. Provisioned by an admin. More info: https://releases.k8s.io/HEAD/examples/volumes/glusterfs/README.md
   * @type GlusterfsPersistentVolumeSource
   */
  glusterfs ?: GlusterfsPersistentVolumeSource

  /**
   * HostPath represents a directory on the host. Provisioned by a developer or tester. This is useful for single-node development and testing only! On-host storage is not supported in any way and WILL NOT WORK in a multi-node cluster. More info: https://kubernetes.io/docs/concepts/storage/volumes#hostpath
   * @type HostPathVolumeSource
   */
  hostPath ?: HostPathVolumeSource

  /**
   * ISCSI represents an ISCSI Disk resource that is attached to a kubelet's host machine and then exposed to the pod. Provisioned by an admin.
   * @type ISCSIPersistentVolumeSource
   */
  iscsi ?: ISCSIPersistentVolumeSource

  /**
   * Local represents directly-attached storage with node affinity
   * @type LocalVolumeSource
   */
  local ?: LocalVolumeSource

  /**
   * A list of mount options, e.g. ["ro", "soft"]. Not validated - mount will simply fail if one is invalid. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#mount-options
   * @type string[]
   */
  mountOptions ?: string[]

  /**
   * NFS represents an NFS mount on the host. Provisioned by an admin. More info: https://kubernetes.io/docs/concepts/storage/volumes#nfs
   * @type NFSVolumeSource
   */
  nfs ?: NFSVolumeSource

  /**
   * NodeAffinity defines constraints that limit what nodes this volume can be accessed from. This field influences the scheduling of pods that use this volume.
   * @type VolumeNodeAffinity
   */
  nodeAffinity ?: VolumeNodeAffinity

  /**
   * What happens to a persistent volume when released from its claim. Valid options are Retain (default for manually created PersistentVolumes), Delete (default for dynamically provisioned PersistentVolumes), and Recycle (deprecated). Recycle must be supported by the volume plugin underlying this PersistentVolume. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#reclaiming
   * @type string
   */
  persistentVolumeReclaimPolicy ?: string

  /**
   * PhotonPersistentDisk represents a PhotonController persistent disk attached and mounted on kubelets host machine
   * @type PhotonPersistentDiskVolumeSource
   */
  photonPersistentDisk ?: PhotonPersistentDiskVolumeSource

  /**
   * PortworxVolume represents a portworx volume attached and mounted on kubelets host machine
   * @type PortworxVolumeSource
   */
  portworxVolume ?: PortworxVolumeSource

  /**
   * Quobyte represents a Quobyte mount on the host that shares a pod's lifetime
   * @type QuobyteVolumeSource
   */
  quobyte ?: QuobyteVolumeSource

  /**
   * RBD represents a Rados Block Device mount on the host that shares a pod's lifetime. More info: https://releases.k8s.io/HEAD/examples/volumes/rbd/README.md
   * @type RBDPersistentVolumeSource
   */
  rbd ?: RBDPersistentVolumeSource

  /**
   * ScaleIO represents a ScaleIO persistent volume attached and mounted on Kubernetes nodes.
   * @type ScaleIOPersistentVolumeSource
   */
  scaleIO ?: ScaleIOPersistentVolumeSource

  /**
   * Name of StorageClass to which this persistent volume belongs. Empty value means that this volume does not belong to any StorageClass.
   * @type string
   */
  storageClassName ?: string

  /**
   * StorageOS represents a StorageOS volume that is attached to the kubelet's host machine and mounted into the pod More info: https://releases.k8s.io/HEAD/examples/volumes/storageos/README.md
   * @type StorageOSPersistentVolumeSource
   */
  storageos ?: StorageOSPersistentVolumeSource

  /**
   * volumeMode defines if a volume is intended to be used with a formatted filesystem or to remain in raw block state. Value of Filesystem is implied when not included in spec. This is a beta feature.
   * @type string
   */
  volumeMode ?: string

  /**
   * VsphereVolume represents a vSphere volume attached and mounted on kubelets host machine
   * @type VsphereVirtualDiskVolumeSource
   */
  vsphereVolume ?: VsphereVirtualDiskVolumeSource

}
