/**
 * AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
 * @interface AzureFilePersistentVolumeSource
 * @see io.k8s.api.core.v1.AzureFilePersistentVolumeSource
 */
export interface AzureFilePersistentVolumeSource {
  /**
   * Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * the name of secret that contains Azure Storage Account Name and Key
   * @type string
   */
  secretName : string

  /**
   * the namespace of the secret that contains Azure Storage Account Name and Key default is the same as the Pod
   * @type string
   */
  secretNamespace ?: string

  /**
   * Share Name
   * @type string
   */
  shareName : string

}
