/**
 * ContainerStateWaiting is a waiting state of a container.
 * @interface ContainerStateWaiting
 * @see io.k8s.api.core.v1.ContainerStateWaiting
 */
export interface ContainerStateWaiting {
  /**
   * Message regarding why the container is not yet running.
   * @type string
   */
  message ?: string

  /**
   * (brief) reason the container is not yet running.
   * @type string
   */
  reason ?: string

}
