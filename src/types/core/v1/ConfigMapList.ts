import { ConfigMap } from '../../core/v1/ConfigMap';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * ConfigMapList is a resource containing a list of ConfigMap objects.
 * @interface ConfigMapList
 * @see io.k8s.api.core.v1.ConfigMapList
 */
export interface ConfigMapList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * Items is the list of ConfigMaps.
   * @type ConfigMap[]
   */
  items : ConfigMap[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
