/**
 * DaemonEndpoint contains information about a single Daemon endpoint.
 * @interface DaemonEndpoint
 * @see io.k8s.api.core.v1.DaemonEndpoint
 */
export interface DaemonEndpoint {
  /**
   * Port number of the given endpoint.
   * @type number
   */
  Port : number

}
