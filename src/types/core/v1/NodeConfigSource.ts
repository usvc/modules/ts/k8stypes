import { ConfigMapNodeConfigSource } from '../../core/v1/ConfigMapNodeConfigSource';

/**
 * NodeConfigSource specifies a source of node configuration. Exactly one subfield (excluding metadata) must be non-nil.
 * @interface NodeConfigSource
 * @see io.k8s.api.core.v1.NodeConfigSource
 */
export interface NodeConfigSource {
  /**
   * ConfigMap is a reference to a Node's ConfigMap
   * @type ConfigMapNodeConfigSource
   */
  configMap ?: ConfigMapNodeConfigSource

}
