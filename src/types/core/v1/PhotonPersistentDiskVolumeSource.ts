/**
 * Represents a Photon Controller persistent disk resource.
 * @interface PhotonPersistentDiskVolumeSource
 * @see io.k8s.api.core.v1.PhotonPersistentDiskVolumeSource
 */
export interface PhotonPersistentDiskVolumeSource {
  /**
   * Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified.
   * @type string
   */
  fsType ?: string

  /**
   * ID that identifies Photon Controller persistent disk
   * @type string
   */
  pdID : string

}
