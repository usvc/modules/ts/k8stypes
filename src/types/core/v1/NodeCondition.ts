import { Time } from '../../meta/v1/Time';

/**
 * NodeCondition contains condition information for a node.
 * @interface NodeCondition
 * @see io.k8s.api.core.v1.NodeCondition
 */
export interface NodeCondition {
  /**
   * Last time we got an update on a given condition.
   * @type Time
   */
  lastHeartbeatTime ?: Time

  /**
   * Last time the condition transit from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * Human readable message indicating details about last transition.
   * @type string
   */
  message ?: string

  /**
   * (brief) reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status of the condition, one of True, False, Unknown.
   * @type string
   */
  status : string

  /**
   * Type of node condition.
   * @type string
   */
  type : string

}
