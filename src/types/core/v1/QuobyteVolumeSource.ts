/**
 * Represents a Quobyte mount that lasts the lifetime of a pod. Quobyte volumes do not support ownership management or SELinux relabeling.
 * @interface QuobyteVolumeSource
 * @see io.k8s.api.core.v1.QuobyteVolumeSource
 */
export interface QuobyteVolumeSource {
  /**
   * Group to map volume access to Default is no group
   * @type string
   */
  group ?: string

  /**
   * ReadOnly here will force the Quobyte volume to be mounted with read-only permissions. Defaults to false.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Registry represents a single or multiple Quobyte Registry services specified as a string as host:port pair (multiple entries are separated with commas) which acts as the central registry for volumes
   * @type string
   */
  registry : string

  /**
   * Tenant owning the given Quobyte volume in the Backend Used with dynamically provisioned Quobyte volumes, value is set by the plugin
   * @type string
   */
  tenant ?: string

  /**
   * User to map volume access to Defaults to serivceaccount user
   * @type string
   */
  user ?: string

  /**
   * Volume is a string that references an already created Quobyte volume by name.
   * @type string
   */
  volume : string

}
