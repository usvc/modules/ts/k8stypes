import { DownwardAPIVolumeFile } from '../../core/v1/DownwardAPIVolumeFile';

/**
 * Represents downward API info for projecting into a projected volume. Note that this is identical to a downwardAPI volume source without the default mode.
 * @interface DownwardAPIProjection
 * @see io.k8s.api.core.v1.DownwardAPIProjection
 */
export interface DownwardAPIProjection {
  /**
   * Items is a list of DownwardAPIVolume file
   * @type DownwardAPIVolumeFile[]
   */
  items ?: DownwardAPIVolumeFile[]

}
