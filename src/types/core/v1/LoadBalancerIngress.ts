/**
 * LoadBalancerIngress represents the status of a load-balancer ingress point: traffic intended for the service should be sent to an ingress point.
 * @interface LoadBalancerIngress
 * @see io.k8s.api.core.v1.LoadBalancerIngress
 */
export interface LoadBalancerIngress {
  /**
   * Hostname is set for load-balancer ingress points that are DNS based (typically AWS load-balancers)
   * @type string
   */
  hostname ?: string

  /**
   * IP is set for load-balancer ingress points that are IP based (typically GCE or OpenStack load-balancers)
   * @type string
   */
  ip ?: string

}
