import { Quantity } from '../../api/resource/Quantity';

/**
 * ResourceQuotaStatus defines the enforced hard limits and observed use.
 * @interface ResourceQuotaStatus
 * @see io.k8s.api.core.v1.ResourceQuotaStatus
 */
export interface ResourceQuotaStatus {
  /**
   * Hard is the set of enforced hard limits for each named resource. More info: https://kubernetes.io/docs/concepts/policy/resource-quotas/
   * @type {[key : string] : Quantity}
   */
  hard ?: {[key : string] : Quantity}

  /**
   * Used is the current observed total usage of the resource in the namespace.
   * @type {[key : string] : Quantity}
   */
  used ?: {[key : string] : Quantity}

}
