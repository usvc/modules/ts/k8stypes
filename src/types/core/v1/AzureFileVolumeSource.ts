/**
 * AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
 * @interface AzureFileVolumeSource
 * @see io.k8s.api.core.v1.AzureFileVolumeSource
 */
export interface AzureFileVolumeSource {
  /**
   * Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * the name of secret that contains Azure Storage Account Name and Key
   * @type string
   */
  secretName : string

  /**
   * Share Name
   * @type string
   */
  shareName : string

}
