import { HTTPHeader } from '../../core/v1/HTTPHeader';
import { IntOrString } from '../../util/intstr/IntOrString';

/**
 * HTTPGetAction describes an action based on HTTP Get requests.
 * @interface HTTPGetAction
 * @see io.k8s.api.core.v1.HTTPGetAction
 */
export interface HTTPGetAction {
  /**
   * Host name to connect to, defaults to the pod IP. You probably want to set "Host" in httpHeaders instead.
   * @type string
   */
  host ?: string

  /**
   * Custom headers to set in the request. HTTP allows repeated headers.
   * @type HTTPHeader[]
   */
  httpHeaders ?: HTTPHeader[]

  /**
   * Path to access on the HTTP server.
   * @type string
   */
  path ?: string

  /**
   * Name or number of the port to access on the container. Number must be in the range 1 to 65535. Name must be an IANA_SVC_NAME.
   * @type IntOrString
   */
  port : IntOrString

  /**
   * Scheme to use for connecting to the host. Defaults to HTTP.
   * @type string
   */
  scheme ?: string

}
