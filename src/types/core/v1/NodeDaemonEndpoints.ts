import { DaemonEndpoint } from '../../core/v1/DaemonEndpoint';

/**
 * NodeDaemonEndpoints lists ports opened by daemons running on the Node.
 * @interface NodeDaemonEndpoints
 * @see io.k8s.api.core.v1.NodeDaemonEndpoints
 */
export interface NodeDaemonEndpoints {
  /**
   * Endpoint on which Kubelet is listening.
   * @type DaemonEndpoint
   */
  kubeletEndpoint ?: DaemonEndpoint

}
