import { Time } from '../../meta/v1/Time';

/**
 * The node this Taint is attached to has the "effect" on any pod that does not tolerate the Taint.
 * @interface Taint
 * @see io.k8s.api.core.v1.Taint
 */
export interface Taint {
  /**
   * Required. The effect of the taint on pods that do not tolerate the taint. Valid effects are NoSchedule, PreferNoSchedule and NoExecute.
   * @type string
   */
  effect : string

  /**
   * Required. The taint key to be applied to a node.
   * @type string
   */
  key : string

  /**
   * TimeAdded represents the time at which the taint was added. It is only written for NoExecute taints.
   * @type Time
   */
  timeAdded ?: Time

  /**
   * Required. The taint value corresponding to the taint key.
   * @type string
   */
  value ?: string

}
