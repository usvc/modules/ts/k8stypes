import { NodeSelectorTerm } from '../../core/v1/NodeSelectorTerm';

/**
 * An empty preferred scheduling term matches all objects with implicit weight 0 (i.e. it's a no-op). A null preferred scheduling term matches no objects (i.e. is also a no-op).
 * @interface PreferredSchedulingTerm
 * @see io.k8s.api.core.v1.PreferredSchedulingTerm
 */
export interface PreferredSchedulingTerm {
  /**
   * A node selector term, associated with the corresponding weight.
   * @type NodeSelectorTerm
   */
  preference : NodeSelectorTerm

  /**
   * Weight associated with matching the corresponding nodeSelectorTerm, in the range 1-100.
   * @type number
   */
  weight : number

}
