/**
 * HostAlias holds the mapping between IP and hostnames that will be injected as an entry in the pod's hosts file.
 * @interface HostAlias
 * @see io.k8s.api.core.v1.HostAlias
 */
export interface HostAlias {
  /**
   * Hostnames for the above IP address.
   * @type string[]
   */
  hostnames ?: string[]

  /**
   * IP address of the host file entry.
   * @type string
   */
  ip ?: string

}
