/**
 * PersistentVolumeStatus is the current status of a persistent volume.
 * @interface PersistentVolumeStatus
 * @see io.k8s.api.core.v1.PersistentVolumeStatus
 */
export interface PersistentVolumeStatus {
  /**
   * A human-readable message indicating details about why the volume is in this state.
   * @type string
   */
  message ?: string

  /**
   * Phase indicates if a volume is available, bound to a claim, or released by a claim. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#phase
   * @type string
   */
  phase ?: string

  /**
   * Reason is a brief CamelCase string that describes any failure and is meant for machine parsing and tidy display in the CLI.
   * @type string
   */
  reason ?: string

}
