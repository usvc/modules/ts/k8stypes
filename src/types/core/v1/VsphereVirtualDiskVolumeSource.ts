/**
 * Represents a vSphere volume resource.
 * @interface VsphereVirtualDiskVolumeSource
 * @see io.k8s.api.core.v1.VsphereVirtualDiskVolumeSource
 */
export interface VsphereVirtualDiskVolumeSource {
  /**
   * Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified.
   * @type string
   */
  fsType ?: string

  /**
   * Storage Policy Based Management (SPBM) profile ID associated with the StoragePolicyName.
   * @type string
   */
  storagePolicyID ?: string

  /**
   * Storage Policy Based Management (SPBM) profile name.
   * @type string
   */
  storagePolicyName ?: string

  /**
   * Path that identifies vSphere volume vmdk
   * @type string
   */
  volumePath : string

}
