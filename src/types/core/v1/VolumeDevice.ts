/**
 * volumeDevice describes a mapping of a raw block device within a container.
 * @interface VolumeDevice
 * @see io.k8s.api.core.v1.VolumeDevice
 */
export interface VolumeDevice {
  /**
   * devicePath is the path inside of the container that the device will be mapped to.
   * @type string
   */
  devicePath : string

  /**
   * name must match the name of a persistentVolumeClaim in the pod
   * @type string
   */
  name : string

}
