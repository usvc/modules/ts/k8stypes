/**
 * SecretKeySelector selects a key of a Secret.
 * @interface SecretKeySelector
 * @see io.k8s.api.core.v1.SecretKeySelector
 */
export interface SecretKeySelector {
  /**
   * The key of the secret to select from.  Must be a valid secret key.
   * @type string
   */
  key : string

  /**
   * Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
   * @type string
   */
  name ?: string

  /**
   * Specify whether the Secret or its key must be defined
   * @type boolean
   */
  optional ?: boolean

}
