import { SecretReference } from '../../core/v1/SecretReference';

/**
 * Represents storage that is managed by an external CSI volume driver (Beta feature)
 * @interface CSIPersistentVolumeSource
 * @see io.k8s.api.core.v1.CSIPersistentVolumeSource
 */
export interface CSIPersistentVolumeSource {
  /**
   * ControllerExpandSecretRef is a reference to the secret object containing sensitive information to pass to the CSI driver to complete the CSI ControllerExpandVolume call. This is an alpha field and requires enabling ExpandCSIVolumes feature gate. This field is optional, and may be empty if no secret is required. If the secret object contains more than one secret, all secrets are passed.
   * @type SecretReference
   */
  controllerExpandSecretRef ?: SecretReference

  /**
   * ControllerPublishSecretRef is a reference to the secret object containing sensitive information to pass to the CSI driver to complete the CSI ControllerPublishVolume and ControllerUnpublishVolume calls. This field is optional, and may be empty if no secret is required. If the secret object contains more than one secret, all secrets are passed.
   * @type SecretReference
   */
  controllerPublishSecretRef ?: SecretReference

  /**
   * Driver is the name of the driver to use for this volume. Required.
   * @type string
   */
  driver : string

  /**
   * Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs".
   * @type string
   */
  fsType ?: string

  /**
   * NodePublishSecretRef is a reference to the secret object containing sensitive information to pass to the CSI driver to complete the CSI NodePublishVolume and NodeUnpublishVolume calls. This field is optional, and may be empty if no secret is required. If the secret object contains more than one secret, all secrets are passed.
   * @type SecretReference
   */
  nodePublishSecretRef ?: SecretReference

  /**
   * NodeStageSecretRef is a reference to the secret object containing sensitive information to pass to the CSI driver to complete the CSI NodeStageVolume and NodeStageVolume and NodeUnstageVolume calls. This field is optional, and may be empty if no secret is required. If the secret object contains more than one secret, all secrets are passed.
   * @type SecretReference
   */
  nodeStageSecretRef ?: SecretReference

  /**
   * Optional: The value to pass to ControllerPublishVolumeRequest. Defaults to false (read/write).
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Attributes of the volume to publish.
   * @type {[key : string] : string}
   */
  volumeAttributes ?: {[key : string] : string}

  /**
   * VolumeHandle is the unique volume name returned by the CSI volume plugin’s CreateVolume to refer to the volume on all subsequent calls. Required.
   * @type string
   */
  volumeHandle : string

}
