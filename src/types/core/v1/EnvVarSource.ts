import { ConfigMapKeySelector } from '../../core/v1/ConfigMapKeySelector';
import { ObjectFieldSelector } from '../../core/v1/ObjectFieldSelector';
import { ResourceFieldSelector } from '../../core/v1/ResourceFieldSelector';
import { SecretKeySelector } from '../../core/v1/SecretKeySelector';

/**
 * EnvVarSource represents a source for the value of an EnvVar.
 * @interface EnvVarSource
 * @see io.k8s.api.core.v1.EnvVarSource
 */
export interface EnvVarSource {
  /**
   * Selects a key of a ConfigMap.
   * @type ConfigMapKeySelector
   */
  configMapKeyRef ?: ConfigMapKeySelector

  /**
   * Selects a field of the pod: supports metadata.name, metadata.namespace, metadata.labels, metadata.annotations, spec.nodeName, spec.serviceAccountName, status.hostIP, status.podIP.
   * @type ObjectFieldSelector
   */
  fieldRef ?: ObjectFieldSelector

  /**
   * Selects a resource of the container: only resources limits and requests (limits.cpu, limits.memory, limits.ephemeral-storage, requests.cpu, requests.memory and requests.ephemeral-storage) are currently supported.
   * @type ResourceFieldSelector
   */
  resourceFieldRef ?: ResourceFieldSelector

  /**
   * Selects a key of a secret in the pod's namespace
   * @type SecretKeySelector
   */
  secretKeyRef ?: SecretKeySelector

}
