/**
 * Represents a Fibre Channel volume. Fibre Channel volumes can only be mounted as read/write once. Fibre Channel volumes support ownership management and SELinux relabeling.
 * @interface FCVolumeSource
 * @see io.k8s.api.core.v1.FCVolumeSource
 */
export interface FCVolumeSource {
  /**
   * Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified.
   * @type string
   */
  fsType ?: string

  /**
   * Optional: FC target lun number
   * @type number
   */
  lun ?: number

  /**
   * Optional: Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
   * @type boolean
   */
  readOnly ?: boolean

  /**
   * Optional: FC target worldwide names (WWNs)
   * @type string[]
   */
  targetWWNs ?: string[]

  /**
   * Optional: FC volume world wide identifiers (wwids) Either wwids or combination of targetWWNs and lun must be set, but not both simultaneously.
   * @type string[]
   */
  wwids ?: string[]

}
