import { PodAffinityTerm } from '../../core/v1/PodAffinityTerm';

/**
 * The weights of all of the matched WeightedPodAffinityTerm fields are added per-node to find the most preferred node(s)
 * @interface WeightedPodAffinityTerm
 * @see io.k8s.api.core.v1.WeightedPodAffinityTerm
 */
export interface WeightedPodAffinityTerm {
  /**
   * Required. A pod affinity term, associated with the corresponding weight.
   * @type PodAffinityTerm
   */
  podAffinityTerm : PodAffinityTerm

  /**
   * weight associated with matching the corresponding podAffinityTerm, in the range 1-100.
   * @type number
   */
  weight : number

}
