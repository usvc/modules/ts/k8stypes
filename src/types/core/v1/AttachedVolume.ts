/**
 * AttachedVolume describes a volume attached to a node
 * @interface AttachedVolume
 * @see io.k8s.api.core.v1.AttachedVolume
 */
export interface AttachedVolume {
  /**
   * DevicePath represents the device path where the volume should be available
   * @type string
   */
  devicePath : string

  /**
   * Name of the attached volume
   * @type string
   */
  name : string

}
