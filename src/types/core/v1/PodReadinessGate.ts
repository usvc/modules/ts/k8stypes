/**
 * PodReadinessGate contains the reference to a pod condition
 * @interface PodReadinessGate
 * @see io.k8s.api.core.v1.PodReadinessGate
 */
export interface PodReadinessGate {
  /**
   * ConditionType refers to a condition in the pod's condition list with matching type.
   * @type string
   */
  conditionType : string

}
