import { NodeSelector } from '../../core/v1/NodeSelector';

/**
 * VolumeNodeAffinity defines constraints that limit what nodes this volume can be accessed from.
 * @interface VolumeNodeAffinity
 * @see io.k8s.api.core.v1.VolumeNodeAffinity
 */
export interface VolumeNodeAffinity {
  /**
   * Required specifies hard node constraints that must be met.
   * @type NodeSelector
   */
  required ?: NodeSelector

}
