import { ListMeta } from '../../meta/v1/ListMeta';
import { Pod } from '../../core/v1/Pod';

/**
 * PodList is a list of Pods.
 * @interface PodList
 * @see io.k8s.api.core.v1.PodList
 */
export interface PodList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "core/v1" | string

  /**
   * List of pods. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md
   * @type Pod[]
   */
  items : Pod[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type ListMeta
   */
  metadata ?: ListMeta

}
