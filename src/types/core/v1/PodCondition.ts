import { Time } from '../../meta/v1/Time';

/**
 * PodCondition contains details for the current condition of this pod.
 * @interface PodCondition
 * @see io.k8s.api.core.v1.PodCondition
 */
export interface PodCondition {
  /**
   * Last time we probed the condition.
   * @type Time
   */
  lastProbeTime ?: Time

  /**
   * Last time the condition transitioned from one status to another.
   * @type Time
   */
  lastTransitionTime ?: Time

  /**
   * Human-readable message indicating details about last transition.
   * @type string
   */
  message ?: string

  /**
   * Unique, one-word, CamelCase reason for the condition's last transition.
   * @type string
   */
  reason ?: string

  /**
   * Status is the status of the condition. Can be True, False, Unknown. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#pod-conditions
   * @type string
   */
  status : string

  /**
   * Type is the type of the condition. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#pod-conditions
   * @type string
   */
  type : string

}
