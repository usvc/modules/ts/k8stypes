/**
 * LocalObjectReference contains enough information to let you locate the referenced object inside the same namespace.
 * @interface LocalObjectReference
 * @see io.k8s.api.core.v1.LocalObjectReference
 */
export interface LocalObjectReference {
  /**
   * Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
   * @type string
   */
  name ?: string

}
