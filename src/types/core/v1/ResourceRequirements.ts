import { Quantity } from '../../api/resource/Quantity';

/**
 * ResourceRequirements describes the compute resource requirements.
 * @interface ResourceRequirements
 * @see io.k8s.api.core.v1.ResourceRequirements
 */
export interface ResourceRequirements {
  /**
   * Limits describes the maximum amount of compute resources allowed. More info: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
   * @type {[key : string] : Quantity}
   */
  limits ?: {[key : string] : Quantity}

  /**
   * Requests describes the minimum amount of compute resources required. If Requests is omitted for a container, it defaults to Limits if that is explicitly specified, otherwise to an implementation-defined value. More info: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
   * @type {[key : string] : Quantity}
   */
  requests ?: {[key : string] : Quantity}

}
