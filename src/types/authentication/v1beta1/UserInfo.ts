/**
 * UserInfo holds the information about the user needed to implement the user.Info interface.
 * @interface UserInfo
 * @see io.k8s.api.authentication.v1beta1.UserInfo
 */
export interface UserInfo {
  /**
   * Any additional information provided by the authenticator.
   * @type {[key : string] : any}
   */
  extra ?: {[key : string] : any}

  /**
   * The names of groups this user is a part of.
   * @type string[]
   */
  groups ?: string[]

  /**
   * A unique value that identifies this user across time. If this user is deleted and another user by the same name is added, they will have different UIDs.
   * @type string
   */
  uid ?: string

  /**
   * The name that uniquely identifies this user among all active users.
   * @type string
   */
  username ?: string

}
