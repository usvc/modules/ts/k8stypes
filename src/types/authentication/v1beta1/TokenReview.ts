import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { TokenReviewSpec } from '../../authentication/v1beta1/TokenReviewSpec';
import { TokenReviewStatus } from '../../authentication/v1beta1/TokenReviewStatus';

/**
 * TokenReview attempts to authenticate a token to a known user. Note: TokenReview requests may be cached by the webhook token authenticator plugin in the kube-apiserver.
 * @interface TokenReview
 * @see io.k8s.api.authentication.v1beta1.TokenReview
 */
export interface TokenReview {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "authentication/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * no description provided
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Spec holds information about the request being evaluated
   * @type TokenReviewSpec
   */
  spec : TokenReviewSpec

  /**
   * Status is filled in by the server and indicates whether the request can be authenticated.
   * @type TokenReviewStatus
   */
  status ?: TokenReviewStatus

}
