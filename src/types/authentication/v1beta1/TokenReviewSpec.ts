/**
 * TokenReviewSpec is a description of the token authentication request.
 * @interface TokenReviewSpec
 * @see io.k8s.api.authentication.v1beta1.TokenReviewSpec
 */
export interface TokenReviewSpec {
  /**
   * Audiences is a list of the identifiers that the resource server presented with the token identifies as. Audience-aware token authenticators will verify that the token was intended for at least one of the audiences in this list. If no audiences are provided, the audience will default to the audience of the Kubernetes apiserver.
   * @type string[]
   */
  audiences ?: string[]

  /**
   * Token is the opaque bearer token.
   * @type string
   */
  token ?: string

}
