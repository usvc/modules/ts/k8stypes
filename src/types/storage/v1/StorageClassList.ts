import { ListMeta } from '../../meta/v1/ListMeta';
import { StorageClass } from '../../storage/v1/StorageClass';

/**
 * StorageClassList is a collection of storage classes.
 * @interface StorageClassList
 * @see io.k8s.api.storage.v1.StorageClassList
 */
export interface StorageClassList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "storage/v1" | string

  /**
   * Items is the list of StorageClasses
   * @type StorageClass[]
   */
  items : StorageClass[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
