import { ObjectMeta } from '../../meta/v1/ObjectMeta';
import { VolumeAttachmentSpec } from '../../storage/v1/VolumeAttachmentSpec';
import { VolumeAttachmentStatus } from '../../storage/v1/VolumeAttachmentStatus';

/**
 * VolumeAttachment captures the intent to attach or detach the specified volume to/from the specified node.  VolumeAttachment objects are non-namespaced.
 * @interface VolumeAttachment
 * @see io.k8s.api.storage.v1.VolumeAttachment
 */
export interface VolumeAttachment {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "storage/v1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard object metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * Specification of the desired attach/detach volume behavior. Populated by the Kubernetes system.
   * @type VolumeAttachmentSpec
   */
  spec : VolumeAttachmentSpec

  /**
   * Status of the VolumeAttachment request. Populated by the entity completing the attach or detach operation, i.e. the external-attacher.
   * @type VolumeAttachmentStatus
   */
  status ?: VolumeAttachmentStatus

}
