import { Time } from '../../meta/v1/Time';

/**
 * VolumeError captures an error encountered during a volume operation.
 * @interface VolumeError
 * @see io.k8s.api.storage.v1.VolumeError
 */
export interface VolumeError {
  /**
   * String detailing the error encountered during Attach or Detach operation. This string may be logged, so it should not contain sensitive information.
   * @type string
   */
  message ?: string

  /**
   * Time the error was encountered.
   * @type Time
   */
  time ?: Time

}
