import { CSIDriver } from '../../storage/v1beta1/CSIDriver';
import { ListMeta } from '../../meta/v1/ListMeta';

/**
 * CSIDriverList is a collection of CSIDriver objects.
 * @interface CSIDriverList
 * @see io.k8s.api.storage.v1beta1.CSIDriverList
 */
export interface CSIDriverList {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "storage/v1beta1" | string

  /**
   * items is the list of CSIDriver
   * @type CSIDriver[]
   */
  items : CSIDriver[]

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * Standard list metadata More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
   * @type ListMeta
   */
  metadata ?: ListMeta

}
