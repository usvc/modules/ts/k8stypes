import { CSINodeDriver } from '../../storage/v1beta1/CSINodeDriver';

/**
 * CSINodeSpec holds information about the specification of all CSI drivers installed on a node
 * @interface CSINodeSpec
 * @see io.k8s.api.storage.v1beta1.CSINodeSpec
 */
export interface CSINodeSpec {
  /**
   * drivers is a list of information of all CSI Drivers existing on a node. If all drivers in the list are uninstalled, this can become empty.
   * @type CSINodeDriver[]
   */
  drivers : CSINodeDriver[]

}
