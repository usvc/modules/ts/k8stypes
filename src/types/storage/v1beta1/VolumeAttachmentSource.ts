import { PersistentVolumeSpec } from '../../core/v1/PersistentVolumeSpec';

/**
 * VolumeAttachmentSource represents a volume that should be attached. Right now only PersistenVolumes can be attached via external attacher, in future we may allow also inline volumes in pods. Exactly one member can be set.
 * @interface VolumeAttachmentSource
 * @see io.k8s.api.storage.v1beta1.VolumeAttachmentSource
 */
export interface VolumeAttachmentSource {
  /**
   * inlineVolumeSpec contains all the information necessary to attach a persistent volume defined by a pod's inline VolumeSource. This field is populated only for the CSIMigration feature. It contains translated fields from a pod's inline VolumeSource to a PersistentVolumeSpec. This field is alpha-level and is only honored by servers that enabled the CSIMigration feature.
   * @type PersistentVolumeSpec
   */
  inlineVolumeSpec ?: PersistentVolumeSpec

  /**
   * Name of the persistent volume to attach.
   * @type string
   */
  persistentVolumeName ?: string

}
