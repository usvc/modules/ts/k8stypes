import { CSINodeSpec } from '../../storage/v1beta1/CSINodeSpec';
import { ObjectMeta } from '../../meta/v1/ObjectMeta';

/**
 * CSINode holds information about all CSI drivers installed on a node. CSI drivers do not need to create the CSINode object directly. As long as they use the node-driver-registrar sidecar container, the kubelet will automatically populate the CSINode object for the CSI driver as part of kubelet plugin registration. CSINode has the same name as a node. If the object is missing, it means either there are no CSI Drivers available on the node, or the Kubelet version is low enough that it doesn't create this object. CSINode has an OwnerReference that points to the corresponding node object.
 * @interface CSINode
 * @see io.k8s.api.storage.v1beta1.CSINode
 */
export interface CSINode {
  /**
   * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   * @type string
   */
  apiVersion : "storage/v1beta1" | string

  /**
   * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
   * @type string
   */
  kind ?: string

  /**
   * metadata.name must be the Kubernetes node name.
   * @type ObjectMeta
   */
  metadata ?: ObjectMeta

  /**
   * spec is the specification of CSINode
   * @type CSINodeSpec
   */
  spec : CSINodeSpec

}
