import { VolumeError } from '../../storage/v1beta1/VolumeError';

/**
 * VolumeAttachmentStatus is the status of a VolumeAttachment request.
 * @interface VolumeAttachmentStatus
 * @see io.k8s.api.storage.v1beta1.VolumeAttachmentStatus
 */
export interface VolumeAttachmentStatus {
  /**
   * The last error encountered during attach operation, if any. This field must only be set by the entity completing the attach operation, i.e. the external-attacher.
   * @type VolumeError
   */
  attachError ?: VolumeError

  /**
   * Indicates the volume is successfully attached. This field must only be set by the entity completing the attach operation, i.e. the external-attacher.
   * @type boolean
   */
  attached : boolean

  /**
   * Upon successful attach, this field is populated with any information returned by the attach operation that must be passed into subsequent WaitForAttach or Mount calls. This field must only be set by the entity completing the attach operation, i.e. the external-attacher.
   * @type {[key : string] : string}
   */
  attachmentMetadata ?: {[key : string] : string}

  /**
   * The last error encountered during detach operation, if any. This field must only be set by the entity completing the detach operation, i.e. the external-attacher.
   * @type VolumeError
   */
  detachError ?: VolumeError

}
