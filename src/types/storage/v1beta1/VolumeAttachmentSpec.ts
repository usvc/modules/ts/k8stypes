import { VolumeAttachmentSource } from '../../storage/v1beta1/VolumeAttachmentSource';

/**
 * VolumeAttachmentSpec is the specification of a VolumeAttachment request.
 * @interface VolumeAttachmentSpec
 * @see io.k8s.api.storage.v1beta1.VolumeAttachmentSpec
 */
export interface VolumeAttachmentSpec {
  /**
   * Attacher indicates the name of the volume driver that MUST handle this request. This is the name returned by GetPluginName().
   * @type string
   */
  attacher : string

  /**
   * The node that the volume should be attached to.
   * @type string
   */
  nodeName : string

  /**
   * Source represents the volume that should be attached.
   * @type VolumeAttachmentSource
   */
  source : VolumeAttachmentSource

}
