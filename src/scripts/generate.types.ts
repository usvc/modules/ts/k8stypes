export interface K8SField {
  description : string
  imports : string[]
  key : string
  required ?: boolean
  type : string
};

export type K8SResource = Partial<K8SField> & {
  path : string
  resourceName : string
  version : string
  fields? : K8SField[]
};

export type OpenAPIPropertyField =
  OpenAPIPropertyFieldReferenceType |
  OpenAPIPropertyFieldNonArrayType |
  OpenAPIPropertyFieldObjectType |
  OpenAPIPropertyFieldArrayType;

export type OpenAPIPropertyFieldExtended =
  OpenAPIPropertyFieldReferenceType |
  OpenAPIPropertyFieldNonArrayType;

export interface OpenAPIPropertyFieldArrayType extends OpenAPIPropertyFieldBase {
  items : OpenAPIPropertyFieldExtended
  type : "array"
}
export const propertyIsFieldArray = (toBeDetermined: OpenAPIPropertyField)
  : toBeDetermined is OpenAPIPropertyFieldArrayType =>
    ((toBeDetermined as OpenAPIPropertyFieldArrayType).type === 'array');
    
export interface OpenAPIPropertyFieldNonArrayType extends OpenAPIPropertyFieldBase {
  type : string
}
export const propertyIsFieldNonArray = (toBeDetermined: OpenAPIPropertyField)
  : toBeDetermined is OpenAPIPropertyFieldNonArrayType =>
    ((toBeDetermined as OpenAPIPropertyFieldNonArrayType).type !== 'array');

export interface OpenAPIPropertyFieldObjectType extends OpenAPIPropertyFieldBase {
  additionalProperties : OpenAPIPropertyFieldExtended
  type : "object"
}
export const propertyIsFieldObject = (toBeDetermined: OpenAPIPropertyField)
  : toBeDetermined is OpenAPIPropertyFieldObjectType =>
    ((toBeDetermined as OpenAPIPropertyFieldObjectType).type === 'object');

export interface OpenAPIPropertyFieldReferenceType extends OpenAPIPropertyFieldBase {
  $ref : string
  type : undefined
}
export const propertyIsFieldReference = (toBeDetermined: OpenAPIPropertyField)
  : toBeDetermined is OpenAPIPropertyFieldReferenceType =>
    ((toBeDetermined as OpenAPIPropertyFieldReferenceType).$ref !== undefined);

export interface OpenAPIPropertyFieldBase {
  description : string | undefined
}