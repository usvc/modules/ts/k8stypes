#!/bin/sh
# author: @zephinzer
# usage: run this script to retrieve the latest kubernetes open api specifications
#        at ../data/kubernetes-swagger.json relative to where you're running this
#        from

set -ex;

CURDIR="$(dirname $0)";
PATH_TO_DATA="${CURDIR}/../data";

printf -- '-----------------------------------------\n\n';

printf -- 'retrieving versions of kubernetes from github...\n';
curl -Lo ${PATH_TO_DATA}/releases.json \
  "https://api.github.com/repos/kubernetes/kubernetes/releases";
if [ "$?" != 0 ]; then
  printf -- 'an error happened while grabbing the versions of kubernetes from github :/ try again later!\n\n';
  exit 1;
fi;
printf -- 'stored version information at $(pwd)/data/releases.json\n\n';

printf -- '-----------------------------------------\n\n';

printf -- 'retrieving latest version of kubernetes...\n';
cat ${PATH_TO_DATA}/releases.json \
  | jq '.[].tag_name' -r \
  | grep -v beta \
  | grep -v alpha \
  | grep -v rc \
  | sort -V \
  | tail -n 1 \
  | tr -d '\n' \
  > ${PATH_TO_DATA}/latest;
printf -- "stored latest version ($(cat ${PATH_TO_DATA}/latest)) at $(pwd)/data/latest\n\n";

printf -- '-----------------------------------------\n\n';

printf -- 'downloading kubernetes open-api specification...\n';
curl -Lo ${PATH_TO_DATA}/kubernetes-swagger.json \
  "https://github.com/kubernetes/kubernetes/blob/$(cat ${PATH_TO_DATA}/latest)/api/openapi-spec/swagger.json?raw=true";
printf -- 'stored latest open-api specification at $(pwd)/data/kubernetes-swagger.json\n\n';
