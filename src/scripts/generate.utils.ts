import * as fs from 'fs';
import * as path from 'path';
import { OpenAPIV2 } from 'openapi-types';
import {
  K8SField,
  K8SResource,
  OpenAPIPropertyField,
  OpenAPIPropertyFieldReferenceType,
  propertyIsFieldArray,
  propertyIsFieldNonArray,
  propertyIsFieldReference,
  propertyIsFieldObject,
} from './generate.types';

export function getLastItem<T>(arr : T[]) : T { return arr[arr.length - 1]; }
export const hasDescription = (obj : Object) : boolean => (obj.hasOwnProperty('description'));
export const hasProperties = (obj : Object) : boolean => (obj.hasOwnProperty('properties'));
export const hasRequired = (obj : Object) : boolean => (obj.hasOwnProperty('required'));
export const hasType = (obj : Object) : boolean => (obj.hasOwnProperty('type'));
export const removeUselessPrefix = (key) : string => key.replace(/^io\.k8s./, '');

/**
 * clearExistingData recursively removes all files from sub-directories
 * and eventually the directory specified in :fromDirectory
 *
 * @param fromDirectory path to the directory of ill fate
 */
export function clearExistingData(fromDirectory : string) {
  try {
    fs.rmdirSync(fromDirectory);
  } catch (ex) {
    switch (ex.code) {
      case 'ENOTEMPTY':
        const directoryListings = fs.readdirSync(fromDirectory);
        directoryListings.forEach((directoryListing) => {
          const directoryListingFullPath = path.join(fromDirectory, directoryListing);
          const listingInfo = fs.lstatSync(directoryListingFullPath);
          if (listingInfo.isDirectory()) {
            clearExistingData(directoryListingFullPath);
          } else if (listingInfo.isFile() || listingInfo.isSymbolicLink()) {
            fs.unlinkSync(directoryListingFullPath);
          }
        });
        clearExistingData(fromDirectory);
        break;
      case 'ENOENT':
        break;
      default:
        throw ex;
    }
  }
}

/**
 * createFieldFromProperty creates a K8SField object from a property
 * belonging to an OpenAPI specification definition
 *
 * @param key name of the property
 * @param property property object
 */
export function createFieldFromProperty(key : string, property : OpenAPIPropertyField) : K8SField {
  const description = normalizeDescription(property.description);
  let imports : string[] = [];
  let type = 'any';
  if (propertyIsFieldReference(property)) {
    const dereferencedType : DereferencedType =
      getReferencedType(property);
    type = dereferencedType.type;
    imports.push(...dereferencedType.imports);
  } else if (propertyIsFieldArray(property)) {
    if (propertyIsFieldReference(property.items)) {
      const dereferencedType : DereferencedType =
        getReferencedType(property.items);
      type = dereferencedType.type;
      imports.push(...dereferencedType.imports);
    } else if(propertyIsFieldNonArray(property.items)) {
      type = normalizeType(property.items.type);
    }
    type += '[]';
  } else if (propertyIsFieldObject(property)) {
    if (propertyIsFieldReference(property.additionalProperties)) {
      const dereferencedType : DereferencedType =
        getReferencedType(property.additionalProperties);
      type = dereferencedType.type;
      imports.push(...dereferencedType.imports);
    } else if (propertyIsFieldNonArray(property.additionalProperties)) {
      type = normalizeType(property.additionalProperties.type);
    } else {
      type = 'any';
    }
    type = `{[key : string] : ${type}}`;
  } else if (propertyIsFieldNonArray(property)) {
    type = normalizeType(property.type);
  }

  return {description, key, imports, type};
};

interface DereferencedType {
  imports : string[]
  type : string
};
export function getReferencedType(fieldReference : OpenAPIPropertyFieldReferenceType) : DereferencedType {
  let type : string = 'any';
  const imports : string[] = [];
  const {$ref} = fieldReference;
  const refPath = $ref.split('/');
  const refDefinition = getLastItem(refPath);
  const refDomains = refDefinition.split('.');
  type = getLastItem(refDomains);
  if (imports.indexOf(refDefinition) === -1) {
    imports.push(refDefinition);
  }
  return {
    imports,
    type,
  }
};

export function createOutputDirectory(directory : string) {
  try {
    fs.mkdirSync(directory, {recursive: true});
  } catch (ex) {
    if (ex.message.indexOf('EEXIST: file already exists') === -1) {
      throw ex;
    }
  }
};

export function normalizeDescription(description : string) : string {
  if (description === undefined) {
    return 'no description provided';
  }
  return description.replace(/(\n|\t)/gi, ' ').replace(/\*\//gi, '*\\/');
};

export function normalizeKey(key : string) : string {
  if (key.match(/[\-\/]/gi) !== null) {
    return `"${key}"`;
  }
  return key;
}

export function normalizeType(type : string) : string {
  switch (type) {
    case 'integer':
      return 'number';
    case 'object':
      return undefined;
    default:
      return type;
  }
};

export function processDefinitionToK8SResource(id : string, definition : OpenAPIV2.SchemaObject) : K8SResource {
  const domains : string[] = id.split('.');
  let fields : K8SField[] = undefined;
  let type : string = 'any';
  let description : string = undefined;

  if (hasProperties(definition)) {
    const {properties} = definition;
    const propertyKeys = Object.keys(properties);
    propertyKeys.forEach((propertyKey) => {
      const property = properties[propertyKey];
      if (fields === undefined) {
        fields = [];
      }
      const k8sField = createFieldFromProperty(propertyKey, property as OpenAPIPropertyField);
      if (hasRequired(definition) && definition.required.indexOf(propertyKey) !== -1) {
        k8sField.required = true;
      }
      fields.push(k8sField);
    });
  } else if (hasType(definition)) {
    type = normalizeType(definition.type as string);
  }
  if (hasDescription(definition)) {
    description = normalizeDescription(definition.description);
  }
  return {
    resourceName: getLastItem(domains),
    path: domains.slice(0, domains.length - 1).join('/'),
    version: domains.slice(domains.length - 3, domains.length - 1).join('/'),
    fields,
    type,
    description,
  };
};

export function removeUndefinedFields(obj : any) {
  const keys = Object.keys(obj);
  keys.forEach((key) => {
    if (obj[key] === undefined) {
      delete obj[key];
    }
  });
  return obj;
};
