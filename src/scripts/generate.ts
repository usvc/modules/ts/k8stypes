import * as fs from 'fs';
import * as path from 'path';
import { OpenAPIV2 } from 'openapi-types';
import {
  K8SResource,
} from './generate.types';
import {
  clearExistingData,
  createOutputDirectory,
  normalizeKey,
  processDefinitionToK8SResource,
  removeUndefinedFields,
} from './generate.utils';

const PATH_OPENAPI_SPEC = path.join(__dirname, '../data/kubernetes-swagger.json');
const PATH_OUTPUT = path.join(__dirname, '../types');
const PATH_TEMPLATES = path.join(__dirname, '../templates');
const PATH_VERSION = path.join(__dirname, '../data/latest');
const OPENAPI_SPEC : OpenAPIV2.Document = require(PATH_OPENAPI_SPEC);

clearExistingData(PATH_OUTPUT);
createOutputDirectory(PATH_OUTPUT);

const k8sResources = generateTypescriptedK8SResources(OPENAPI_SPEC);

fs.writeFileSync(path.join(PATH_OUTPUT, 'data.json'), JSON.stringify(k8sResources, null, 2));

const pathToIndex = path.join(PATH_OUTPUT, '/index.ts');
const pathToIndexTemplate = path.join(PATH_TEMPLATES, '/index.ts');
let indexFileData = '\n';
try {
  fs.lstatSync(pathToIndexTemplate)
  indexFileData += fs.readFileSync(pathToIndexTemplate).toString('utf8') + '\n';
} catch (ex) { }
const kubernetesVersion = fs.readFileSync(PATH_VERSION);
indexFileData = indexFileData.replace(/\'\.\.\/types/gi, '\'.');
indexFileData += `
/**
 * Version of Kubernetes this set of definitions was created from
 * @type string
 */
`.trimLeft();
indexFileData += `export const VERSION = '${kubernetesVersion}';\n`;
fs.writeFileSync(pathToIndex, indexFileData);

k8sResources.forEach((resource : K8SResource) => {
  const resourceDirPath = path.join(PATH_OUTPUT, `/${resource.version}`);
  const resourcePath = path.join(resourceDirPath, `/${resource.resourceName}.ts`);
  const templatePath = path.join(PATH_TEMPLATES, `/${resource.version}/${resource.resourceName}.ts`);
  let resourceFileData : string = '\n';
  try {
    fs.mkdirSync(resourceDirPath, {recursive: true});
  } catch (ex) {
    throw ex;
  }
  if (resource.fields !== undefined) {
    resourceFileData += `
/**
 * ${resource.description}
 * @interface ${resource.resourceName || resource.type}
 * @see ${resource.path.replace(/\//gi, '.')}.${resource.resourceName || resource.type}
 */\n`.trimLeft();
    resourceFileData += `export interface ${resource.resourceName} {\n`;
    const importedImports : {[key: string]: string} = {}
    resource.fields.forEach((field) => {
      field.imports.forEach((importedItem) => {
        const importedItemDomains = importedItem.split('.');
        const importedItemName = importedItemDomains[importedItemDomains.length - 1];
        const importedItemPath = importedItemDomains.slice(importedItemDomains.length - 3, importedItemDomains.length - 1).join('/');
        if (importedImports[importedItemName] === undefined) {
          importedImports[importedItemName] = `../../${importedItemPath}/${importedItemName}`;
        }
      });
      resourceFileData += '  ' + `
  /**
   * ${field.description}
   * @type ${field.type}
   */\n`.trimLeft();
      if (field.key === 'apiVersion') {
        resourceFileData += `  ${normalizeKey(field.key)} : "${resource.version}" | ${field.type}\n\n`;
      } else {
        resourceFileData += `  ${normalizeKey(field.key)} ${field.required ? '' : '?'}: ${field.type}\n\n`;
      }
    });
    Object.keys(importedImports).sort((a, b) => -a.localeCompare(b)).forEach((importedImportResourceName) => {
      const importPath = importedImports[importedImportResourceName];
      resourceFileData = `import { ${importedImportResourceName} } from '${importPath}';\n` + resourceFileData;
    })
    resourceFileData += '}\n';
  } else {
    resourceFileData += `
/**
 * ${resource.description}
 * @type ${resource.resourceName || resource.type}
 */\n`.trimLeft();
    resourceFileData += `export type ${resource.resourceName} = ${resource.type};\n`;
  }
  resourceFileData = resourceFileData.trimLeft();
  fs.writeFileSync(resourcePath, resourceFileData);
});

function generateTypescriptedK8SResources(openAPISpecification : OpenAPIV2.Document) : K8SResource[] {
  const {definitions} = openAPISpecification;
  const definitionKeys = Object.keys(definitions);
  const outputDefinitions : K8SResource[] = [];

  definitionKeys.forEach((definitionKey : string) => {
    const definition = definitions[definitionKey];
    outputDefinitions.push(processDefinitionToK8SResource(definitionKey, definition));
  });

  return outputDefinitions.map(removeUndefinedFields);
}
