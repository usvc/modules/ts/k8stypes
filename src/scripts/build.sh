#!/bin/sh
tsc --project .. \
  && cd .. \
  && (cp ./* ./lib/ || cp ./.* ./lib || printf -- '') \
  && sed -i -e 's|__k8stypes|k8stypes|g' ./lib/package.json \
  && sed -i '/generate/d' ./lib/package.json \
  && sed -i '/update/d' ./lib/package.json \
  && printf -- "\033[1m\\n\\nnow navigate to ./lib (cd ./lib) and run npm publish from there!\\n\\n\033[0m";
