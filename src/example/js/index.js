/**
 * @typedef {import('../../../lib/apps/v1/Deployment').Deployment} Deployment
 */

const name = 'a-deployment';
const labels = {
  app: 'a-deployment',
};

/** @type {Deployment} */
module.exports = {
  apiVersion: 'app/v1',
  kind: 'Deployment',
  metadata: {
    name,
    namespace: 'default',
    labels,
  },
  spec: {
    replicas: 3,
    selector: {
      matchLabels: labels,
    },
    template: {
      metadata: {
        name,
      },
      spec: {
        containers: [
          {
            name: 'container-a',
            image: 'zephinzer/demo-echoserver:latest',
            imagePullPolicy: 'Always',
          },
        ],
      },
    },
  },
};
