import {Deployment} from '../../../lib/apps/v1/Deployment';

const name = 'a-deployment';
const labels = {
  app: 'a-deployment',
};

const deployment : Deployment = {
  apiVersion: 'apps/v1',
  metadata: {
    name,
    namespace: 'default',
    labels,
  },
  spec: {
    replicas: 3,
    selector: {
      matchLabels: labels,
    },
    template: {
      metadata: {
        name,
      },
      spec: {
        containers: [
          {
            name: 'container-a',
            image: 'zephinzer/demo-echoserver:latest',
            imagePullPolicy: 'Always',
          },
        ],
      },
    },
  },
}